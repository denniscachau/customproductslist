<?php
/**
 * 2007-2023 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2023 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class CPLConfig extends ObjectModel
{
    /** @var string Name */
    public $position;
    public $table;
    public $attribute;
    public $title;
    public $type;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'customproductslist',
        'primary' => 'id_customproductslist',
        'multilang' => false,
        'fields' => array(
            'list' => array(
                'type' => self::TYPE_STRING, 'required' => false
            ),
            'position' => array(
                'type' => self::TYPE_STRING, 'required' => false
            ),
            'table' => array(
                'type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => false
            ),
            'attribute' => array(
                'type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => false
            ),
            'title' => array(
                'type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => false
            ),
            'type' => array(
                'type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => false
            ),
        ),
    );

    public function add($auto_date = true, $null_values = false)
    {
        if (isset($this->id) && !$this->force_id) {
            unset($this->id);
        }

        // Automatically fill dates
        if ($auto_date && property_exists($this, 'date_add')) {
            $this->date_add = date('Y-m-d H:i:s');
        }
        if ($auto_date && property_exists($this, 'date_upd')) {
            $this->date_upd = date('Y-m-d H:i:s');
        }

        if (empty(Tools::getValue('attributes'))) {
            $this->id = 1;
            return true;
        }

        $product_shop = Db::getInstance()->executeS(
            "SHOW COLUMNS FROM `" . _DB_PREFIX_ . "product_shop`"
        );

        $positions = explode(';', Tools::getValue('positions'));
        $attributes = explode(';', Tools::getValue('attributes'));
        $tables = explode(';', Tools::getValue('tables'));
        $titles = explode(';', Tools::getValue('titles'));
        $types = explode(';', Tools::getValue('types'));

        foreach ($attributes as $key => $attribute) {
            $table = pSQL($tables[$key]);
            $attribute = pSQL(str_replace(' ', '_', $attribute));

            if ($table == 'product') {
                foreach ($product_shop as $p_shop) {
                    if ($p_shop['Field'] == $attribute) {
                        $table = 'product_shop';
                    }
                }
            }

            $fields_to_insert = array(
                'list' => pSQL(Context::getContext()->cookie->__get('cpl_list')),
                'position' => pSQL($positions[$key]),
                'attribute' => $attribute,
                'table' => $table,
                'type' => pSQL($types[$key]),
                'title' => pSQL($titles[$key])
            );

            if (!$result = Db::getInstance()->insert(
                $this->def['table'],
                $fields_to_insert,
                $null_values,
                true,
                Db::INSERT_IGNORE
            )) {
                return false;
            }
        }
        $this->id = Db::getInstance()->Insert_ID();

        if (!$result) {
            return false;
        }

        return $result;
    }

    public function changePositions()
    {
        $cpl_list = Context::getContext()->cookie->__get('cpl_list');
        if (!$res = Db::getInstance()->executeS(
            "SELECT `id_customproductslist`, `position`
            FROM `" . _DB_PREFIX_ . "customproductslist`
            WHERE `list` = '" . pSQL($cpl_list) . "'
            ORDER BY `position`"
        )) {
            return false;
        }

        if (version_compare(_PS_VERSION_, '1.6.1.0', '<')) {
            $buffer_position = 1;
        } else {
            $buffer_position = 0;
        }

        foreach ($res as $columns) {
            if ((int)$columns['position'] != $buffer_position) {
                if (!Db::getInstance()->execute(
                    "UPDATE `" . _DB_PREFIX_ . "customproductslist`
                    SET `position` = '" . (int)$buffer_position . "'
                    WHERE `id_customproductslist` = '" . (int)$columns['id_customproductslist'] . "'"
                )) {
                    return false;
                }
            }
            $buffer_position++;
        }
        return true;
    }

    public static function getLastPosition()
    {
        $cpl_list = Context::getContext()->cookie->__get('cpl_list');
        return Db::getInstance()->getValue(
            "SELECT IF(MAX(`position`) IS NULL, 0, MAX(`position`)+1)
            FROM `" . _DB_PREFIX_ . "customproductslist`
            WHERE `list` = '" . pSQL($cpl_list) . "'"
        );
    }

    public function updatePosition($way, $position)
    {
        $cpl_list = Context::getContext()->cookie->__get('cpl_list');
        $id_customproductslist = (int)Configuration::get('id_customproductslist');
        if (!$res = Db::getInstance()->executeS(
            "SELECT `id_customproductslist`, `position`
            FROM `" . _DB_PREFIX_ . "customproductslist`
            WHERE `list` = '" . pSQL($cpl_list) . "'
            ORDER BY `position`"
        )) {
            return false;
        }

        if (version_compare(_PS_VERSION_, '1.6.1.0', '<')) {
            $buffer_position = 1;
        } else {
            $buffer_position = 0;
        }

        foreach ($res as $columns) {
            if ((int)$columns['position'] != $buffer_position) {
                Db::getInstance()->execute(
                    "UPDATE `" . _DB_PREFIX_ . "customproductslist`
                    SET `position` = '" . (int)$buffer_position . "'
                    WHERE `id_customproductslist` = '" . (int)$columns['id_customproductslist'] . "'"
                );
            }
            $buffer_position++;
            if ((int)$columns['id_customproductslist'] == (int)$id_customproductslist) {
                $moved_columns = Db::getInstance()->getRow(
                    "SELECT `id_customproductslist`, `position`
                    FROM `" . _DB_PREFIX_ . "customproductslist`
                    WHERE `id_customproductslist` = '" . (int)$id_customproductslist . "'"
                );
            }
        }

        if (!isset($moved_columns) || !isset($position)) {
            return false;
        }

        // < and > statements rather than BETWEEN operator
        // since BETWEEN is treated differently according to databases
        return (Db::getInstance()->execute("
            UPDATE `" . _DB_PREFIX_ . "customproductslist`
            SET `position`= `position` " . ($way ? "- 1" : "+ 1") . "
            WHERE `list` = '" . pSQL($cpl_list) . "' AND `position`
            " . ($way
                    ? "> " . (int)$moved_columns['position'] . " AND `position` <= " . (int)$position
                    : "< " . (int)$moved_columns['position']." AND `position` >= " . (int)$position . "
            "))
            && Db::getInstance()->execute('
            UPDATE `'._DB_PREFIX_.'customproductslist`
            SET `position` = '.(int)$position.'
            WHERE `id_customproductslist` = '.(int)$moved_columns['id_customproductslist']));
    }

    public function delete()
    {
        $return = parent::delete();
        $this->changePositions();
        return $return;
    }
}
