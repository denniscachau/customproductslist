<?php
/**
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

abstract class CPL extends DbQueryCore
{
    protected $configuration_table = "customproductslist";

    public $select_fields = array();

    public $default_tables = array();

    public $join_ids = array();

    protected $aliases = array();

    protected $context;

    protected $already_join = array();

    protected $attributes_as_list = array();

    protected $attributes = array();

    protected $tables = array();

    protected $types = array();

    protected $fields = array();

    protected $aligns = array();

    protected $titles = array();

    protected $config_tables = array();

    protected $not_editable = array();

    protected $use_parent = true;

    protected $with_aliases = true;

    private static $instance = array();

    protected $isFieldsSet = false;

    protected $isQuerySet = false;

    protected $isConfigSet = false;

    protected $list;

    public function __construct()
    {
        $this->context = Context::getContext();
        $this->attributes_as_list = array(
            'productactive' => 'active',
            'imageid_image' => 'image',
            'category_productposition' => 'position'
        );

        if (version_compare(_PS_VERSION_, '1.7.0.0', '<') || version_compare(_PS_VERSION_, '1.7.6.0', '>=')) {
            $this->attributes_as_list['productprice'] = 'price';
            $this->attributes_as_list['product_shopprice'] = 'price';
            $this->attributes_as_list['productfinal_price'] = 'price_final';
        }
//        $this->setParameters();

        $this->not_editable = $this->getNotEditable();
    }

    /**
     * @param string $type
     * @return CPL
     */
    public static function getInstance($type)
    {
        if (!isset(self::$instance[$type])) {
            switch ($type) {
                case 'override':
                    self::$instance[$type] = new CPLOverride();
                    break;
                case 'config':
                    self::$instance[$type] = (new CPLCustom())->setUseParent(true)->setWithAliases(false);
                    break;
                case 'custom':
                    self::$instance[$type] = new CPLCustom();
                    break;
                case '16':
                    self::$instance[$type] = new CPL16();
                    break;
                default:
                    return null;
            }

            self::$instance[$type]->setParameters();
        }

        return self::$instance[$type];
    }

    public function setQuery()
    {
        if ($this->isQuerySet) {
            return $this;
        }

        $get_configs = $this->getConfigurations();

        if (empty($get_configs)) {
            return 0;
        }

        $this->setJoin();
        $this->handlePosition();
        $this->setSelect();

        $this->isQuerySet = true;

        return $this;
    }

    protected function setJoin()
    {
        $this->already_join[] = 'product';
        $this->already_join[] = 'product_lang';
        $this->already_join[] = "customproductslist";

        $tables = array_unique($this->config_tables);
        $ndefault_tables = array();
        foreach ($tables as $table) {
            foreach ($this->default_tables as $default_table) {
                switch ($table) {
                    case $default_table:
                        $this->addDefaultJoin($table);
                        continue 3;
                    case 'tag':
                        if (!in_array('product_tag', $this->already_join)) {
                            if (version_compare(_PS_VERSION_, '1.6.1.0', '>=')) {
                                $tag_on = " AND `product_tag`.`id_lang` = '" . (int)$this->context->language->id . "'";
                            } else {
                                $tag_on = "";
                            }

                            $this->joinQuery(
                                'left',
                                $this->getAlias('product'),
                                'product_tag',
                                'product_tag',
                                $this->getAlias('product') . '.`id_product` = `product_tag`.`id_product`' . $tag_on
                            );

                            $this->already_join[] = 'product_tag';
                        }

                        if (!in_array('tag', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                'product_tag',
                                'tag',
                                'tag',
                                '`product_tag`.`id_tag` = `tag`.`id_tag`
                                    AND `tag`.`id_lang` = ' . (int)$this->context->language->id
                            );

                            $this->already_join[] = 'tag';
                        }

                        continue 3;
                    case 'attribute_group_name':
                    case 'attribute_lang':
                        $this->addDefaultJoin('product_attribute_combination');

                        if (!in_array('attribute_lang', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                'product_attribute_combination',
                                'attribute_lang',
                                'attribute_lang',
                                '`product_attribute_combination`.`id_attribute` = `attribute_lang`.`id_attribute`
                                    AND `attribute_lang`.`id_lang` = ' . (int)$this->context->language->id
                            );

                            $this->already_join[] = 'attribute_lang';
                        }

                        if (!in_array('attribute', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                'product_attribute_combination',
                                'attribute',
                                'attribute',
                                '`product_attribute_combination`.`id_attribute` = `attribute`.`id_attribute`'
                            );

                            $this->already_join[] = 'attribute';
                        }
                        continue 3;
                    case 'carrier':
                        $this->addDefaultJoin('product_carrier');
                        if (!in_array('carrier', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                'product_carrier',
                                'carrier',
                                'carrier',
                                '`product_carrier`.`id_carrier_reference` = `carrier`.`id_reference`
                                    AND `carrier`.`deleted` = 0'
                            );

                            $this->already_join[] = 'carrier';
                        }
                        continue 3;
                }
            }
            $ndefault_tables[] = $table;
        }

        foreach ($ndefault_tables as $value) {
            if (empty($value) || in_array($value, $this->already_join)) {
                continue;
            }
            $columns = Db::getInstance()->executeS(
                "SHOW COLUMNS FROM `" . _DB_PREFIX_ . pSQL($value) . "`"
            );
            $base_on = '';
            $join_on = '';
            $join_table = '';
            $lang = false;

            foreach ($this->join_ids as $join_table_temp => $field) {
                if (is_array($field)) {
                    $join_table_temp = $field['table'];
                } else {
                    $field = array(
                        'base_on' => $field,
                        'join_on' => $field,
                    );
                }

                foreach ($columns as $column) {
                    if ($column['Field'] == $field['join_on'] && $join_on == '') {
                        $base_on = $field['base_on'];
                        $join_on = $field['join_on'];
                        $join_table = $join_table_temp;
                        $this->addDefaultJoin($join_table);
                    } elseif ($column['Field'] == 'id_lang') {
                        $lang = true;
                    }

                    if ($lang && $join_on != '') {
                        break 2;
                    }
                }
            }
            $this->already_join[] = $value;
            $on = '`' . pSQL($value) . '`.`' . pSQL($join_on) . '` =
                    `' . pSQL($join_table) . '`.`' . pSQL($base_on) . '`';
            if ($lang && preg_match('/.*_lang$/', $value)) {
                $on .= " AND `" . pSQL($value) . "`.`id_lang` = '" . (int)$this->context->language->id . "'";
            }

            $this->joinQuery(
                'left',
                pSQL($join_table),
                pSQL($value),
                pSQL($value),
                $on
            );
        }
    }

    protected function setSelect()
    {
        foreach ($this->attributes as $key => $attribute) {
            if (!array_key_exists($key, $this->config_tables) ||
                empty($this->config_tables[$key]) ||
                empty($attribute)) {
                unset($this->attributes[$key]);
                continue;
            }

            $attribute_as = $this->getAttributeAs($this->config_tables[$key], $attribute);

            $default_select = false;

            switch ($this->config_tables[$key]) {
                case 'customproductslist':
                    switch ($attribute) {
                        case 'final_price_discount_tax_excl':
                        case 'final_price_discount':
                            $this->selectQuery(
                                "'0'",
                                $attribute_as
                            );
                            break;
                        case 'discount_reduction':
                            // TODO get with callback
                            $this->selectQuery(
                                "IF(
                                    `specific_price`.`reduction_type` = 'amount',
                                    CONCAT(
                                        ROUND(`specific_price`.`reduction`, 2),
                                        ' " . pSQL($this->context->currency->getSign()) . "'
                                    ),
                                    CONCAT(
                                        ROUND(`specific_price`.`reduction` * 100, 2)
                                        , ' %'
                                    )
                                )",
                                $attribute_as
                            );

                            if (!in_array('specific_price', $this->already_join)) {
                                $this->joinQuery(
                                    'left',
                                    $this->getAlias('product'),
                                    'specific_price',
                                    'specific_price',
                                    "`a`.`id_product` = `specific_price`.`id_product`
                                    AND `specific_price`.`id_product_attribute` = 0
                                    AND `specific_price`.`id_group` = 0
                                    AND `specific_price`.`id_customer` = 0
                                    AND (`specific_price`.`from` <= NOW() OR `specific_price`.`from` = ''
                                        OR `specific_price`.`from` IS NULL)
                                    AND (`specific_price`.`to` >= NOW() OR `specific_price`.`to` = ''
                                        OR `specific_price`.`to` = 0 OR `specific_price`.`to` IS NULL)"
                                );

                                $this->already_join[] = 'specific_price';
                            }

                            $this->types[$key] = "";
                            break;
                        case 'categories':
                            $this->selectQuery(
                                "GROUP_CONCAT(DISTINCT `cpl_categories_lang`.`name` SEPARATOR ', ')",
                                $attribute_as
                            );

                            $this->addDefaultJoin('categories_category_product');
                            if (!in_array('cpl_categories_lang', $this->already_join)) {
                                $this->joinQuery(
                                    'left',
                                    $this->getAlias('category_product'),
                                    'category_lang',
                                    'cpl_categories_lang',
                                    "`" . $this->getAlias('categories_category_product') . "`.`id_category` =
                                        `cpl_categories_lang`.`id_category`
                                        AND `cpl_categories_lang`.`id_lang` = " . (int)$this->context->language->id
                                );

                                $this->already_join[] = 'cpl_categories_lang';
                            }

                            $this->tables[$key] = 'cpl_categories_lang';
                            $attribute = 'name';
                            break;
                        case 'unit_price':
                            $this->selectQuery(
                                "MAX(
                                    `" . $this->getAlias('product_shop') . "`.`price` / 
                                    `" . $this->getAlias('product_shop') . "`.`unit_price_ratio`)",
                                $attribute_as
                            );
                            break;
                    }
                    break;
                case 'image':
                    $this->selectQuery(
                        "`" . pSQL($this->tables[$key]) . "`.`id_image`",
                        'id_image'
                    );
                    break;
                case 'feature_name':
                    $id_feature_value = (int)Db::getInstance()->getValue(
                        "SELECT `id_feature`
                        FROM `" . _DB_PREFIX_ . "feature_lang`
                        WHERE `name` LIKE '" . pSQL($attribute) . "'"
                    );

                    if ($id_feature_value > 0) {
                        $this->addDefaultJoin('feature_product');

                        $on = "`table_" . $attribute_as . "`.`id_feature_value`=`feature_product`.`id_feature_value`
                            AND `table_" . $attribute_as . "`.`id_lang` = " . (int)$this->context->language->id . "
                            AND `feature_product`.`id_feature` = " . (int)$id_feature_value;

                        $this->joinQuery(
                            'left',
                            'feature_product',
                            'feature_value_lang',
                            'table_' . $attribute_as,
                            $on
                        );

                        $this->selectQuery(
                            "GROUP_CONCAT(DISTINCT `table_" . pSQL($attribute_as) . "`.`value`
                                SEPARATOR ', ')",
                            $attribute_as
                        );
                    }
                    $this->tables[$key] = 'table_' . pSQL($attribute_as);
                    $attribute = 'value';
                    break;
                case 'attribute_group_name':
                    $id_attribute_group = (int)Db::getInstance()->getValue(
                        "SELECT `id_attribute_group`
                        FROM `" . _DB_PREFIX_ . "attribute_group_lang`
                        WHERE `name` LIKE '" . pSQL($attribute) . "'"
                    );

                    if ($id_attribute_group > 0) {
                        $this->addDefaultJoin('product_attribute_combination');
                        $this->addDefaultJoin('attribute');

                        $on =
                            "`table_" . $attribute_as . "`.`id_attribute`=`product_attribute_combination`.`id_attribute`
                        AND `table_" . $attribute_as . "`.`id_lang` = " . (int)$this->context->language->id . "
                        AND `attribute`.`id_attribute_group` = " . (int)$id_attribute_group;

                        $this->joinQuery(
                            'left',
                            'product_attribute_combination',
                            'attribute_lang',
                            'table_' . $attribute_as,
                            $on
                        );

                        $this->selectQuery(
                            "GROUP_CONCAT(DISTINCT `table_" . pSQL($attribute_as) . "`.`name`
                                SEPARATOR ', ')",
                            $attribute_as
                        );
                    }
                    $this->tables[$key] = 'table_' . pSQL($attribute_as);
                    $attribute = 'name';
                    break;

                default:
                    switch ($attribute_as) {
                        case 'price_final':
                        case 'productfinal_price':
                            $this->selectQuery(
                                "MAX(`" . $this->getAlias('product_shop') . "`.`price`)",
                                $attribute_as
                            );
                            break;
                        case 'carriername':
                            $this->selectQuery(
                                "GROUP_CONCAT(
                                    DISTINCT IF(`" . pSQL($this->tables[$key]) . "`.`" . pSQL($attribute) . "` = '0',
                                    '" . pSQL(Configuration::get('PS_SHOP_NAME')) . "',
                                    `" . pSQL($this->tables[$key]) . "`.`" . pSQL($attribute) . "`)
                                    SEPARATOR ', ')",
                                $attribute_as
                            );
                            break;
//                        Done in the getConfiguration method
//                        case 'position':
//                            if (!$this->categoryFiltered(false)) {
//                                unset($this->attributes[$key]);
//                                continue 3;
//                            }
//                            break;

                        default:
                            if (!empty($this->types[$key]) &&
                                ($this->types[$key] == 'price' ||
                                    $this->types[$key] == 'integer' ||
                                    $this->types[$key] == 'decimal')) {
                                $this->selectQuery(
                                    "MAX(`" . pSQL($this->tables[$key]) . "`.`" . pSQL($attribute) . "`)",
                                    $attribute_as
                                );
                            } elseif (!empty($this->types[$key]) && $this->types[$key] != 'editable') {
                                $this->selectQuery(
                                    "`" . pSQL($this->tables[$key]) . "`.`" . pSQL($attribute) . "`",
                                    $attribute_as
                                );
                            } else {
                                $default_select = true;
                            }
                            break;
                    }
                    break;
            }

            if ($default_select) {
                $this->selectQuery(
                    "GROUP_CONCAT(" .
                    "DISTINCT `" . pSQL($this->tables[$key]) . "`.`" . pSQL($attribute) . "` " .
                    "SEPARATOR ', ')",
                    $attribute_as
                );
            }
        }
    }

    public function getFields()
    {
        if ($this->isFieldsSet) {
            return $this->fields;
        }
        $count_editable = 0;

        $this->fields['id_product'] = array(
            'title' => 'ID',
            'align' => 'text-center',
            'class' => 'fixed-width-xs'
        );
        foreach ($this->attributes as $key => $attribute) {
            $attribute_as = $this->getAttributeAs($this->config_tables[$key], $attribute);
            $class = 'cpl-' . $attribute_as;
            $havingFilter = true;

            switch ($this->aligns[$key]) {
                case '0':
                    $align = 'text-left';
                    break;
                case '2':
                    $align = 'text-right';
                    break;
                default:
                    $align = 'text-center';
                    break;
            }

            if (!empty($this->titles[$key])) {
                $this->fields[$attribute_as]['title'] = $this->titles[$key];
                $this->fields[$attribute_as]['align'] = $align;
            } else {
                $this->fields[$attribute_as]['title'] = $attribute;
                $this->fields[$attribute_as]['align'] = $align;
            }

            switch ($this->types[$key]) {
                case 'boolean':
                    $data_type = Db::getInstance()->getValue(
                        "SELECT `data_type`
                        FROM `information_schema`.`COLUMNS`
                        WHERE `table_schema` = '" . _DB_NAME_ . "'
                        AND `table_name` = '" . _DB_PREFIX_ . pSQL($this->config_tables[$key]) . "'
                        AND `column_name` = '" . pSQL($attribute) . "'"
                    );

                    if ($data_type == "tinyint" &&
                        (version_compare(_PS_VERSION_, '1.7.0.0', '>') || !Configuration::get('CPL_use_default'))) {
                        $this->fields[$attribute_as]['type'] = 'bool';
                        $this->fields[$attribute_as]['active'] = $this->config_tables[$key] . ';;;' . $attribute;
                        $this->fields[$attribute_as]['ajax'] = true;
                    } elseif ($data_type == "tinyint") {
                        $this->fields[$attribute_as]['type'] = 'bool';
                        $this->fields[$attribute_as]['callback'] = 'printBooleanField';
                        $this->fields[$attribute_as]['callback_object'] = $this;
                    }
                    break;
                case 'price':
                    $this->fields[$attribute_as]['type'] = 'price';
                    break;
                case 'percent':
                    $this->fields[$attribute_as]['type'] = 'percent';
                    break;
                case 'decimal':
                    $this->fields[$attribute_as]['type'] = 'decimal';
                    break;
                case 'integer':
                    $this->fields[$attribute_as]['type'] = 'int';
                    break;
                case 'date':
                    $this->fields[$attribute_as]['type'] = 'date';
                    break;
                case 'datetime':
                    $this->fields[$attribute_as]['type'] = 'datetime';
                    break;
                case 'editable':
                    if (!in_array($attribute_as, $this->not_editable) &&
                        $this->config_tables[$key] != 'feature_name' &&
                        $this->config_tables[$key] != 'attribute_group_name' &&
                        $this->config_tables[$key] != 'customproductslist') {
                        if ($count_editable <= 10) {
                            $class .= ' cpl-editable';
                            $this->fields[$attribute_as]['callback'] = 'printEditableField' . $count_editable;
                            $this->fields[$attribute_as]['callback_object'] = $this;
                        }
                    }
                    $count_editable++;
                    break;
                case 'select':
                    // id = id_feature, table = feature_lang, filter_key = feature_product
                    if (in_array($this->tables[$key], array_keys($this->select_fields))) {
                        $select_values = Db::getInstance()->executeS(
                            "SELECT `" . pSQL($this->select_fields[$this->tables[$key]]['id']) .
                                "` AS 'id',
                                `" . pSQL($this->select_fields[$this->tables[$key]]['name']) .
                                "` AS `name`
                                FROM `" . _DB_PREFIX_ . pSQL(
                                    $this->select_fields[$this->tables[$key]]['table']
                                ) . "`" . ($this->select_fields[$this->tables[$key]]['lang'] ?
                                "WHERE `id_lang` = '" . (int)$this->context->language->id . "'" :
                                "") . " ORDER BY `name` ASC"
                        );

                        $select_display = array();
                        foreach ($select_values as $select_value) {
                            $select_display[$select_value['id']] = $select_value['name'];
                        }

                        $this->fields[$attribute_as]['type'] = 'select';
                        $this->fields[$attribute_as]['list'] = $select_display;
                        $this->fields[$attribute_as]['filter_key'] =
                            (isset($this->select_fields[$this->tables[$key]]['filter_key']) ?
                                $this->select_fields[$this->tables[$key]]['filter_key'] :
                                $this->tables[$key]) . '!' . $this->select_fields[$this->tables[$key]]['id'];
                        continue 2;
                    }
                    break;
            }

            switch ($attribute_as) {
                case 'active':
                    $this->fields[$attribute_as]['active'] = 'status';
                    $this->fields[$attribute_as]['type'] = 'bool';
                    break;
                case 'image':
                    $this->fields[$attribute_as]['search'] = false;
                    $this->fields[$attribute_as]['orderby'] = false;
                    $this->fields[$attribute_as]['image'] = 'p';
                    break;
                case 'price_final':
                case 'productfinal_price':
                    $this->fields[$attribute_as]['search'] = false;
//                  $this->fields[$attribute_as]['orderby'] = false;
                    break;
                case 'position':
                    $this->fields[$attribute_as]['position'] = 'position';
                    $this->fields[$attribute_as]['filter_key'] = $this->getAlias('category_product') . '!position';
                    $this->fields[$attribute_as]['order_by'] = $this->getAlias('category_product') . '!position';
                    $class .= ' fixed-width-xs';
                    break;

                default:
                    if ($this->tables[$key] == 'customproductslist') {
                        $this->fields[$attribute_as]['search'] = false;
                        $this->fields[$attribute_as]['orderby'] = false;
                    } elseif ($havingFilter) {
                        $this->fields[$attribute_as]['havingFilter'] = true;
                    } else {
                        $this->fields[$attribute_as]['filter_key'] = $this->tables[$key] . '!' . $attribute;
                    }
                    break;
            }
            $this->fields[$attribute_as]['class'] = $class;
        }

        $this->isFieldsSet = true;
        return $this->fields;
    }

    protected function getConfigurations()
    {
        if ($this->isConfigSet) {
            return true;
        }

        $config = Db::getInstance()->getRow(
            "SELECT
                GROUP_CONCAT(`table` ORDER BY `position` ASC SEPARATOR ';') as 'tables',
                GROUP_CONCAT(`attribute` ORDER BY `position` ASC SEPARATOR ';') as 'attributes',
                GROUP_CONCAT(`title` ORDER BY `position` ASC SEPARATOR ';') as 'titles',
                GROUP_CONCAT(`type` ORDER BY `position` ASC SEPARATOR ';') as 'types',
                GROUP_CONCAT(`align` ORDER BY `position` ASC SEPARATOR ';') as 'aligns'
            FROM `" . _DB_PREFIX_ . $this->configuration_table . "`
            WHERE `list` = '" . pSQL($this->list) . "'"
        );

        $this->tables = explode(';', $config['tables']);

        foreach ($this->default_tables as $table_to_min) {
            if (!array_key_exists($table_to_min, $this->aliases)) {
                continue;
            }
            $this->tables = preg_replace(
                '/^' . $table_to_min . '$/',
                $this->aliases[$table_to_min],
                $this->tables
            );
        }

        // Initialisation
        $this->attributes = explode(';', $config['attributes']);
        $this->config_tables = explode(';', $config['tables']);
        $this->titles = explode(';', $config['titles']);
        $this->types = explode(';', $config['types']);
        $this->aligns = explode(';', $config['aligns']);

        if (!$this->categoryFiltered(true)) {
            $key = array_search('position', $this->attributes);
            unset($this->attributes[$key]);
        }

        $this->isConfigSet = true;

        return !empty($config);
    }

    protected function setParameters()
    {
        $this->aliases = array(
            'product' => 'a',
            'product_lang' => 'b',
            'category' => 'c',
            'category_lang' => 'cl',
            'category_product' => 'cp',
            'image' => 'i',
//            'image_shop' => 'is',
            'product_shop' => 'sa',
            'stock_available' => 'sav',
            'product_download' => 'pd'
        );

        $this->join_ids = array(
            $this->getAlias('product') => 'id_product',
            'a2' => array(
                'base_on' => 'id_tax_rules_group',
                'join_on' => 'id_tax_rules_group',
                'table' => $this->getAlias('product_shop')
            ),
            'a3' => array(
                'base_on' => 'id_category',
                'join_on' => 'id_category',
                'table' => $this->getAlias('product_shop')
            ),
            'manufacturer' => 'id_manufacturer',
            'supplier' => 'id_supplier',
            'feature_product' => 'id_feature_value',
            'carrier' => array(
                'base_on' => 'id_carrier_reference',
                'join_on' => 'id_reference',
                'table' => 'product_carrier'
            ),
        );

        $this->default_tables = array(
            'product',
            'product_lang',
            'category',
            'category_lang',
            'image',
            'image_shop',
            'product_download',
            'product_attribute',
            'stock_available',
            'product_shop',
            'feature_product',
            'manufacturer',
            'product_supplier',
            'product_carrier',
            'supplier',
            'customproductslist',
            'category_product',
            'feature_name', // DO NOT REMOVE
        );

        // No changes is needed for the aliases
        $this->select_fields = array(
            'manufacturer' => array(
                'id' => 'id_manufacturer',
                'table' => 'manufacturer',
                'name' => 'name',
                'lang' => false
            ),
            'supplier' => array(
                'id' => 'id_supplier',
                'table' => 'supplier',
                'name' => 'name',
                'lang' => false
            ),
            'cl' => array(
                'id' => 'id_category',
                'table' => 'category_lang',
                'name' => 'name',
                'lang' => true
            ),
            'attribute_lang' => array(
                'id' => 'id_attribute_group',
                'table' => 'attribute_group_lang',
                'name' => 'name',
                'filter_key' => 'attribute',
                'lang' => true
            ),
            'feature_value_lang' => array(
                'id' => 'id_feature',
                'table' => 'feature_lang',
                'name' => 'name',
                'filter_key' => 'feature_product',
                'lang' => true
            ),
        );
    }

    public function getSelect()
    {
        return $this->query['select'];
    }

    public function getJoin()
    {
        return $this->query['join'];
    }

    public function selectQuery(
        $select,
        $attribute_as,
        $filtering = null
    ) {
        if ($this->use_parent) {
            $this->select($select . " AS '" . pSQL($attribute_as) . "'");
        } else {
            if ($filtering == null) {
                $filtering = '`' . $attribute_as . '` LIKE \'%%%s%%\'';
            }
            $select = array(
                'select' => $select,
                'attribute_as' => $attribute_as,
                'filtering' => $filtering
            );
            $this->select($select);
        }
    }

    public function joinQuery(
        $type,
        $fromAlias,
        $join,
        $alias,
        $condition = null,
        $nested = false
    ) {
        if ($this->use_parent && !$nested) {
            $callback = $type . 'Join';
            parent::$callback($join, $alias, $condition);
        } elseif ($this->use_parent && $nested) {
            $this->join(
                "LEFT JOIN (" . $join . ") `" . pSQL($alias) . "` " . (!empty($condition) ? " ON " . $condition : '')
            );
        } else {
            $this->query['join'][] = array(
                'type' => $type,
                'fromAlias' => $fromAlias,
                'join' => $join,
                'alias' => $alias,
                'condition' => $condition,
                'nested' => $nested
            );
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function setDefaultJoin()
    {
        if (!in_array('product_shop', $this->already_join)) {
            $this->joinQuery(
                'inner',
                $this->getAlias('product'),
                'product_shop',
                $this->getAlias('product_shop'),
                "`" . $this->getAlias('product') . "`.`id_product` = 
                    `" . $this->getAlias('product_shop') . "`.`id_product`
                    AND `" . $this->getAlias('product_shop') . "`.`id_shop` = " . (int)$this->context->shop->id
            );

            $this->already_join[] = 'product_shop';
        }

        return $this;
    }

    public function addDefaultJoin($table)
    {
        switch ($table) {
            case 'category_lang':
                if (!in_array('category_lang', $this->already_join)) {
                    $this->joinQuery(
                        'left',
                        $this->getAlias('product_shop'),
                        'category_lang',
                        $this->getAlias('category_lang'),
                        "`" . $this->getAlias('product_shop') . "`.`id_category_default` =
                        `" . $this->getAlias('category_lang') . "`.`id_category`
                            AND `" . $this->getAlias('category_lang') . "`.`id_lang` =
                                '" . (int)$this->context->language->id . "'"
                    );

                    $this->already_join[] = 'category_lang';
                }

//                $this->joinQuery(
//                    'left',
//                    $this->getAlias('category_lang'),
//                    'category',
//                    $this->getAlias('category'),
//                    "`" . $this->getAlias('category_lang') . "`.`id_category` =
//                        `" . $this->getAlias('category') . "`.`id_category`"
//                );
                break;
            case 'shop':
                if (!in_array('shop', $this->already_join)) {
                    $id_shop = Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP ?
                        (int)$this->context->shop->id :
                        '`' . $this->getAlias('product') . '`.`id_shop_default`';

                    $this->joinQuery(
                        'left',
                        $this->getAlias('product'),
                        'shop',
                        $this->getAlias('shop'),
                        "`" . $this->getAlias('shop') . "`.`id_shop` = " . $id_shop
                    );

                    $this->already_join[] = 'shop';
                }
                break;
            case 'image':
                if (version_compare(_PS_VERSION_, '1.6.1.0', '<')) {
                    if (!in_array('image', $this->already_join)) {
                        $this->joinQuery(
                            'left',
                            $this->getAlias('product'),
                            'image',
                            $this->getAlias('image'),
                            "`" . $this->getAlias('product') . "`.`id_product` =
                                `" . $this->getAlias('image') . "`.`id_product`
                                AND `" . $this->getAlias('image') . "`.`cover` = '1'"
                        );

                        $this->already_join[] = 'image';
                    }

                    if (!in_array('image_shop', $this->already_join)) {
                        $this->joinQuery(
                            'left',
                            $this->getAlias('image'),
                            'image_shop',
                            $this->getAlias('image_shop'),
                            "`" . $this->getAlias('image') . "`.`id_image` =
                                `" . $this->getAlias('image_shop') . "`.`id_image`"
                        );

                        $this->already_join[] = 'image_shop';
                    }
                } else {
                    if (!in_array('image_shop', $this->already_join)) {
                        $this->joinQuery(
                            'left',
                            $this->getAlias('product'),
                            'image_shop',
                            $this->getAlias('image_shop'),
                            "`" . $this->getAlias('product') . "`.`id_product` =
                            `" . $this->getAlias('image_shop') . "`.`id_product`
                                AND `" . $this->getAlias('image_shop') . "`.`cover` = '1'"
                        );

                        $this->already_join[] = 'image_shop';
                    }

                    if (!in_array('image', $this->already_join)) {
                        $this->joinQuery(
                            'left',
                            $this->getAlias('image_shop'),
                            'image',
                            $this->getAlias('image'),
                            "`" . $this->getAlias('image_shop') . "`.`id_image` =
                                `" . $this->getAlias('image') . "`.`id_image`"
                        );

                        $this->already_join[] = 'image';
                    }
                }
                break;
            case 'product_download':
                if (!in_array('product_download', $this->already_join)) {
                    $this->joinQuery(
                        'left',
                        $this->getAlias('product'),
                        'product_download',
                        $this->getAlias('product_download'),
                        "`" . $this->getAlias('product') . "`.`id_product` =
                            `" . $this->getAlias('product_download') . "`.`id_product`
                            AND `" . $this->getAlias('product_download') . "`.`active` = 1"
                    );

                    $this->already_join[] = 'product_download';
                }
                break;
            case 'stock_available':
                if (!in_array('stock_available', $this->already_join)) {
                    $this->joinQuery(
                        'left',
                        $this->getAlias('product'),
                        'stock_available',
                        $this->getAlias('stock_available'),
                        "`" . $this->getAlias('product') . "`.`id_product` =
                            `" . $this->getAlias('stock_available') . "`.`id_product`
                            AND `" . $this->getAlias('stock_available') . "`.`id_product_attribute` = 0" .
                                StockAvailable::addSqlShopRestriction(null, null, $this->getAlias('stock_available'))
                    );

                    $this->already_join[] = 'stock_available';
                }
                break;
            case 'feature_product':
                if (!in_array('feature_product', $this->already_join)) {
                    $this->joinQuery(
                        'left',
                        $this->getAlias('product'),
                        'feature_product',
                        'feature_product',
                        "`" . $this->getAlias('product') . "`.`id_product` = `feature_product`.`id_product`"
                    );

                    $this->already_join[] = 'feature_product';
                }
                break;
            case 'manufacturer':
                if (!in_array('manufacturer', $this->already_join)) {
                    $this->joinQuery(
                        'left',
                        $this->getAlias('product'),
                        'manufacturer',
                        'manufacturer',
                        "`" . $this->getAlias('product') . "`.`id_manufacturer` = `manufacturer`.`id_manufacturer`"
                    );

                    $this->already_join[] = 'manufacturer';
                }
                break;
            case 'product_supplier':
                if (!in_array('product_supplier', $this->already_join)) {
                    $supplier_shop = new DbQuery();
                    $supplier_shop->select('`a`.*')
                        ->from('product_supplier', 'a')
                        ->innerJoin(
                            'supplier_shop',
                            'b',
                            "`a`.`id_supplier` = `b`.`id_supplier`" . Shop::addSqlRestriction(false, "b")
                        );

                    $this->joinQuery(
                        'left',
                        $this->getAlias('product'),
                        $supplier_shop,
                        'product_supplier',
                        "`" . $this->getAlias('product') . "`.`id_product` = `product_supplier`.`id_product`",
                        true
                    );

                    $this->already_join[] = 'product_supplier';
                }
                break;
            case 'supplier':
                $this->addDefaultJoin('product_supplier');
                if (!in_array('supplier', $this->already_join)) {
                    $this->joinQuery(
                        'left',
                        'product_supplier',
                        'supplier',
                        'supplier',
                        '`product_supplier`.`id_supplier` = `supplier`.`id_supplier`'
                    );

                    $this->already_join[] = 'supplier';
                }
                break;
            case 'category_product':
                if (!in_array('category_product', $this->already_join)) {
                    $categoryId = $this->categoryFiltered(false);
                    $where = $categoryId > 0 ?
                        " AND `" . $this->getAlias('category_product') . "`.`id_category` = " . (int)$categoryId :
                        "";
                    $this->joinQuery(
                        'inner',
                        $this->getAlias('product'),
                        'category_product',
                        $this->getAlias('category_product'),
                        "`" . $this->getAlias('product') . "`.`id_product` =
                            `" . $this->getAlias('category_product') . "`.`id_product`" . $where
                    );

                    $this->already_join[] = 'category_product';
                }
                break;
            case 'categories_category_product':
                if (!in_array('categories_category_product', $this->already_join)) {
                    $this->joinQuery(
                        'left',
                        $this->getAlias('product'),
                        'category_product',
                        $this->getAlias('categories_category_product'),
                        "`" . $this->getAlias('product') . "`.`id_product` =
                            `" . $this->getAlias('categories_category_product') . "`.`id_product`"
                    );

                    $this->already_join[] = 'categories_category_product';
                }
                break;
            case 'product_carrier':
                if (!in_array('product_carrier', $this->already_join)) {
                    $this->joinQuery(
                        'left',
                        $this->getAlias('product'),
                        'product_carrier',
                        'product_carrier',
                        "`" . $this->getAlias('product') . "`.`id_product` = `product_carrier`.`id_product`"
                    );

                    $this->already_join[] = 'product_carrier';
                }
                break;
            case 'product_attribute':
                if (!in_array('product_attribute', $this->already_join)) {
                    $this->joinQuery(
                        'left',
                        $this->getAlias('product'),
                        'product_attribute',
                        'product_attribute',
                        "`" . $this->getAlias('product') . "`.`id_product` = `product_attribute`.`id_product`"
                    );

                    $this->already_join[] = 'product_attribute';
                }
                break;
            case 'product_attribute_combination':
                $this->addDefaultJoin('product_attribute');
                if (!in_array('product_attribute_combination', $this->already_join)) {
                    $this->joinQuery(
                        'left',
                        'product_attribute',
                        'product_attribute_combination',
                        'product_attribute_combination',
                        '`product_attribute`.`id_product_attribute` = 
                            `product_attribute_combination`.`id_product_attribute`'
                    );

                    $this->already_join[] = 'product_attribute_combination';
                }
                break;
            case 'attribute':
                $this->addDefaultJoin('product_attribute_combination');
                if (!in_array('attribute', $this->already_join)) {
                    $this->joinQuery(
                        'left',
                        'product_attribute_combination',
                        'attribute',
                        'attribute',
                        '`product_attribute_combination`.`id_attribute` = `attribute`.`id_attribute`'
                    );

                    $this->already_join[] = 'attribute';
                }
                break;
        }
    }

    /**
     * @param bool $editable
     * @return string
     */
    public function getDefaultWhere()
    {
        $where = "";
        if (version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            $where .= " AND `" . $this->getAlias('product') . "`.`state` = '1'";
        }

//        $id_category = (int)$this->categoryFiltered();
//        if (!$editable && $id_category > 0) {
//            $where .= " AND `" . $this->getAlias('category_product') . "`.`id_category` = " . (int)$id_category;
//        }

        return $where;
    }

    /**
     * @return string
     */
    public function getDefaultGroupBy()
    {
        return "GROUP BY `" . $this->getAlias('product') . "`.`id_product`";
    }

    /**
     * @return string
     */
    public function getDefaultOrderBy()
    {
        return $this->categoryFiltered(true) ? 'position' : 'id_product';
    }

    /**
     * @return string
     */
    public function getDefaultOrderWay()
    {
        return $this->categoryFiltered(true) ? 'ASC' : 'DESC';
    }

    public function getNotEditable()
    {
        return array(
            // 'productfinal_price',
            'manufacturername',
            'suppliername',
            'feature_value_langvalue',
            'attribute_langname',
            'category_langname',
            'image',
//            'customproductslistfinal_price_discount',
//            'customproductslistdiscount_reduction',
//            'customproductslistunit_price',
        );
    }

    public static function getColumns($only_editable = false)
    {
        $where = "1";
        if ($only_editable) {
            $where = "type = 'editable'";
        }

        return Db::getInstance()->executeS(
            "SELECT `table`, `attribute`, `title`, `type`
            FROM `" . _DB_PREFIX_ . "customproductslist`
            WHERE 1 AND " . $where . "
            ORDER BY `position`"
        );
    }

    public function getAttributeAs($table, $attribute)
    {
        if ($table == "feature_name" || $table == "attribute_group_name") {
            $attribute_as = Tools::substr(hash("md5", pSQL($attribute)), 0, 10);
        } else {
            $attribute_as = $this->stripAccents(pSQL($table) . pSQL($attribute));
        }

        if (array_key_exists($attribute_as, $this->attributes_as_list)) {
            $attribute_as = $this->attributes_as_list[$attribute_as];
        }

        return $attribute_as;
    }

    public function setUseParent($use_parent)
    {
        $this->use_parent = (bool)$use_parent;

        return $this;
    }

    public function setWithAliases($with_aliases)
    {
        $this->with_aliases = (bool)$with_aliases;

        return $this;
    }

    public function getAlias($table)
    {
        if ($this->with_aliases && array_key_exists($table, $this->aliases)) {
            return $this->aliases[$table];
        } else {
            return $table;
        }
    }

    public function printEditableField0($value, $tr)
    {
        return $this->printEditableField($value, $tr, 0);
    }

    public function printEditableField1($value, $tr)
    {
        return $this->printEditableField($value, $tr, 1);
    }

    public function printEditableField2($value, $tr)
    {
        return $this->printEditableField($value, $tr, 2);
    }

    public function printEditableField3($value, $tr)
    {
        return $this->printEditableField($value, $tr, 3);
    }

    public function printEditableField4($value, $tr)
    {
        return $this->printEditableField($value, $tr, 4);
    }

    public function printEditableField5($value, $tr)
    {
        return $this->printEditableField($value, $tr, 5);
    }

    public function printEditableField6($value, $tr)
    {
        return $this->printEditableField($value, $tr, 6);
    }

    public function printEditableField7($value, $tr)
    {
        return $this->printEditableField($value, $tr, 7);
    }

    public function printEditableField8($value, $tr)
    {
        return $this->printEditableField($value, $tr, 8);
    }

    public function printEditableField9($value, $tr)
    {
        return $this->printEditableField($value, $tr, 9);
    }

    public function printEditableField10($value, $tr)
    {
        return $this->printEditableField($value, $tr, 10);
    }

    public function printEditableField($value, $tr, $n_editable)
    {
        $fields = CPL::getColumns(true);

        if (array_key_exists($n_editable, $fields)) {
            $field = $fields[$n_editable];
        } else {
            return $value;
        }

        $this->context->smarty->assign(array(
            'cpl_editable' => array(
                'id_product' => $tr['id_product'],
                'table' => $field['table'],
                'attribute' => $field['attribute'],
                'value' => $value
            )
        ));

        return $this->context->smarty->fetch(
            _PS_MODULE_DIR_ . 'customproductslist/views/templates/admin/list/editable_field.tpl'
        );
    }

    public function printBooleanField($value)
    {
        $this->context->smarty->assign(array(
            'cpl_boolean' => array(
                'value' => (bool)$value
            )
        ));

        return $this->context->smarty->fetch(
            _PS_MODULE_DIR_ . 'customproductslist/views/templates/admin/list/boolean16_field.tpl'
        );
    }

    abstract protected function categoryFiltered($check = true);

    protected function handlePosition()
    {
        if ($this->categoryFiltered(true)) {
            $first_position = (int)Db::getInstance()->getValue(
                "SELECT MIN(`position`)
                FROM `" . _DB_PREFIX_ . "category_product`"
            );
//            $first_position = (0-$min_position)*(-1);
//            if (version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
//                $first_position = 1;
//            }

            $this->selectQuery(
                "(`" . $this->getAlias('category_product') . "`.`position`-" . (int)$first_position . ")",
                'position'
            );
            $this->addDefaultJoin('category_product');
        }
    }

    protected function stripAccents($str)
    {
        return strtr(
            utf8_decode($str),
            utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'),
            'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'
        );
    }
}
