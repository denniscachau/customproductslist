<?php
/**
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class CPLOverride extends CPL
{
    public function __construct()
    {
        $this->list = "override";
        parent::__construct();
    }

    protected function setJoin()
    {
        $this->already_join[] = 'product';
        $this->already_join[] = 'product_lang';
        $this->already_join[] = 'product_shop';
        $this->already_join[] = "customproductslist";

        $tables = array_unique($this->config_tables);
        $ndefault_tables = array();
        foreach ($tables as $table) {
            foreach ($this->default_tables as $default_table) {
                switch ($table) {
                    case $default_table:
                        $this->addDefaultJoin($table);
                        continue 3;
                    case 'tag':
                        if (!in_array('product_tag', $this->already_join)) {
                            if (version_compare(_PS_VERSION_, '1.6.1.0', '>=')) {
                                $tag_on = " AND `product_tag`.`id_lang` = '" . (int)$this->context->language->id . "'";
                            } else {
                                $tag_on = "";
                            }

                            $this->joinQuery(
                                'left',
                                $this->getAlias('product'),
                                'product_tag',
                                'product_tag',
                                $this->getAlias('product') . '.`id_product` = `product_tag`.`id_product`' . $tag_on
                            );

                            $this->already_join[] = 'product_tag';
                        }

                        if (!in_array('tag', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                'product_tag',
                                'tag',
                                'tag',
                                '`product_tag`.`id_tag` = `tag`.`id_tag`
                                    AND `tag`.`id_lang` = ' . (int)$this->context->language->id
                            );

                            $this->already_join[] = 'tag';
                        }

                        continue 3;
                    case 'attribute_group_name':
                    case 'attribute_lang':
                        $this->addDefaultJoin('product_attribute_combination');

                        if (!in_array('attribute_lang', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                'product_attribute_combination',
                                'attribute_lang',
                                'attribute_lang',
                                '`product_attribute_combination`.`id_attribute` = `attribute_lang`.`id_attribute`
                                    AND `attribute_lang`.`id_lang` = ' . (int)$this->context->language->id
                            );

                            $this->already_join[] = 'attribute_lang';
                        }

                        if (!in_array('attribute', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                'product_attribute_combination',
                                'attribute',
                                'attribute',
                                '`product_attribute_combination`.`id_attribute` = `attribute`.`id_attribute`'
                            );

                            $this->already_join[] = 'attribute';
                        }
                        continue 3;
                    case 'carrier':
                        $this->addDefaultJoin('product_carrier');
                        if (!in_array('carrier', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                'product_carrier',
                                'carrier',
                                'carrier',
                                '`product_carrier`.`id_carrier_reference` = `carrier`.`id_reference`
                                    AND `carrier`.`deleted` = 0'
                            );

                            $this->already_join[] = 'carrier';
                        }
                        continue 3;
                }
            }
            $ndefault_tables[] = $table;
        }

        foreach ($ndefault_tables as $value) {
            if (empty($value) || in_array($value, $this->already_join)) {
                continue;
            }
            $columns = Db::getInstance()->executeS(
                "SHOW COLUMNS FROM `" . _DB_PREFIX_ . pSQL($value) . "`"
            );
            $base_on = '';
            $join_on = '';
            $join_table = '';
            $lang = false;

            foreach ($this->join_ids as $join_table_temp => $field) {
                if (is_array($field)) {
                    $join_table_temp = $field['table'];
                } else {
                    $field = array(
                        'base_on' => $field,
                        'join_on' => $field,
                    );
                }

                foreach ($columns as $column) {
                    if ($column['Field'] == $field['join_on'] && $join_on == '') {
                        $base_on = $field['base_on'];
                        $join_on = $field['join_on'];
                        $join_table = $join_table_temp;
                        $this->addDefaultJoin($join_table);
                    } elseif ($column['Field'] == 'id_lang') {
                        $lang = true;
                    }

                    if ($lang && $join_on != '') {
                        break 2;
                    }
                }
            }
            $this->already_join[] = $value;
            $on = '`' . pSQL($value) . '`.`' . pSQL($join_on) . '` =
                    `' . pSQL($join_table) . '`.`' . pSQL($base_on) . '`';
            if ($lang && preg_match('/.*_lang$/', $value)) {
                $on .= " AND `" . pSQL($value) . "`.`id_lang` = '" . (int)$this->context->language->id . "'";
            }

            $this->joinQuery(
                'left',
                pSQL($join_table),
                pSQL($value),
                pSQL($value),
                $on
            );
        }
    }

    protected function setSelect()
    {
        foreach ($this->attributes as $key => $attribute) {
            if (!array_key_exists($key, $this->config_tables) ||
                empty($this->config_tables[$key]) ||
                empty($attribute)) {
                unset($this->attributes[$key]);
                continue;
            }

            $attribute_as = $this->getAttributeAs($this->config_tables[$key], $attribute);

            $default_select = false;

            switch ($this->config_tables[$key]) {
                case 'customproductslist':
                    switch ($attribute) {
                        case 'final_price_discount_tax_excl':
                        case 'final_price_discount':
                            $this->selectQuery(
                                "'0'",
                                $attribute_as
                            );
                            break;
                        case 'discount_reduction':
                            // TODO get with callback
                            $this->selectQuery(
                                "IF(
                                    `specific_price`.`reduction_type` = 'amount',
                                    CONCAT(
                                        ROUND(`specific_price`.`reduction`, 2),
                                        ' " . pSQL($this->context->currency->getSign()) . "'
                                    ),
                                    CONCAT(
                                        ROUND(`specific_price`.`reduction` * 100, 2)
                                        , ' %'
                                    )
                                )",
                                $attribute_as
                            );

                            if (!in_array('specific_price', $this->already_join)) {
                                $this->joinQuery(
                                    'left',
                                    $this->getAlias('product'),
                                    'specific_price',
                                    'specific_price',
                                    "`a`.`id_product` = `specific_price`.`id_product`
                                    AND `specific_price`.`id_product_attribute` = 0
                                    AND `specific_price`.`id_group` = 0
                                    AND `specific_price`.`id_customer` = 0
                                    AND (`specific_price`.`from` <= NOW() OR `specific_price`.`from` = ''
                                        OR `specific_price`.`from` IS NULL)
                                    AND (`specific_price`.`to` >= NOW() OR `specific_price`.`to` = ''
                                        OR `specific_price`.`to` = 0 OR `specific_price`.`to` IS NULL)"
                                );

                                $this->already_join[] = 'specific_price';
                            }

                            $this->types[$key] = "";
                            break;
                        case 'categories':
                            $this->selectQuery(
                                "GROUP_CONCAT(DISTINCT `cpl_categories_lang`.`name` SEPARATOR ', ')",
                                $attribute_as
                            );

                            $this->addDefaultJoin('category_product');
                            if (!in_array('cpl_categories_lang', $this->already_join)) {
                                $this->joinQuery(
                                    'left',
                                    $this->getAlias('category_product'),
                                    'category_lang',
                                    'cpl_categories_lang',
                                    "`" . $this->getAlias('category_product') . "`.`id_category` =
                                        `cpl_categories_lang`.`id_category`
                                        AND `cpl_categories_lang`.`id_lang` = " . (int)$this->context->language->id
                                );

                                $this->already_join[] = 'cpl_categories_lang';
                            }

                            $this->tables[$key] = 'cpl_categories_lang';
                            $attribute = 'name';
                            break;
                        case 'unit_price':
                            $this->selectQuery(
                                "MAX(
                                    `" . $this->getAlias('product_shop') . "`.`price` / 
                                    `" . $this->getAlias('product_shop') . "`.`unit_price_ratio`)",
                                $attribute_as
                            );
                            break;
                    }
                    break;
                case 'image':
                    $this->selectQuery(
                        "`" . pSQL($this->tables[$key]) . "`.`id_image`",
                        'id_image'
                    );
                    break;
                case 'feature_name':
                    $id_feature_value = (int)Db::getInstance()->getValue(
                        "SELECT `id_feature`
                        FROM `" . _DB_PREFIX_ . "feature_lang`
                        WHERE `name` LIKE '" . pSQL($attribute) . "'"
                    );

                    if ($id_feature_value > 0) {
                        $this->addDefaultJoin('feature_product');

                        $on = "`table_" . $attribute_as . "`.`id_feature_value`=`feature_product`.`id_feature_value`
                            AND `table_" . $attribute_as . "`.`id_lang` = " . (int)$this->context->language->id . "
                            AND `feature_product`.`id_feature` = " . (int)$id_feature_value;

                        $this->joinQuery(
                            'left',
                            'feature_product',
                            'feature_value_lang',
                            'table_' . $attribute_as,
                            $on
                        );

                        $this->selectQuery(
                            "GROUP_CONCAT(DISTINCT `table_" . pSQL($attribute_as) . "`.`value`
                                SEPARATOR ', ')",
                            $attribute_as
                        );
                    }
                    $this->tables[$key] = 'table_' . pSQL($attribute_as);
                    $attribute = 'value';
                    break;
                case 'attribute_group_name':
                    $id_attribute_group = (int)Db::getInstance()->getValue(
                        "SELECT `id_attribute_group`
                        FROM `" . _DB_PREFIX_ . "attribute_group_lang`
                        WHERE `name` LIKE '" . pSQL($attribute) . "'"
                    );

                    if ($id_attribute_group > 0) {
                        $this->addDefaultJoin('product_attribute_combination');
                        $this->addDefaultJoin('attribute');

                        $on =
                            "`table_" . $attribute_as . "`.`id_attribute`=`product_attribute_combination`.`id_attribute`
                            AND `table_" . $attribute_as . "`.`id_lang` = " . (int)$this->context->language->id . "
                            AND `attribute`.`id_attribute_group` = " . (int)$id_attribute_group;

                        $this->joinQuery(
                            'left',
                            'product_attribute_combination',
                            'attribute_lang',
                            'table_' . $attribute_as,
                            $on
                        );

                        $this->selectQuery(
                            "GROUP_CONCAT(DISTINCT `table_" . pSQL($attribute_as) . "`.`name`
                                SEPARATOR ', ')",
                            $attribute_as
                        );
                    }
                    $this->tables[$key] = 'table_' . pSQL($attribute_as);
                    $attribute = 'name';
                    break;

                default:
                    switch ($attribute_as) {
                        case 'price_final':
                        case 'productfinal_price':
                            $this->selectQuery(
                                "MAX(`" . $this->getAlias('product_shop') . "`.`price`)",
                                $attribute_as
                            );
                            break;
                        case 'carriername':
                            $this->selectQuery(
                                "GROUP_CONCAT(
                                    DISTINCT IF(`" . pSQL($this->tables[$key]) . "`.`" . pSQL($attribute) . "` = '0',
                                    '" . pSQL(Configuration::get('PS_SHOP_NAME')) . "',
                                    `" . pSQL($this->tables[$key]) . "`.`" . pSQL($attribute) . "`)
                                    SEPARATOR ', ')",
                                $attribute_as
                            );
                            break;
                        case 'position':
                            if (!$this->categoryFiltered(false)) {
                                unset($this->attributes[$key]);
                                continue 3;
                            }
                            break;

                        default:
                            if (!empty($this->types[$key]) &&
                                ($this->types[$key] == 'price' ||
                                    $this->types[$key] == 'percent' ||
                                    $this->types[$key] == 'integer' ||
                                    $this->types[$key] == 'decimal')) {
                                $this->selectQuery(
                                    "MAX(`" . pSQL($this->tables[$key]) . "`.`" . pSQL($attribute) . "`)",
                                    $attribute_as,
                                    '`' . $attribute_as . '` %s'
                                );
                            } elseif (!empty($this->types[$key]) && $this->types[$key] == 'boolean') {
                                $this->selectQuery(
                                    "`" . pSQL($this->tables[$key]) . "`.`" . pSQL($attribute) . "`",
                                    $attribute_as,
                                    '`' . $attribute_as . '` = %d'
                                );
                            } elseif (!empty($this->types[$key]) && $this->types[$key] != 'editable') {
                                $this->selectQuery(
                                    "`" . pSQL($this->tables[$key]) . "`.`" . pSQL($attribute) . "`",
                                    $attribute_as
                                );
                            } else {
                                $default_select = true;
                            }
                            break;
                    }
                    break;
            }

            if ($default_select) {
                $this->selectQuery(
                    "GROUP_CONCAT(" .
                    "DISTINCT `" . pSQL($this->tables[$key]) . "`.`" . pSQL($attribute) . "` " .
                    "SEPARATOR ', ')",
                    $attribute_as
                );
            }
        }
    }

    public function getFields()
    {
        if ($this->isFieldsSet) {
            return $this->fields;
        }

        $this->getConfigurations();
        $count_editable = 0;

        $this->fields['id_product'] = array(
            'title' => 'ID',
            'align' => 'form-check-label',
            'class' => 'fixed-width-xs',
            'label' => true,
            'type' => 'int',
            'default' => '',
            // 'style' => 'width:5rem;',
            'input_name' => 'id_product'
        );
        foreach ($this->attributes as $key => $attribute) {
            $attribute_as = $this->getAttributeAs($this->config_tables[$key], $attribute);
            $class = 'cpl-' . $attribute_as;
            $havingFilter = true;
            $this->fields[$attribute_as]['default'] = '';
            $this->fields[$attribute_as]['url'] = false;
            $this->fields[$attribute_as]['input_name'] = $attribute_as;

            switch ($this->aligns[$key]) {
                case '0':
                    $align = 'text-left';
                    break;
                case '2':
                    $align = 'text-right';
                    break;
                default:
                    $align = 'text-center';
                    break;
            }

            $this->fields[$attribute_as]['align'] = $align;
            if (!empty($this->titles[$key])) {
                $this->fields[$attribute_as]['title'] = $this->titles[$key];
            } else {
                $this->fields[$attribute_as]['title'] = $attribute;
            }

            switch ($this->types[$key]) {
                case 'boolean':
                    $data_type = Db::getInstance()->getValue(
                        "SELECT `data_type`
                        FROM `information_schema`.`COLUMNS`
                        WHERE `table_schema` = '" . _DB_NAME_ . "'
                        AND `table_name` = '" . _DB_PREFIX_ . pSQL($this->config_tables[$key]) . "'
                        AND `column_name` = '" . pSQL($attribute) . "'"
                    );

                    if ($data_type == "tinyint" &&
                        (version_compare(_PS_VERSION_, '1.7.0.0', '>') || !Configuration::get('CPL_use_default'))) {
                        $this->fields[$attribute_as]['type'] = 'bool';
                        $this->fields[$attribute_as]['active'] = $this->config_tables[$key] . ';;;' . $attribute;
                        $this->fields[$attribute_as]['ajax'] = true;
                    } elseif ($data_type == "tinyint") {
                        $this->fields[$attribute_as]['type'] = 'bool';
                        $this->fields[$attribute_as]['callback'] = 'printBooleanField';
                        $this->fields[$attribute_as]['callback_object'] = $this;
                    }
                    break;
                case 'price':
                    $this->fields[$attribute_as]['type'] = 'price';
                    break;
                case 'percent':
                    $this->fields[$attribute_as]['type'] = 'percent';
                    break;
                case 'decimal':
                    $this->fields[$attribute_as]['type'] = 'decimal';
                    break;
                case 'integer':
                    $this->fields[$attribute_as]['type'] = 'int';
                    break;
                case 'date':
                    $this->fields[$attribute_as]['type'] = 'date';
                    $this->fields[$attribute_as]['format'] = $this->context->language->date_format_lite;
                    $this->fields[$attribute_as]['search'] = false;
                    break;
                case 'datetime':
                    $this->fields[$attribute_as]['type'] = 'datetime';
                    $this->fields[$attribute_as]['format'] = $this->context->language->date_format_full;
                    $this->fields[$attribute_as]['search'] = false;
                    break;
                case 'editable':
                    if (!in_array($attribute_as, $this->not_editable) &&
                        $this->config_tables[$key] != 'feature_name' &&
                        $this->config_tables[$key] != 'attribute_group_name' &&
                        $this->config_tables[$key] != 'customproductslist') {
                        if ($count_editable <= 10) {
                            $class .= ' cpl-editable';
                            $this->fields[$attribute_as]['callback'] = 'printEditableField' . $count_editable;
                            $this->fields[$attribute_as]['callback_object'] = $this;
                        }
                    }
                    $count_editable++;
                    break;
                case 'select':
                    // id = id_feature, table = feature_lang, filter_key = feature_product
                    if (in_array($this->tables[$key], array_keys($this->select_fields))) {
                        $select_values = Db::getInstance()->executeS(
                            "SELECT `" . pSQL($this->select_fields[$this->tables[$key]]['id']) .
                            "` AS 'id',
                                `" . pSQL($this->select_fields[$this->tables[$key]]['name']) .
                            "` AS `name`
                                FROM `" . _DB_PREFIX_ . pSQL(
                                $this->select_fields[$this->tables[$key]]['table']
                            ) . "`" . ($this->select_fields[$this->tables[$key]]['lang'] ?
                                "WHERE `id_lang` = '" . (int)$this->context->language->id . "'" :
                                "") . " ORDER BY `name` ASC"
                        );

                        $select_display = array();
                        foreach ($select_values as $select_value) {
                            $select_display[$select_value['id']] = $select_value['name'];
                        }

                        $this->fields[$attribute_as]['type'] = 'select';
                        $this->fields[$attribute_as]['list'] = $select_display;
                        $this->fields[$attribute_as]['filter_key'] =
                            (isset($this->select_fields[$this->tables[$key]]['filter_key']) ?
                                $this->select_fields[$this->tables[$key]]['filter_key'] :
                                $this->tables[$key]) . '!' . $this->select_fields[$this->tables[$key]]['id'];
                        continue 2;
                    }
                    break;
            }

            switch ($attribute_as) {
                case 'active':
                case 'product_shopactive':
                    $this->fields[$attribute_as]['active'] = 'status';
                    $this->fields[$attribute_as]['onclick'] = 'unitProductAction';
                    $this->fields[$attribute_as]['type'] = 'bool';
                    break;
                case 'image':
                    $this->fields[$attribute_as]['search'] = false;
                    $this->fields[$attribute_as]['orderby'] = false;
                    $this->fields[$attribute_as]['image'] = 'p';
                    break;
                case 'price_final':
                case 'productfinal_price':
                    $this->fields[$attribute_as]['search'] = false;
                    $this->fields[$attribute_as]['url'] = true;
                    $this->fields[$attribute_as]['step'] = '2';
                    $this->fields[$attribute_as]['default'] = 'N/A';
                    break;
                case 'price':
                    $this->fields[$attribute_as]['url'] = true;
                    $this->fields[$attribute_as]['step'] = '2';
                    $this->fields[$attribute_as]['default'] = 'N/A';
                    break;
                case 'stock_availablequantity':
                    $this->fields[$attribute_as]['url'] = true;
                    $this->fields[$attribute_as]['step'] = '3';
                    $this->fields[$attribute_as]['default'] = 'N/A';
                    break;
                case 'product_langname':
                    $this->fields[$attribute_as]['url'] = true;
                    $this->fields[$attribute_as]['step'] = '1';
                    $this->fields[$attribute_as]['default'] = 'N/A';
                    break;
                case 'position':
                    $this->fields[$attribute_as]['position'] = 'position';
                    $this->fields[$attribute_as]['filter_key'] = $this->getAlias('category_product') . '!position';
                    $this->fields[$attribute_as]['order_by'] = $this->getAlias('category_product') . '!position';
                    $class .= ' fixed-width-xs';
                    break;

                default:
                    if ($this->tables[$key] == 'customproductslist') {
                        $this->fields[$attribute_as]['search'] = false;
                        $this->fields[$attribute_as]['orderby'] = false;
                    }
                    break;
            }

            if ($havingFilter) {
                $this->fields[$attribute_as]['havingFilter'] = true;
            } else {
                $this->fields[$attribute_as]['filter_key'] = $this->tables[$key] . '!' . $attribute;
            }
            $this->fields[$attribute_as]['class'] = $class;
        }
        return $this->fields;
    }

    protected function categoryFiltered($check = true)
    {
        if ($check && !in_array('position', $this->attributes)) {
            return 0;
        }
        $filter = array(
            'employee' => (int)$this->context->employee->id,
            'shop' => (int)$this->context->shop->id,
            'controller' => 'ProductController',
            'action' => 'catalogAction',
        );

        $where = array();
        foreach ($filter as $field => $value) {
            $where[] = "`" . $field . "` = '" . $value . "'";
        }

        $where = implode(' AND ', $where);

        $res = json_decode(Db::getInstance()->getValue(
            "SELECT `filter`
            FROM `" . _DB_PREFIX_ . "admin_filter`
            WHERE " . $where
        ), true);

        return (int)$res['filter_category'];
    }

    protected function handlePosition()
    {
        if ($this->categoryFiltered(true)) {
            $this->selectQuery(
                "`" . $this->getAlias('category_product') . "`.`position`",
                'position'
            );
            $this->addDefaultJoin('category_product');
        }
    }
}
