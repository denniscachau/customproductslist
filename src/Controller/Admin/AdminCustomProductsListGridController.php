<?php
/**
 * 2007-2023 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2023 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

namespace PrestaShop\Module\CustomProductsList\Controller\Admin;

use CPL;
use Module;
use PrestaShopBundle\Controller\Admin\ProductController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class AdminCustomProductsListGridController extends ProductController
{
    /**
     * @Template("@PrestaShop/Admin/Product/CatalogPage/catalog.html.twig")
     *
     * {@inheritDoc}
     */
    public function catalogAction(
        Request $request,
        $limit = 10,
        $offset = 0,
        $orderBy = 'id_product',
        $sortOrder = 'desc'
    ) {
        $output = parent::catalogAction($request, $limit, $offset, $orderBy, $sortOrder);
        if (!Module::isEnabled('customproductslist') ||
            !Module::getInstanceByName('customproductslist')->isActive('override')) {
            return $output;
        }

        if (is_array($output)) {
            return array_merge(
                $output,
                array(
                    'cpl' => CPL::getInstance('override')->getFields()
                )
            );
        } else {
            return $output;
        }
    }

    /**
     * @Template("@PrestaShop/Admin/Product/CatalogPage/Lists/list.html.twig")
     *
     * {@inheritDoc}
     */
    public function listAction(
        Request $request,
        $limit = 10,
        $offset = 0,
        $orderBy = 'id_product',
        $sortOrder = 'asc',
        $view = 'full'
    ) {
        $output = parent::listAction($request, $limit, $offset, $orderBy, $sortOrder, $view);

        if (!Module::isEnabled('customproductslist') ||
            !Module::getInstanceByName('customproductslist')->isActive('override')) {
            return $output;
        }

        if (is_array($output)) {
            return array_merge(
                $output,
                array(
                    'cpl' => CPL::getInstance('override')->getFields()
                )
            );
        } else {
            return $output;
        }
    }
}
