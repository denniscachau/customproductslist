<?php
/**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

namespace PrestaShop\Module\CustomProductsList\Entity;

use CPL;
use Module;
use PrestaShopBundle\Entity\AdminFilter;

class CPLAdminFilter extends AdminFilter
{
    /**
     * {@inheritdoc}
     */
    public static function sanitizeFilterParameters(array $filter)
    {
        $filterMinMax = function ($filter) {
            return function ($subject) use ($filter) {
                $operator = null;

                if (false !== strpos($subject, '<=')) {
                    $operator = '<=';
                }

                if (false !== strpos($subject, '>=')) {
                    $operator = '>=';
                }

                if (null === $operator) {
                    $pattern = '#BETWEEN (?P<min>\d+\.?\d*) AND (?P<max>\d+\.?\d*)#';
                    if (0 === preg_match($pattern, $subject, $matches)) {
                        return '';
                    }

                    return sprintf('BETWEEN %f AND %f', $matches['min'], $matches['max']);
                } else {
                    $subjectWithoutOperator = str_replace($operator, '', $subject);

                    $flag = FILTER_DEFAULT;
                    if ($filter === FILTER_SANITIZE_NUMBER_FLOAT) {
                        $flag = FILTER_FLAG_ALLOW_FRACTION;
                    }

                    $filteredSubjectWithoutOperator = filter_var($subjectWithoutOperator, $filter, $flag);
                    if (!$filteredSubjectWithoutOperator) {
                        $filteredSubjectWithoutOperator = 0;
                    }

                    return $operator . $filteredSubjectWithoutOperator;
                }
            };
        };

        $cpl_filters = array();

        if (Module::isEnabled('customproductslist') &&
            Module::getInstanceByName('customproductslist')->isActive('override')) {
            $cpl_fields = CPL::getInstance('override')->getFields();
            foreach ($cpl_fields as $cpl_field) {
                if (!array_key_exists('input_name', $cpl_field)) {
                    return array();
                }
                $key = 'filter_column_' . $cpl_field['input_name'];
                $cpl_field['type'] = array_key_exists('type', $cpl_field) ? $cpl_field['type'] : '';
                switch ($cpl_field['type']) {
                    case 'bool':
                        $filter_type = FILTER_SANITIZE_NUMBER_INT;
                        break;
                    case 'price':
                    case 'int':
                        $filter_type = array(
                            'filter' => FILTER_CALLBACK,
                            'options' => $filterMinMax(FILTER_SANITIZE_NUMBER_INT),
                        );
                        break;
                    case 'decimal':
                        $filter_type = array(
                            'filter' => FILTER_CALLBACK,
                            'options' => $filterMinMax(FILTER_SANITIZE_NUMBER_FLOAT),
                        );
                        break;
                    case 'date':
                    case 'datetime':
                    default:
                        $filter_type = FILTER_SANITIZE_STRING;
                        break;
                }
                $cpl_filters[$key] = $filter_type;
            }
        }

        return filter_var_array($filter, array_merge(array(
            'filter_category' => FILTER_SANITIZE_NUMBER_INT,
            'filter_column_id_product' => array(
                'filter' => FILTER_CALLBACK,
                'options' => $filterMinMax(FILTER_SANITIZE_NUMBER_INT),
            ),
            'filter_column_name' => FILTER_SANITIZE_STRING,
            'filter_column_reference' => FILTER_SANITIZE_STRING,
            'filter_column_name_category' => FILTER_SANITIZE_STRING,
            'filter_column_price' => array(
                'filter' => FILTER_CALLBACK,
                'options' => $filterMinMax(FILTER_SANITIZE_NUMBER_FLOAT),
            ),
            'filter_column_sav_quantity' => array(
                'filter' => FILTER_CALLBACK,
                'options' => $filterMinMax(FILTER_SANITIZE_NUMBER_INT),
            ),
            'filter_column_active' => FILTER_SANITIZE_NUMBER_INT,
            'last_offset' => FILTER_SANITIZE_NUMBER_INT,
            'last_limit' => FILTER_SANITIZE_NUMBER_INT,
            'last_orderBy' => FILTER_SANITIZE_STRING,
            'last_sortOrder' => FILTER_SANITIZE_STRING,
        ), $cpl_filters));
    }
}
