<?php
/**
 * 2007-2023 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2023 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

namespace PrestaShop\Module\CustomProductsList\Core\Product;

use CPL;
use Module;
use PrestaShop\PrestaShop\Core\Product\ProductCsvExporter;
use PrestaShop\PrestaShop\Core\Product\ProductExporterInterface;
use PrestaShopBundle\Component\CsvResponse;
use PrestaShopBundle\Service\DataProvider\Admin\ProductInterface as ProductDataProviderInterface;

/**
 * Used to export list of Products in CSV in the Product list page.
 */
class CPLProductCsvExporter implements ProductExporterInterface
{
    /**
     * @var ProductDataProviderInterface
     */
    private $productProvider;

    /**
     * @var ProductCsvExporter
     */
    private $decoratedController;

    public function __construct(ProductCsvExporter $decoratedController, ProductDataProviderInterface $productProvider)
    {
        $this->decoratedController = $decoratedController;
        $this->productProvider = $productProvider;
    }

    /**
     * In this specific case, we don't need to pass a products list.
     *
     * @param array $products
     *
     * @return CsvResponse
     *
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function export(array $products = [])
    {
        $csv_response = $this->decoratedController->export($products);

        if (!Module::isEnabled('customproductslist') ||
            !Module::getInstanceByName('customproductslist')->isActive('override')) {
            return $csv_response;
        }

        $cpl_fields = CPL::getInstance('override')->getFields();

        $headersData = array();
        foreach ($cpl_fields as $key => $field) {
            if ($key == 'image') {
                $key = 'image_link';
            }
            $headersData[$key] = $field['title'];
        }

        return $csv_response->setHeadersData($headersData);
    }
}
