<?php
/**
 * 2007-2023 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2023 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

namespace PrestaShop\Module\CustomProductsList\Adapter\Product;

use AppKernel;
use Configuration;
use Context;
use CPL;
use Currency;
use Db;
use Doctrine\ORM\EntityManager;
use Hook;
use Module;
use PrestaShop\PrestaShop\Adapter\Admin\AbstractAdminQueryBuilder;
use PrestaShop\PrestaShop\Adapter\ImageManager;
use PrestaShop\PrestaShop\Adapter\Product\AdminProductDataProvider;
use PrestaShop\PrestaShop\Adapter\Validate;
use PrestaShop\Module\CustomProductsList\Entity\CPLAdminFilter;
use PrestaShopBundle\Entity\AdminFilter;
use PrestaShopBundle\Service\DataProvider\Admin\ProductInterface;
use Product;
use Psr\Cache\CacheItemPoolInterface;
use StockAvailable;
use Symfony\Component\Process\Exception\LogicException;
use Tools;

class CPLDataProvider extends AbstractAdminQueryBuilder implements ProductInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var string|null
     */
    private $lastCompiledSql = null;

    /**
     * @var ImageManager
     */
    private $imageManager;

    /**
     * @var CacheItemPoolInterface
     */
    private $cache;

    /**
     * @var AdminProductDataProvider
     */
    private $decoratedController;

    public function __construct(
        AdminProductDataProvider $decoratedController,
        EntityManager $entityManager,
        ImageManager $imageManager,
        CacheItemPoolInterface $cache
    ) {
        $this->entityManager = $entityManager;
        $this->decoratedController = $decoratedController;
        $this->imageManager = $imageManager;
        $this->cache = $cache;
    }

    protected function compileSqlQuery(
        array $select,
        array $table,
        array $where = array(),
        array $groupBy = array(),
        array $order = array(),
        $limit = null,
        $having = null
    ) {
        $sql = array();

        // SELECT
        $s = array();
        foreach ($select as $alias => $field) {
            $a = is_string($alias) ? ' AS `' . $alias . '`' : '';
            if (is_array($field)) {
                if (isset($field['table'])) {
                    $s[] = ' ' . $field['table'] . '.`' . $field['field'] . '` ' . $a;
                } elseif (isset($field['select'])) {
                    $s[] = ' ' . $field['select'] . $a;
                }
            } else {
                $s[] = ' ' . $field . $a;
            }
        }
        if (count($s) === 0) {
            throw new LogicException('Compile SQL failed: No field to SELECT!');
        }
        $sql[] = 'SELECT SQL_CALC_FOUND_ROWS' . implode(',' . PHP_EOL, $s);

        // FROM / JOIN
        $s = array();
        foreach ($table as $alias => $join) {
            if (!is_array($join)) {
                if (count($s) > 0) {
                    throw new LogicException(
                        'Compile SQL failed: cannot join the table ' . $join . ' into SQL query without JOIN sepcs.'
                    );
                }
                $s[0] = ' `' . _DB_PREFIX_ . $join . '` ' . $alias;
            } elseif (array_key_exists('nested', $join) && $join['nested']) {
                if (count($s) === 0) {
                    throw new LogicException(
                        'Compile SQL failed: cannot join the table alias ' .
                        $alias . ' into SQL query before to insert initial table.'
                    );
                }
                $s[] = ' ' . $join['join'] . ' (' . $join['table'] . ') ' .
                    $alias . ((isset($join['on'])) ? ' ON (' . $join['on'] . ')' : '');
            } else {
                if (count($s) === 0) {
                    throw new LogicException(
                        'Compile SQL failed: cannot join the table alias ' .
                        $alias . ' into SQL query before to insert initial table.'
                    );
                }
                $s[] = ' ' . $join['join'] . ' `' . _DB_PREFIX_ . $join['table'] . '` ' .
                    $alias . ((isset($join['on'])) ? ' ON (' . $join['on'] . ')' : '');
            }
        }
        if (count($s) === 0) {
            throw new LogicException('Compile SQL failed: No table to insert into FROM!');
        }
        $sql[] = 'FROM ' . implode(' ' . PHP_EOL, $s);

        // WHERE (recursive call)
        if (count($where)) {
            $s = $this->compileSqlWhere($where);
            if (Tools::strlen($s) > 0) {
                $sql[] = 'WHERE ' . $s . PHP_EOL;
            }
        }

        // GROUP BY
        if (!empty($groupBy)) {
            $sql[] = 'GROUP BY ' . implode(', ', array_map('pSQL', $groupBy)) . PHP_EOL;
        }
        // HAVING
        if (count($having) > 1) {
            $s = $this->compileSqlWhere($having);
            if (Tools::strlen($s) > 0) {
                $sql[] = 'HAVING ' . $s . PHP_EOL;
            }
        }

        // ORDER
        if (count($order) > 0) {
            $goodOrder = array();
            foreach ($order as $o) {
                $value = explode(' ', $o);
                if (!empty($value) && 2 === count($value) &&
                    Validate::isOrderBy($value[0]) &&
                    Validate::isOrderWay($value[1]) &&
                    in_array($value[0], array_keys($select))) {
                    $goodOrder[] = ' `' . bqSQL($value[0]) . '` ' . $value[1];
                }
            }

            if (count($goodOrder) > 0) {
                $sql[] = 'ORDER BY ' . implode(', ', $goodOrder) . PHP_EOL;
            } else {
                $sql[] = 'ORDER BY `id_product` DESC' . PHP_EOL;
            }
        }

        // LIMIT
        if ($limit) {
            $sql[] = 'LIMIT ' . $limit . PHP_EOL;
        }

        $this->lastCompiledSql = implode(' ' . PHP_EOL, $sql) . ';';

        return $this->lastCompiledSql;
    }

    /**
     * @param array $whereArray
     *
     * @return mixed|string
     */
    private function compileSqlWhere(array $whereArray)
    {
        $operator = 'AND';
        $s = array();
        while ($item = array_shift($whereArray)) {
            if ($item == 'OR') {
                $operator = 'OR';
            } elseif ($item == 'AND') {
                $operator = 'AND';
            } else {
                $s[] = (is_array($item) ? $this->compileSqlWhere($item) : $item);
            }
        }
        if (count($s) == 1) {
            return $s[0];
        }

        return '(' . implode(' ' . $operator . ' ', $s) . ')';
    }

    /**
     * Returns the last SQL query that was compiled on this Provider.
     *
     * @return string The last SQL query that was compiled with $this->compileSqlQuery()
     */
    public function getLastCompiledSql()
    {
        return $this->lastCompiledSql;
    }

    public function getPersistedFilterParameters()
    {
        return $this->decoratedController->getPersistedFilterParameters();
    }

    /**
     * {@inheritdoc}
     */
    public function isCategoryFiltered()
    {
        return $this->decoratedController->isCategoryFiltered();
    }

    /**
     * {@inheritdoc}
     */
    public function isColumnFiltered()
    {
        return $this->decoratedController->isColumnFiltered();
    }

    /**
     * {@inheritdoc}
     */
    public function persistFilterParameters(array $parameters)
    {
        $employee = Context::getContext()->employee;
        $shop = Context::getContext()->shop;
        if (version_compare(_PS_VERSION_, '8.0.0', '<')) {
            $filter = $this->entityManager->getRepository('PrestaShopBundle:AdminFilter')->findOneBy([
                'employee' => $employee->id ?: 0,
                'shop' => $shop->id ?: 0,
                'controller' => 'ProductController',
                'action' => 'catalogAction',
            ]);
        } else {
            $filter = $this->entityManager->getRepository(AdminFilter::class)->findOneBy([
                'employee' => $employee->id ?: 0,
                'shop' => $shop->id ?: 0,
                'controller' => 'ProductController',
                'action' => 'catalogAction',
            ]);
        }

        if (!$filter) {
            $filter = new AdminFilter();
            $filter
                ->setEmployee($employee->id ?: 0)
                ->setShop($shop->id ?: 0)
                ->setController('ProductController')
                ->setAction('catalogAction');
        }
        $filter->setFilter(json_encode($parameters) ? json_encode($parameters) : "{}");
        $this->entityManager->persist($filter);

        $filters = is_array($filter->getFilter()) ?
            $filter->getFilter() :
            json_decode($filter->getFilter() ?: "{}", true);
        $filters = array_filter($filters, function($f) {
            return !is_array($f);
        });
        // if each filter is == '', then remove item from DB :)
        if (count(array_diff($filters, [''])) == 0) {
            $this->entityManager->remove($filter);
        }

        $this->entityManager->flush();

        //Flush cache
        $employee = Context::getContext()->employee;
        $employeeId = $employee->id ?: 0;

        $this->cache->deleteItem("app.product_filters_${employeeId}");
    }

    /**
     * {@inheritdoc}
     */
    public function combinePersistentCatalogProductFilter($paramsIn = [], $avoidPersistence = false)
    {
        // retrieve persisted filter parameters
        $persistedParams = $this->getPersistedFilterParameters();
        // merge with new values
        $paramsOut = array_merge($persistedParams, (array) $paramsIn);
        // persist new values
        if (!$avoidPersistence) {
            $this->persistFilterParameters($paramsOut);
        }

        // return new values
        return $paramsOut;
    }

    /**
     * {@inheritdoc}
     */
    public function getCatalogProductList(
        $offset,
        $limit,
        $orderBy,
        $sortOrder,
        $post = [],
        $avoidPersistence = false,
        $formatCldr = true
    ) {
        if (!Module::isEnabled('customproductslist') ||
            !Module::getInstanceByName('customproductslist')->isActive('override')) {
            $default_fields = [
                'id_product',
                'reference',
                'price',
                'id_shop_default',
                'is_virtual',
                'name',
                'link_rewrite',
                'active',
                'shopname',
                'id_image',
                'name_category',
                'price_final',
                'nb_downloadable',
                'sav_quantity',
                'badge_danger'
            ];
            $orderBy = in_array($orderBy, $default_fields) ? $orderBy : 'id_product';
            return $this->decoratedController->getCatalogProductList(
                $offset,
                $limit,
                $orderBy,
                $sortOrder,
                $post,
                $avoidPersistence,
                $formatCldr
            );
        }

        $offset = (int) $offset;
        $limit = (int) $limit;
        $orderBy = Validate::isOrderBy($orderBy) ? $orderBy : 'id_product';
        $sortOrder = Validate::isOrderWay($sortOrder) ? $sortOrder : 'desc';

        $filterParams = $this->combinePersistentCatalogProductFilter(array_merge(
            $post,
            [
                'last_offset' => $offset,
                'last_limit' => $limit,
                'last_orderBy' => $orderBy,
                'last_sortOrder' => $sortOrder
            ]
        ), $avoidPersistence);

        $filterParams = CPLAdminFilter::sanitizeFilterParameters($filterParams);

        $showPositionColumn = $this->isCategoryFiltered();
        if ($orderBy == 'position_ordering' && $showPositionColumn) {
            foreach (array_keys($filterParams) as $key) {
                if (strpos($key, 'filter_column_') === 0) {
                    $filterParams[$key] = '';
                }
            }
        }
        if ($orderBy == 'position_ordering') {
            $orderBy = 'position';
        }

        $idShop = Context::getContext()->shop->id;
        $idLang = Context::getContext()->language->id;

        $sqlSelect = array(
            'id_product' => array('table' => 'p', 'field' => 'id_product', 'filtering' => ' %s '),
            'reference' => array('table' => 'p', 'field' => 'reference', 'filtering' => self::FILTERING_LIKE_BOTH),
            'price' => array(
                'table' => version_compare(_PS_VERSION_, '1.7.6.0', '<') ? 'p' : 'sa',
                'field' => 'price',
                'filtering' => ' %s '
            ),
            'id_shop_default' => array('table' => 'p', 'field' => 'id_shop_default'),
            'is_virtual' => array('table' => 'p', 'field' => 'is_virtual'),
            'name' => array('table' => 'pl', 'field' => 'name', 'filtering' => self::FILTERING_LIKE_BOTH),
            'link_rewrite' =>
                array('table' => 'pl', 'field' => 'link_rewrite', 'filtering' => self::FILTERING_LIKE_BOTH),
            'active' => array('table' => 'sa', 'field' => 'active', 'filtering' => self::FILTERING_EQUAL_NUMERIC),
            'shopname' => array('table' => 'shop', 'field' => 'name'),
            'id_image' => array('table' => 'image_shop', 'field' => 'id_image'),
            'name_category' => array('table' => 'cl', 'field' => 'name', 'filtering' => self::FILTERING_LIKE_BOTH),
            'price_final' => '0',
            'nb_downloadable' => array('table' => 'pd', 'field' => 'nb_downloadable'),
            'sav_quantity' => array('table' => 'sav', 'field' => 'quantity', 'filtering' => ' %s '),
            'badge_danger' =>
                array('select' => 'IF(sav.`quantity`<=0, 1, 0)', 'filtering' => 'IF(sav.`quantity`<=0, 1, 0) = %s'),
        );
        $sqlTable = array(
            'p' => 'product',
            'pl' => array(
                'table' => 'product_lang',
                'join' => 'LEFT JOIN',
                'on' => 'pl.`id_product` = p.`id_product` 
                    AND pl.`id_lang` = ' . $idLang . ' 
                    AND pl.`id_shop` = ' . $idShop,
            ),
            'sav' => array(
                'table' => 'stock_available',
                'join' => 'LEFT JOIN',
                'on' => 'sav.`id_product` = p.`id_product` AND sav.`id_product_attribute` = 0' .
                    StockAvailable::addSqlShopRestriction(null, $idShop, 'sav'),
            ),
            'sa' => array(
                'table' => 'product_shop',
                'join' => 'JOIN',
                'on' => 'p.`id_product` = sa.`id_product` AND sa.id_shop = ' . $idShop,
            ),
            'cl' => array(
                'table' => 'category_lang',
                'join' => 'LEFT JOIN',
                'on' => 'sa.`id_category_default` = cl.`id_category` 
                    AND cl.`id_lang` = ' . $idLang . ' 
                    AND cl.id_shop = ' . $idShop,
            ),
            'c' => array(
                'table' => 'category',
                'join' => 'LEFT JOIN',
                'on' => 'c.`id_category` = cl.`id_category`',
            ),
            'shop' => array(
                'table' => 'shop',
                'join' => 'LEFT JOIN',
                'on' => 'shop.id_shop = ' . $idShop,
            ),
            'image_shop' => array(
                'table' => 'image_shop',
                'join' => 'LEFT JOIN',
                'on' => 'image_shop.`id_product` = p.`id_product` 
                    AND image_shop.`cover` = 1 
                    AND image_shop.id_shop = ' . $idShop,
            ),
            'i' => array(
                'table' => 'image',
                'join' => 'LEFT JOIN',
                'on' => 'i.`id_image` = image_shop.`id_image`',
            ),
            'pd' => array(
                'table' => 'product_download',
                'join' => 'LEFT JOIN',
                'on' => 'pd.`id_product` = p.`id_product`',
            ),
        );
        $sqlWhere = array('AND', 1);
        $sqlHaving = array('AND', 1);
        $sqlOrder = array($orderBy . ' ' . $sortOrder);
        if ($orderBy != 'id_product') {
            // secondary order by (useful when ordering by active, quantity, price, etc...)
            $sqlOrder[] = 'id_product asc';
        }
        $sqlLimit = $offset . ', ' . $limit;

        // Column 'position' added if filtering by category
        if ($showPositionColumn) {
            $filteredCategoryId = (int) $filterParams['filter_category'];
            $sqlSelect['position'] = array('table' => 'cp', 'field' => 'position');
            $sqlTable['cp'] = array(
                'table' => 'category_product',
                'join' => 'INNER JOIN',
                'on' => 'cp.`id_product` = p.`id_product` AND cp.`id_category` = ' . $filteredCategoryId,
            );
        } elseif ($orderBy == 'position') {
            // We do not show position column, so we do not join the table, so we do not order by position!
            $sqlOrder = array('id_product ASC');
        }

        $sqlGroupBy = array();

        // exec legacy hook but with different parameters (retro-compat < 1.7 is broken here)
        Hook::exec('actionAdminProductsListingFieldsModifier', array(
            '_ps_version' => AppKernel::VERSION,
            'sql_select' => &$sqlSelect,
            'sql_table' => &$sqlTable,
            'sql_where' => &$sqlWhere,
            'sql_group_by' => &$sqlGroupBy,
            'sql_order' => &$sqlOrder,
            'sql_limit' => &$sqlLimit,
        ));

        $cpl = CPL::getInstance('override');
        $cpl_fields = $cpl->getFields();
        foreach ($filterParams as $filterParam => $filterValue) {
            if (!$filterValue && $filterValue !== '0') {
                continue;
            }
            if (strpos($filterParam, 'filter_column_') === 0) {
                $field = Tools::substr($filterParam, 14); // 'filter_column_' takes 14 chars
                if (!array_key_exists($field, $cpl_fields)) {
                    continue;
                }
                $html_ok = in_array($filterParam, [
                        'filter_column_id_product',
                        'filter_column_sav_quantity',
                        'filter_column_price',
                    ]) ||
                    (array_key_exists('type', $cpl_fields[$field]) && $this->isHtmlOk($cpl_fields[$field]['type']));
                $filterValue = Db::getInstance()->escape($filterValue, $html_ok, true);
                if (array_key_exists('havingFilter', $cpl_fields[$field]) && $cpl_fields[$field]['havingFilter']) {
                    $sqlHaving[] = '(' . sprintf($sqlSelect[$field]['filtering'], $filterValue) . ')';
                } elseif (isset($sqlSelect[$field]['table'])) {
                    $sqlWhere[] = $sqlSelect[$field]['table'] . '.`' . $sqlSelect[$field]['field'] . '` ' .
                        sprintf($sqlSelect[$field]['filtering'], $filterValue);
                } else {
                    $sqlWhere[] = '(' . sprintf($sqlSelect[$field]['filtering'], $filterValue) . ')';
                }
            }
            // for 'filter_category', see next if($showPositionColumn) block.
        }
        $sqlWhere[] = 'state = ' . Product::STATE_SAVED;

        // exec legacy hook but with different parameters (retro-compat < 1.7 is broken here)
        Hook::exec('actionAdminProductsListingFieldsModifier', array(
            '_ps_version' => AppKernel::VERSION,
            'sql_select' => &$sqlSelect,
            'sql_table' => &$sqlTable,
            'sql_where' => &$sqlWhere,
            'sql_group_by' => &$sqlGroupBy,
            'sql_order' => &$sqlOrder,
            'sql_limit' => &$sqlLimit,
        ));

        $sql = $this->compileSqlQuery($sqlSelect, $sqlTable, $sqlWhere, $sqlGroupBy, $sqlOrder, $sqlLimit, $sqlHaving);
        $products = Db::getInstance()->executeS($sql, true, false);
        $total = Db::getInstance()->executeS('SELECT FOUND_ROWS();', true, false);
        $total = $total[0]['FOUND_ROWS()'];

        // post treatment
        $currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));
        $nothing = null;
        if (version_compare(_PS_VERSION_, '1.7.7.0', '>=')) {
            $localeCldr = Tools::getContextLocale(Context::getContext());
        }
        foreach ($products as &$product) {
            $product['total'] = $total; // total product count (filtered)
            $product['price_final'] = Product::getPriceStatic(
                $product['id_product'],
                true,
                null,
                version_compare(_PS_VERSION_, '1.7.7.0', '<') ?
                    (int)Configuration::get('PS_PRICE_DISPLAY_PRECISION') :
                    Context::getContext()->getComputingPrecision(),
                null,
                false,
                false,
                1,
                true,
                null,
                null,
                null,
                $nothing,
                true,
                true
            );
            if ($formatCldr) {
                if (version_compare(_PS_VERSION_, '1.7.7.0', '<')) {
                    $product['price'] = Tools::displayPrice($product['price'], $currency);
                    $product['price_final'] = Tools::displayPrice($product['price_final'], $currency);
                } else {
                    if (array_key_exists('price_final', $product)) {
                        $product['price_final'] = $localeCldr->formatPrice(
                            (float)$product['price_final'],
                            $currency->iso_code
                        );
                    }
                    if (array_key_exists('final_price', $product)) {
                        $product['final_price'] = $localeCldr->formatPrice(
                            (float)$product['final_price'],
                            $currency->iso_code
                        );
                    }
                    $product['price'] = $localeCldr->formatPrice($product['price'], $currency->iso_code);
                }
            }
            $product['image'] = $this->imageManager->getThumbnailForListing($product['id_image']);
            $product['image_link'] =
                Context::getContext()->link->getImageLink($product['link_rewrite'], $product['id_image']);
        }

        // post treatment by hooks
        // exec legacy hook but with different parameters (retro-compat < 1.7 is broken here)
        Hook::exec('actionAdminProductsListingResultsModifier', array(
            '_ps_version' => AppKernel::VERSION,
            'products' => &$products,
            'total' => $total,
        ));

        return $products;
    }

    private function isHtmlOk($type)
    {
        return $type == 'int' || $type == 'decimal' || $type == 'date' || $type == 'datetime';
    }

    /**
     * {@inheritdoc}
     */
    public function countAllProducts()
    {
        return $this->decoratedController->countAllProducts();
    }

    /**
     * Translates new Core route parameters into their Legacy equivalent.
     *
     * @param string[] $coreParameters The new Core route parameters
     *
     * @return string[] The URL parameters for Legacy URL (GETs)
     */
    public function mapLegacyParametersProductForm($coreParameters = array())
    {
        return $this->decoratedController->mapLegacyParametersProductForm($coreParameters);
    }

    /**
     * {@inheritdoc}
     */
    public function getPaginationLimitChoices()
    {
        return $this->decoratedController->getPaginationLimitChoices();
    }

    /**
     * {@inheritdoc}
     */
    public function isNewProductDefaultActivated()
    {
        return $this->decoratedController->isNewProductDefaultActivated();
    }
}
