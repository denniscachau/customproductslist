{*
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="panel">
  <form id="customproductslist_form" class="defaultForm form-horizontal AdminCustomProductsListConfig"  action="{$link_to_module|escape:'htmlall':'UTF-8'}" method="post" enctype="multipart/form-data" novalidate>

    <div class="panel-heading"><i class="icon-cogs"></i> {l s='Attributes position' mod='customproductslist'}</div>
    <div id="attributes_position" class="panel-body">
      {if isset($positions['attributes'])}
        {for $key=1 to sizeof($positions['attributes'])}
          <div class="panel col-lg-4 ">
            <table class='table'>
              <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <tr>
                <th class="attribute_id">{$positions['attributes'][$key-1]|escape:'htmlall':'UTF-8'}</th>
              </tr>
              <tr>
                <td>
                  <input value="{$positions['titles'][$key-1]|escape:'htmlall':'UTF-8'}" class="title" type="text" name="CPL_TITLE_{$key|escape:'htmlall':'UTF-8'}">
                </td>
              </tr>
              <tr>
                <td class="attribute_type">
                  <select name="CPL_TYPE_{$key|escape:'htmlall':'UTF-8'}" class="type">
                    <option value="" >{l s='Type' mod='customproductslist'}</option>
                    {foreach from=$types item=type key=value}
                      {if $positions['types'][$key-1] == $value}
                        <option value="{$value|escape:'htmlall':'UTF-8'}" selected="selected">{$type|escape:'htmlall':'UTF-8'}</option>
                      {else}
                        <option value="{$value|escape:'htmlall':'UTF-8'}">{$type|escape:'htmlall':'UTF-8'}</option>
                      {/if}
                    {/foreach}
                  </select>
                </td>
              </tr>
              <tr>
                <td class="attribute_name">{$positions['tables'][$key-1]|escape:'htmlall':'UTF-8'}</td>
              </tr>
              <tr>
                <td class="attribute_position">Position : <span class="position">{$key|escape:'htmlall':'UTF-8'}</span> <button type="button" class="btn btn-default plus">+</button> <button type="button" class="btn btn-default moins">-</button></td>
              </tr>
            </table>
            <input class="aHidden" type="hidden" name="CPL_ATTRIBUTE_{$key|escape:'htmlall':'UTF-8'}" value="{$positions['attributes'][$key-1]|escape:'htmlall':'UTF-8'}">
            <input class="tHidden" type="hidden" name="CPL_TABLE_{$key|escape:'htmlall':'UTF-8'}" value="{$positions['tables'][$key-1]|escape:'htmlall':'UTF-8'}">
          </div>
        {/for}
      {/if}
    </div>
    <div class="panel-footer">
      <button type="submit" value="1" id="customproductslist_form_submit_btn" name="submitAddcustomproductslist" class="btn btn-default pull-right">
        <i class="process-icon-save"></i>
        {l s='Validate' mod='customproductslist'}
      </button>
      <a href="{$link_to_module|escape:'htmlall':'UTF-8'}" class="btn btn-default pull-left"><i class="process-icon-cancel"></i>{l s='Cancel' mod='customproductslist'}</a>
    </div>
  </form>
</div>

</div>
</div>
