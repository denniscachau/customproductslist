{*
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="row">
<div class="col-lg-12">

<form class="defaultForm form-horizontal AdminCustomProductsListConfig" action="{$refresh|escape:'htmlall':'UTF-8'}" method="post" enctype="multipart/form-data" novalidate="">
  <div class="panel" id="fieldset_0">
    <div class="panel-heading"><i class="icon-cogs"></i> {l s='Choice of the table' mod='customproductslist'}</div>
    <div class="form-wrapper">
      <div class="form-group">
        <label class="control-label col-lg-3">
          {l s='Tables available :' mod='customproductslist'}
        </label>
        <div class="col-lg-9">
          <select name="selecModule" class=" fixed-width-xl" id="selecModule">
            {foreach from=$modules item=module key=key}
              {if $module['table_name'] == $selected_table}
                <option value="{$module['table_name']|escape:'htmlall':'UTF-8'}" selected="selected">{str_replace($prefix, "", $module['table_name'])|escape:'htmlall':'UTF-8'}</option>
              {else}
                <option value="{$module['table_name']|escape:'htmlall':'UTF-8'}">{str_replace($prefix, "", $module['table_name'])|escape:'htmlall':'UTF-8'}</option>
              {/if}
            {/foreach}
          </select>
        </div>
      </div>
    </div>

    <input type="hidden" name="CPL_GLOBAL_ATT" id="globalAtt">
    <input type="hidden" name="CPL_GLOBAL_TAB" id="globalTab">
    <input type="hidden" name="CPL_GLOBAL_TIT" id="globalTit">
    <input type="hidden" name="CPL_GLOBAL_TYP" id="globalTyp">

    <div class="panel-footer">
      <button type="submit" value="1" class="btn btn-default pull-right">
        <i class="process-icon-save"></i> {l s='Change the table (unguided)' mod='customproductslist'}
      </button>
      <button type="submit" value="1" name="default" class="btn btn-default pull-right">
        <i class="process-icon-back"></i> {l s='Change to table by default' mod='customproductslist'}
      </button>
    </div>
  </div>
</form>
