{*
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<script type="text/javascript">
    $('document').ready(function() {
        let form = '{$id_form|escape:'htmlall':'UTF-8'}';
        let table = form + ' table:first';
        $(table + ' td.pointer.cpl-editable').each(function() {
          $(this).removeAttr('onclick');
            $(this).removeClass('pointer');
        });

        // let value_array;
        // let text;

        // $(table + ' td.cpl-editable').each(function() {
        //     text = $(this).text();
        //     if ($(this).text().match(/.*;;;.*;;;.*;;;.*/)) {
        //         value_array = text.split(';;;');
        //         $(this).html("<input type='text' data-id='" + Number($.trim(value_array[2])) + "' data-table='" + $.trim(value_array[0]) + "' data-attribute='" + $.trim(value_array[1]) + "' class='cpl-input-editable'>");
        //
        //         $(this).children('.cpl-input-editable').val($.trim(value_array[3]));
        //     }
        // });

        $(form).on('change', '.cpl-input-editable', function(){
            $.ajax({
                type: 'POST',
                url: admin_customproductslist_ajax_url,
                dataType: 'json',
                data: {
                    controller : 'AdminCustomProductsListConfig',
                    action : 'updateValue',
                    ajax : true,
                    attribute: $(this).data('attribute'),
                    table: $(this).data('table'),
                    id_product: $(this).data('id'),
                    CPL_value : $(this).val(),
                },
                success: (data) => {
                    if (data.success) {
                        if (typeof data.class !== undefined && typeof data.class_value !== undefined) {
                            let to_modify = $(this).parents('.cpl-editable').siblings('.' + data.class);
                            if (to_modify.length > 0) {
                                if ($(to_modify).hasClass('cpl-editable')) {
                                    $(to_modify).find('.cpl-input-editable').val(data.class_value);
                                } else {
                                    $(to_modify).text(data.class_value_currency);
                                }
                            }
                        }
                        $.growl.notice({ title: "", message: update_success_msg });
                    } else {
                        $.growl.error({ title: "", message: data.text });
                    }
                },
                complete: (data) => {
                    if (data.readyState != 4) {
                        $.growl.error({ title: "", message: "An error has occured." });
                    }
                }
            });
        });
    });
</script>
