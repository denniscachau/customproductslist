{*
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}


<div class="row">
    <div class="col-lg-12">
        <form class="defaultForm form-horizontal"
              method="post"
              enctype="multipart/form-data"
              novalidate="">
            <div class="container panel"
                 id="fieldset_0">
                <div class="panel-heading">
                    <i class="icon-cogs"></i> {l s='Quick add' mod='customproductslist'}
                </div>
                <div class="form-wrapper">
                    <div class="form-group">
                        <label class="control-label col-lg-3">
                            {l s='Quick add' mod='customproductslist'}
                            :
                        </label>
                        <div class="col-lg-9">
                            <select name="CPL_QUICK_ADD"
                                    class=" fixed-width-xl"
                                    id="cpl_quick_add">
                                {foreach from=$quick_add item=category_array key=category_name}
                                <optgroup
                                        label="{$category_name|escape:'htmlall':'UTF-8'}">
                                    {foreach from=$category_array item=column_key}
                                        {assign "columns_bundle" []}
                                        {if preg_match('/^(\w+)-\*$/', $column_key, $columns_bundle)}
                                            {foreach from=${$columns_bundle[1]} item=column_key}
                                                <option value="{$columns_bundle[1]|escape:'quotes':'UTF-8'}-{$column_key['name']|escape:'quotes':'UTF-8'}">{$column_key['name']|escape:'htmlall':'UTF-8'}</option>
                                            {/foreach}
                                        {else}
                                            <option value="{$column_key|escape:'quotes':'UTF-8'}">{$description[$column_key]['description']|escape:'htmlall':'UTF-8'}</option>
                                        {/if}
                                    {/foreach}
                                {/foreach}
                            </select>
                            <p class="help-block">{l s='If you don\'t find the attribute you want to add, please click on the button "Add a new column" at the top right of the page' mod='customproductslist'}</p>
                        </div>
                    </div>
                </div>

                <div class="panel-footer">
                    <button type="submit"
                            value="1"
                            name="CPL_GLOBAL"
                            class="btn btn-default pull-right">
                        <i class="process-icon-save"></i> {l s='Validate' mod='customproductslist'}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
