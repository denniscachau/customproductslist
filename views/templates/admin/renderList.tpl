{*
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<script type="text/javascript">
$('document').ready(function() {
    let table = '{$id_table|escape:'htmlall':'UTF-8'}';
    let form = '{$id_form|escape:'htmlall':'UTF-8'}';
  $(table + ' td.pointer').each(function() {
    $(this).removeAttr('onclick');
    $(this).removeClass('pointer');
  });
  $(table + ' td.cpl-titles').each(function() {
    $(this)[0].innerHTML = "<input type='text' class='cpl-input-text' value=\"" + $(this)[0].innerText + "\">"
  });
  $(table + ' td.cpl-types').each(function() {
    let select = "<select class='cpl-select-type'>"
        select += "<option value=''></option>"
        {foreach from=$types item=type key=value}
          if ($(this)[0].innerText == '{$value|escape:'htmlall':'UTF-8'}') {
            select += "<option value='{$value|escape:'htmlall':'UTF-8'}' selected='selected'>{$type|escape:'htmlall':'UTF-8'}</option>";
          } else {
            select += "<option value='{$value|escape:'htmlall':'UTF-8'}'>{$type|escape:'htmlall':'UTF-8'}</option>";
          }
        {/foreach}
      select += "</select>"
      $(this)[0].innerHTML = select;
  });
  $(table + ' td.cpl-align').each(function() {
    let select = "<select class='cpl-select-align'>"
    {foreach from=$aligns item=align key=value}
    if ($(this)[0].innerText == '{$value|escape:'htmlall':'UTF-8'}') {
      select += "<option value='{$value|escape:'htmlall':'UTF-8'}' selected='selected'>{$align|escape:'htmlall':'UTF-8'}</option>";
    } else {
      select += "<option value='{$value|escape:'htmlall':'UTF-8'}'>{$align|escape:'htmlall':'UTF-8'}</option>";
    }
    {/foreach}
    select += "</select>"
    $(this)[0].innerHTML = select;
  });

  $(form).on('change', '.cpl-input-text', function(){
    $.ajax({
      type: 'POST',
      url: admin_customproductslist_ajax_url,
      dataType: 'json',
      data: {
        controller : 'AdminCustomProductsListConfig',
        action : 'sendInputList',
        ajax : true,
        attribute: 'title',
        CPL_input : $(this).val(),
        id_customproductslist : $(this).parent().parent().attr('id').split('_')[2],
      },
      success: function() {
        $.growl.notice({ title: "", message: update_success_msg });
      },
      error: function() {
        $.growl.error({ title: "", message: "An error has occurred" });
      }
    });
  });
  $(form).on('change', '.cpl-select-type', function(){
    $.ajax({
      type: 'POST',
      url: admin_customproductslist_ajax_url,
      dataType: 'json',
      data: {
        controller : 'AdminCustomProductsListConfig',
        action : 'sendInputList',
        ajax : true,
        attribute: 'type',
        CPL_input : $(this).val(),
        id_customproductslist : $(this).parent().parent().attr('id').split('_')[2],
      },
      success: function() {
        $.growl.notice({ title: "", message: update_success_msg });
      },
      error: function() {
        $.growl.error({ title: "", message: "An error has occurred" });
      }
    });
  });
  $(form).on('change', '.cpl-select-align', function(){
    $.ajax({
      type: 'POST',
      url: admin_customproductslist_ajax_url,
      dataType: 'json',
      data: {
        controller : 'AdminCustomProductsListConfig',
        action : 'sendInputList',
        ajax : true,
        attribute: 'align',
        CPL_input : $(this).val(),
        id_customproductslist : $(this).parent().parent().attr('id').split('_')[2],
      },
      success: function() {
        $.growl.notice({ title: "", message: update_success_msg });
      },
      error: function() {
        $.growl.error({ title: "", message: "An error has occurred" });
      }
    });
  });
});
</script>
