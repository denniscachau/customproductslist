/**
 * 2007-2023 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2023 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

$('document').ready(() => {
    let textField = '<input type="number" class="form-control" id="cpl_position_field" style="display: none;">';
    $('#content').prepend(textField);
    $('.dragGroup').on('click', function(e) {
        let position = Number($(this).find('.positions').text());
        let fieldElement = $('#cpl_position_field');
        fieldElement.show();
        let field_width = fieldElement.width($(this).parent().width()).width();
        let field_height = fieldElement.height();
        fieldElement.css({
            position: 'absolute',
            left: e.pageX - (field_width/2),
            top: e.pageY - (field_height),
            "z-index": 99
        }).val(position).data('cpl-pos', $(this).parents('tr').attr('id'));

        fieldElement.focus();
    });

    let cpl_position_changed = false;
    $('#cpl_position_field').on('keypress', function (e) {
        if (e.keyCode == 13) {
            $(this).change().focusout();
        }
    }).on('change', function () {
        if (Number($(this).val()) > 0) {
            cpl_position_changed = true;
        } else {
            cpl_position_changed = false;
        }
    }).on('focusout', function() {
        if (cpl_position_changed) {
            let newURL = window.location.protocol + "//" + window.location.host + window.location.pathname;
            let queryString = window.location.search
                .replace(/&id_category=[0-9]+/g, '')
                .replace(/&cpl_position=\w+/g, '')
                .replace(/&action=\w+/g, '')
                .replace(/&newPosition=[0-9]+/g, '')
                .replace(/&conf=[0-9]+/g, '') + "&action=position_field&cpl_position=" + $(this).data('cpl-pos')+"&newPosition="+$(this).val();
            location.href = newURL+queryString;
        } else {
            $(this).hide();
        }
    });

    // $('#form-product').on('submit', function(e) {
    //     let cpl_form_action = $(this).attr('action');
    //     let submitActionReg = new RegExp(/^.+(&submitBulkchangePositions.+)&.+$/);
    //
    //     if (submitActionReg.test(cpl_form_action) && !$(this).data('position-ok')) {
    //         $('#position-modal').modal();
    //         e.preventDefault();
    //     }
    // });
    //
    // $('#submitMultiplePosition').on('click', () => {
    //     let position_value = $('#position-input').val();
    //
    //     if (position_value > 0) {
    //         let position_input = '<input type="hidden" name="cpl_position" value="' + position_value + '">'
    //         $('#form-product').prepend(position_input).data('position-ok', true).submit();
    //     } else {
    //         $('#position-error').show();
    //     }
    // });
    //
    // $('#position-modal').on('hide.bs.modal', () => {
    //     let cpl_form_action = $('#form-product').attr('action').replace('&submitBulkchangePositionsproduct', '');
    //     $('#form-product').attr('action', cpl_form_action);
    //     $('#position-error').hide();
    // });
});
