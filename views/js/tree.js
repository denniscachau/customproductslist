/**
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/

$('document').ready(() => {
    $(document).on('change', '#categories-tree input', () => {
        let cpl_categories = [];

        $('#categories-tree input').each(function (i, v) {
            if ($(this).is(':checked') === true) {
                cpl_categories.push($(this).val());
            }
        })

        let newURL = window.location.protocol + "//" + window.location.host + window.location.pathname;
		let queryString = window.location.search.replace(/&id_category=[0-9]*/, "") + "&id_category=" + cpl_categories.join(',');
		location.href = newURL+queryString; // hash part is dropped: window.location.hash
    });
});
