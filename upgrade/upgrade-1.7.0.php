<?php
/**
 * 2007-2023 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2023 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_1_7_0($module)
{
    if (Tab::getIdFromClassName('AdminCustomProductsListGrid') === false &&
        version_compare(_PS_VERSION_, '1.7.6.0', '>=')) {
        $tab = new Tab();
        $tab->class_name = 'AdminCustomProductsListGrid';
        $tab->id_parent = Tab::getIdFromClassName('AdminCatalog');
        $tab->module = $module->name;
        $tab->name[(int) Configuration::get('PS_LANG_DEFAULT')] = 'Products custom grid';
        $tab->active = 0;
        $tab->add();
    }

    $current_list = Configuration::get('CPL_use_default') ? '16' : 'custom';

    $sql = array();
    $sql[] = "ALTER TABLE `" . _DB_PREFIX_ . "customproductslist` 
        ADD `list` VARCHAR(255) NOT NULL DEFAULT '" . pSQL($current_list) . "' AFTER `id_customproductslist`;";

    $sql[] = "ALTER TABLE `" . _DB_PREFIX_ . "customproductslist` DROP INDEX `attribute`, 
        ADD UNIQUE `attribute` (`list`, `attribute`, `table`) USING BTREE;";

    foreach ($sql as $q) {
        Db::getInstance()->execute($q);
    }

    $cpl_config = array();
    if ($current_list != 'custom') {
        $module->populateList('custom');
        $cpl_config[$current_list] = '1';
    } elseif (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
        $module->populateList('16');
    } elseif (version_compare(_PS_VERSION_, '1.7.6.0', '>=')) {
        $module->populateList('override');
        $cpl_config['override'] = '1';
    }
    $cpl_config['custom'] = (string)(int)!(version_compare(_PS_VERSION_, '1.7.0.0', '<') && $current_list == '16');

    Configuration::updateValue('cpl_config', json_encode($cpl_config));
    Configuration::deleteByName('CPL_use_default');

    if (version_compare(_PS_VERSION_, '1.7.6.0', '>=')) {
        Tools::clearSf2Cache('prod');
        Tools::clearSf2Cache('dev');
        $module->registerHook('actionAdminProductsListingFieldsModifier');
        $module->registerHook('actionAdminProductsListingResultsModifier');
    } elseif (version_compare(_PS_VERSION_, '1.7.5.0', '>=') && version_compare(_PS_VERSION_, '1.7.6.0', '<')) {
        $module->registerHook('actionModuleUpgradeBefore');
        $module->deleteConfigDir();
        $module->installOverrides();
    }

    return true;
}
