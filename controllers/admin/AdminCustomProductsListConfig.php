<?php
/**
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once _PS_MODULE_DIR_ . 'customproductslist/classes/CPLConfig.php';

class AdminCustomProductsListConfigController extends ModuleAdminController
{
    protected $position_identifier = 'id_customproductslist';

    /**
     * @var CPL
     */
    protected $cpl;
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'customproductslist';
        $this->identifier = 'id_customproductslist';
        $this->className = 'CPLConfig';
        $this->tabClassName = 'AdminCustomProductsListConfig';
        $this->lang = false;
        $this->deleted = false;
        $this->colorOnBackground = false;
        $this->bulk_actions = array(
          'delete' => array('text' => 'Delete selected', 'confirm' => 'Delete selected items?'),
        );
        $this->context = Context::getContext();

        $this->_defaultOrderBy = 'position';

        parent::__construct();

        $this->fields_list = array(
            'position' => array(
                'title' => $this->l('Position'),
                'align' => 'text-center',
                'position' => 'position',
                'filter_key' => 'position',
                'class' => 'fixed-width-xs',
                'orderby' => false,
                'search' => false
            ),
            'attribute' => array(
                'title' => $this->l('Attributes'),
                'align' => 'text-center',
                'filter_key' => 'attribute',
                'orderby' => false,
                'search' => false
            ),
            'table' => array(
                'title' => $this->l('Table'),
                'align' => 'text-center',
                'filter_key' => 'table',
                'orderby' => false,
                'search' => false
            ),
            'title' => array(
                'title' => $this->l('Title'),
                'align' => 'text-center',
                'class' => 'cpl-titles',
                'orderby' => false,
                'search' => false
            ),
            'type' => array(
                'title' => $this->l('Type'),
                'align' => 'text-center',
                'class' => 'cpl-types',
                'orderby' => false,
                'search' => false
            ),
            'align' => array(
                'title' => $this->l('Align'),
                'align' => 'text-center',
                'class' => 'cpl-align',
                'orderby' => false,
                'search' => false
            ),
        );

        $this->types = array(
            'boolean' => $this->l('Boolean (True / False)'),
            'price' => $this->l('Price'),
            'integer' => $this->l('Integer'),
            'decimal' => $this->l('Decimal'),
            'percent' => $this->l('Percentage'),
            'date' => $this->l('Date'),
            'datetime' => $this->l('Date / time'),
            'editable' => $this->l('Editable'),
            'select' => $this->l('Select filter (only available for some fields)')
        );

        if ($this->context->cookie->__get('cpl_list') == 'override') {
            unset($this->types['editable']);
            unset($this->types['select']);
        }

        $this->quick_add = array(
            $this->l('Default') => array(
                'product_lang-name',
                'image-id_image',
                'product-reference',
                'category_lang-name',
                'product-price',
                'product-final_price',
                'stock_available-quantity',
                'category_product-position',
                'product-active'
            ),
            $this->l('Product\'s information') => array(
                'customproductslist-categories',
                'manufacturer-name',
                'product-location',
                'product_attribute-location',
            ),
            $this->l('Features') => array(
                'feature_value_lang-value',
                'feature_name-*'
            ),
            $this->l('Combinations') => array(
                'attribute_lang-name',
                'attribute_group_name-*'
            ),
            $this->l('Shipping') => array(
                'product-width',
                'product-height',
                'product-depth',
                'product-weight',
                'product-additional_shipping_cost',
                'carrier-name'
            ),
            $this->l('Price') => array(
                'product-wholesale_price',
                'customproductslist-final_price_discount',
                'customproductslist-final_price_discount_tax_excl',
                'customproductslist-discount_reduction',
                'customproductslist-unit_price',
                'product-unity',
                'tax_rules_group-name'
            ),
            $this->l('Options') => array(
                'product-ean13',
                'product-upc',
//                'product-isbn',
                'supplier-name',
                'tag-name'
            ),
        );

        // ISBN only available for the PS version greater than 1.7.0.0
        if (version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            array_unshift($this->quick_add[$this->l('Options')], 'product-isbn');
        }

        $this->cpl = CPL::getInstance('config');
        // $this->cpl->setUseParent(true)->setWithAliases(false);
    }

    public function initPageHeaderToolbar()
    {
        parent::initPageHeaderToolbar();

        if (empty($this->display)) {
            $this->page_header_toolbar_btn['desc-module-back'] = array(
                'href' => 'index.php?controller=AdminModules&configure=customproductslist&token=' .
                    Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back'),
                'icon' => 'process-icon-back',
            );
            $this->page_header_toolbar_btn['desc-module-new'] = array(
                'href' => 'index.php?controller=' . $this->tabClassName . '&add' . $this->table . '&token=' .
                Tools::getAdminTokenLite($this->tabClassName),
                'desc' => $this->l('Add a new column'),
                'icon' => 'process-icon-new',
            );
            $this->page_header_toolbar_btn['desc-module-doc'] = array(
                'href' => _MODULE_DIR_ . 'customproductslist/readme_' . $this->getDocSuffix() . '.pdf',
                'desc' => $this->l('Read the documentation'),
                'target' => true,
                'icon' => 'process-icon- icon-file-text',
            );
        }
    }

    private function getDocSuffix()
    {
        $available_docs = array(
            'fr'
        );

        return in_array($this->context->language->iso_code, $available_docs) ?
            $this->context->language->iso_code :
            'en';
    }

    private function assignation()
    {
        $columns_name = array(
            'id_product',
            'id_tax_rules_group'
        );

        $tables_name = array(
            _DB_PREFIX_ . 'tag',
            _DB_PREFIX_ . 'manufacturer',
            _DB_PREFIX_ . 'attribute_lang',
            _DB_PREFIX_ . 'supplier',
            _DB_PREFIX_ . 'feature_value_lang'
        );

        $non_tables = array(
            _DB_PREFIX_ . 'product_tag'
        );

        $query = "SELECT `table_name` as 'table_name'
            FROM `information_schema`.`columns`
            WHERE `table_schema` = '" . _DB_NAME_ . "'
                AND `table_name` LIKE '" . str_replace('_', '\_', _DB_PREFIX_) . "%'
                AND (`column_name` = '" . implode("' OR `column_name` = '", $columns_name) . "')
                " . (!empty($non_tables) ?
                    "AND (`table_name` <> '" . implode("' AND `table_name` <> '", $non_tables) . "')"
                    : "") . "
                " . (!empty($tables_name)
                    ? "OR (`table_name` = '" . implode("' OR `table_name` = '", $tables_name) . "')"
                    : "") . " GROUP BY `table_name`";

        $listModules = Db::getInstance()->executeS(
            $query
        );

        // Assign Positions
        $positions = array();
        if (Tools::getValue('CPL_GLOBAL_ATT')) {
            $positions['attributes'] = explode(';', Tools::getValue('CPL_GLOBAL_ATT'));
            $positions['tables'] = explode(';', Tools::getValue('CPL_GLOBAL_TAB'));
            $positions['titles'] = explode(';', Tools::getValue('CPL_GLOBAL_TIT'));
            $positions['types'] = explode(';', Tools::getValue('CPL_GLOBAL_TYP'));
        }

        // Assign the default table (table_name)
        $selected_table = Tools::getValue('selecModule') ?
        Tools::getValue('selecModule') : $listModules[0]['table_name'];
        $attributes_available = array();

        if (!Tools::getValue('default') && Tools::getValue('selecModule')) {
            // Assign the available attributes
            $attributes_available[$selected_table] = Db::getInstance()->executeS(
                "SHOW columns FROM `" . pSQL($selected_table) . "`"
            ); // For new tab
        } else {
            $attributes_available[_DB_PREFIX_ . 'product_lang'] = Db::getInstance()->executeS(
                "SHOW columns FROM `" . _DB_PREFIX_ . "product_lang` WHERE `Field` NOT LIKE 'id_%'"
            );
            $attributes_available[_DB_PREFIX_ . 'image'] = Db::getInstance()->executeS(
                "SHOW columns FROM `" . _DB_PREFIX_ . "image` WHERE `Field` = 'id_image'"
            );
            $attributes_available[_DB_PREFIX_ . 'product'] = Db::getInstance()->executeS(
                "SHOW columns FROM `" . _DB_PREFIX_ . "product` WHERE `Field` NOT LIKE 'id_%'
                AND `Field` NOT LIKE 'quantity'"
            );
            $attributes_available[_DB_PREFIX_ . 'stock_available'] = Db::getInstance()->executeS(
                "SHOW columns FROM `" . _DB_PREFIX_ . "stock_available` WHERE `Field` NOT LIKE 'id_%'"
            );
            $attributes_available[_DB_PREFIX_ . 'category_lang'] = Db::getInstance()->executeS(
                "SHOW columns FROM `" . _DB_PREFIX_ . "category_lang` WHERE `Field` NOT LIKE 'id_%'"
            );

            $attributes_available[_DB_PREFIX_ . 'attribute_group_name'] = Db::getInstance()->executeS(
                "SELECT `name` AS 'Field'
                FROM `" . _DB_PREFIX_ . "attribute_group_lang`
                WHERE `id_lang` = " . (int)$this->context->language->id
            );
            $attributes_available[_DB_PREFIX_ . 'attribute_lang'] = Db::getInstance()->executeS(
                "SHOW columns FROM `" . _DB_PREFIX_ . "attribute_lang` WHERE `Field` NOT LIKE 'id_%'"
            );

            $attributes_available[_DB_PREFIX_ . 'feature_name'] = Db::getInstance()->executeS(
                "SELECT `name` AS 'Field'
                FROM `" . _DB_PREFIX_ . "feature_lang`
                WHERE `id_lang` = " . (int)$this->context->language->id
            );
            $attributes_available[_DB_PREFIX_ . 'feature_value_lang'] = Db::getInstance()->executeS(
                "SHOW columns FROM `" . _DB_PREFIX_ . "feature_value_lang` WHERE `Field` NOT LIKE 'id_%'"
            );
        }

        return $this->context->smarty->assign(array(
            'modules' => $listModules,
            'selected_table' => $selected_table,
            'prefix' => _DB_PREFIX_,
            'refresh' => 'index.php?controller=AdminCustomProductsListConfig&add' . $this->table . '&token=' .
              Tools::getAdminTokenLite('AdminCustomProductsListConfig') . '&refresh=1',
            'attributes_available' => $attributes_available,
            'positions' => $positions,
            'types' => $this->types,
            'link_to_module' => 'index.php?controller=' . $this->tabClassName . '&token=' .
            Tools::getAdminTokenLite('AdminCustomProductsListConfig'),
            'default_list' => $this->getDefaultList()
        ));
    }

    public function renderForm()
    {
        if ($this->assignation()) {
            $admin_form = $this->context->smarty->fetch($this->module->getLocalPath() .
            'views/templates/admin/admin_form/available_tables.tpl');
            $admin_form .= $this->context->smarty->fetch($this->module->getLocalPath() .
            'views/templates/admin/admin_form/available_attributes.tpl');
            $admin_form .= $this->context->smarty->fetch($this->module->getLocalPath() .
            'views/templates/admin/admin_form/position_attributes.tpl');
            $script = $this->context->smarty->fetch($this->module->getLocalPath() .
              'views/templates/admin/admin_form/configure.tpl');

            return $admin_form . $script;
        } elseif (isset($this->warning) || isset($this->success) || isset($this->errors)) {
            $link = new Link();
            $link = $link->getAdminLink('AdminCustomProductsListConfig', true);

            return Tools::redirectAdmin($link);
        }
    }

    public function renderList()
    {
        if (!$this->context->cookie->__isset('cpl_list')) {
            if (version_compare(_PS_VERSION_, '1.7.6.0', '<')) {
                $link = $this->context->link->getAdminLink('AdminModules', true) .
                    '&configure=customproductslist';
            } else {
                $link = $this->context->link->getAdminLink(
                    'AdminModules',
                    true,
                    [],
                    ['configure' => 'customproductslist']
                );
            }
            Tools::redirectAdmin($link);
        } else {
            $this->_where = " AND `list` = '" . pSQL($this->context->cookie->__get('cpl_list')) . "'";
        }
        if (Tools::isSubmit('CPL_GLOBAL')) {
            $cpl = new CPLConfig();
            if ($cpl->add()) {
                $this->success[] = $this->l("Successfully added");
                $this->context->smarty->assign(array(
                    'success' => $this->success
                ));
            } else {
                $this->errors[] = $this->l("There was an error");
                $this->context->smarty->assign(array(
                    'errors' => $this->errors
                ));
            }
        }

        if (version_compare(_PS_VERSION_, '1.6.0.12', '<')) {
            $id_table = '#customproductslist';
            $id_form = '#customproductslist';
        } else {
            $id_table = '#table-customproductslist';
            $id_form = '#form-customproductslist';
        }

        $this->addRowAction('Delete');

        $align = array(
            '0' => $this->l('Left'),
            '1' => $this->l('Center'),
            '2' => $this->l('Right'),
        );

        $this->context->smarty->assign(array(
            'types' => $this->types,
            'aligns' => $align,
            'quick_add' => $this->quick_add,
            'description' => $this->getDefaultList(),
            'id_table' => $id_table,
            'id_form' => $id_form,
            'feature_name' => Db::getInstance()->executeS(
                "SELECT `name`
                FROM `" . _DB_PREFIX_ . "feature_lang`
                WHERE `id_lang` = " . (int)$this->context->language->id
            ),
            'attribute_group_name' => Db::getInstance()->executeS(
                "SELECT `name`
                FROM `" . _DB_PREFIX_ . "attribute_group_lang`
                WHERE `id_lang` = " . (int)$this->context->language->id
            )
        ));
        $renderList = $this->context->smarty->fetch($this->module->getLocalPath() .
            'views/templates/admin/renderList.tpl');
        $global_form = $this->context->smarty->fetch($this->module->getLocalPath() .
            'views/templates/admin/global_form.tpl');
        $alert = $this->context->smarty->fetch($this->module->getLocalPath() .
            'views/templates/admin/alert.tpl');
        return $alert . $global_form . parent::renderList() . $renderList;
    }

    public function initProcess()
    {
        if (Tools::isSubmit('submitAddcustomproductslist') || Tools::isSubmit('CPL_GLOBAL')) {
            $lastPosition = CPLConfig::getLastPosition();
            $i = 1;
            $positions = array();
            $attributes = array();
            $tables = array();
            $titles = array();
            $types = array();
        }

        if (Tools::isSubmit('submitAddcustomproductslist')) {
            while (Tools::getValue('CPL_ATTRIBUTE_' . $i)) {
                if (version_compare(_PS_VERSION_, '1.6.1.0', '<')) {
                    $positions[] = $lastPosition + $i;
                } else {
                    $positions[] = $lastPosition + $i - 1;
                }
                $attributes[] = Tools::getValue('CPL_ATTRIBUTE_' . $i);
                $tables[] = Tools::getValue('CPL_TABLE_' . $i);
                $titles[] = Tools::getValue('CPL_TITLE_' . $i);
                $types[] = Tools::getValue('CPL_TYPE_' . $i);
                ++$i;
            }
        } elseif (Tools::isSubmit('CPL_GLOBAL')) {
            if (version_compare(_PS_VERSION_, '1.6.1.0', '<')) {
                $positions[] = $lastPosition + 1;
            } else {
                $positions[] = $lastPosition;
            }

            $attribute_to_add = explode('-', Tools::getValue('CPL_QUICK_ADD'), 2);
            $default_list = $this->getDefaultList();
            $value = Tools::getValue('CPL_QUICK_ADD');

            if (array_key_exists($value, $default_list)) {
                $value = $default_list[$value];
            } else {
                $value = array(
                    'type' => false,
                    'title' => $attribute_to_add[1]
                );
            }

            $tables[] = $attribute_to_add[0];
            $attributes[] = $attribute_to_add[1];
            $titles[] = isset($value['title']) ? $value['title'] : "";
            $types[] = isset($value['type']) ? $value['type'] : false;
        }

        if (Tools::isSubmit('submitAddcustomproductslist') || Tools::isSubmit('CPL_GLOBAL')) {
            $_POST['positions'] = implode(';', $positions);
            $_POST['attributes'] = implode(';', $attributes);
            $_POST['tables'] = implode(';', $tables);
            $_POST['titles'] = implode(';', $titles);
            $_POST['types'] = implode(';', $types);
        }

        return parent::initProcess();
    }

    public function ajaxProcessUpdatePositions()
    {
        $way = (int)Tools::getValue('way');
        $id_customproductslist = (int)Tools::getValue('id');
        $positions = Tools::getValue('customproductslist');

        $new_positions = array();
        foreach ($positions as $v) {
            if (count(explode('_', $v)) == 4) {
                $new_positions[] = $v;
            }
        }

        foreach ($new_positions as $position => $value) {
            $pos = explode('_', $value);

            if (isset($pos[2]) && (int)$pos[2] === $id_customproductslist) {
                if ($cpl = new CPLConfig()) {
                    Configuration::updateValue('id_customproductslist', $id_customproductslist);
                    if (isset($position) && $cpl->updatePosition(
                        $way,
                        $position
                    )) {
                        echo 'ok position '.(int)$position.' for id '.(int)$pos[1].'\r\n';
                    } else {
                        echo '{"hasError" : true, "errors" : "Can not update id ' . (int)$id_customproductslist .
                        ' to position ' . (int)$position . ' "}';
                    }
                }

                break;
            }
        }
    }

    public function ajaxProcessSendInputList()
    {
        // AJAX DATAS :
        // CPL_input
        // id_customproductslist
        // attribute

        Db::getInstance()->execute(
            "UPDATE `" . _DB_PREFIX_ . $this->table . "`
            SET `" . pSQL(Tools::getValue('attribute')) . "` = '" . pSQL(Tools::getValue('CPL_input')) . "'
            WHERE `id_customproductslist` = '" . (int)Tools::getValue('id_customproductslist') . "'"
        );

        die(true);
    }

    public function postProcess()
    {
        if (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
            Configuration::updateValue('id_customproductslist', (int)Tools::getValue('id_customproductslist'));
        }

        parent::postProcess();
    }

    public function getDefaultList()
    {
        return array(
            //CUSTOMPRODUCTSLIST
            'customproductslist-final_price_discount' => array(
                'description' => $this->l('Final price with discounts'),
                'title' => $this->l('Final Price'),
                'type' => 'price'
            ),
            'customproductslist-discount_reduction' => array(
                'description' => $this->l('Discount amount'),
                'title' => $this->l('Discount amount'),
                'type' => false // DO NOT CHANGE
            ),
            'customproductslist-final_price_discount_tax_excl' => array(
                'description' => $this->l('Final price with discounts (Tax Excl.)'),
                'title' => $this->l('Final price (Tax Excl.)'),
                'type' => 'price'
            ),
            'customproductslist-categories' => array(
                'description' => $this->l('Categories'),
                'title' => $this->l('Categories'),
                'type' => false
            ),
            'customproductslist-unit_price' => array(
                'description' => $this->l('Unit price'),
                'title' => $this->l('Unit price'),
                'type' => 'price'
            ),

            //PRODUCT
            'product-on_sale' => array(
                'description' => $this->l('Display the "On sale!" flag on the product page, and on product listings.'),
                'title' => $this->l('On sale'),
                'type' => 'boolean',
            ),
            'product-ean13' => array(
                'description' => 'EAN-13',
                'title' => 'EAN-13',
                'type' => false,
            ),
            'product-isbn' => array(
                'description' => 'ISBN',
                'title' => 'ISBN',
                'type' => false,
            ),
            'product-upc' => array(
                'description' => 'UPC',
                'title' => 'UPC',
                'type' => false,
            ),
            'product-ecotax' => array(
                'description' => $this->l('Ecotax'),
                'type' => false,
            ),
            'product-minimal_quantity' => array(
                'description' => $this->l('Minimal quantity for order'),
                'type' => 'integer',
            ),
            'product-low_stock_alert' => array(
                'description' => $this->l('Low stock alert'),
                'type' => false,
            ),
            'product-price' => array(
                'description' => $this->l('Price (Tax Excl.)'),
                'title' => $this->l('Price (Tax Excl.)'),
                'type' => 'price',
            ),
            'product-final_price' => array(
                'description' => $this->l('Price (Tax Incl.)'),
                'title' => $this->l('Price (Tax Incl.)'),
                'type' => 'price',
            ),
            'product-wholesale_price' => array(
                'description' => $this->l('Cost price'),
                'title' => $this->l('Cost price'),
                'type' => 'price',
            ),
            'product-additional_shipping_cost' => array(
                'description' => $this->l('Additional shipping cost'),
                'title' => $this->l('Additional shipping cost'),
                'type' => 'price',
            ),
            'product-reference' => array(
                'description' => $this->l('Reference'),
                'title' => $this->l('Reference'),
                'type' => false,
            ),
            'product-supplier_reference' => array(
                'description' => $this->l('Supplier reference'),
                'type' => false,
            ),
            'product-location' => array(
                'description' => $this->l('Stock location') . " " . $this->l('of the product'),
                'title' => $this->l('Location'),
                'type' => false,
            ),
            'product-width' => array(
                'description' => $this->l('Width'),
                'title' => $this->l('Width'),
                'type' => 'decimal',
            ),
            'product-height' => array(
                'description' => $this->l('Height'),
                'title' => $this->l('Height'),
                'type' => 'decimal',
            ),
            'product-depth' => array(
                'description' => $this->l('Depth'),
                'title' => $this->l('Depth'),
                'type' => 'decimal',
            ),
            'product-weight' => array(
                'description' => $this->l('Weight'),
                'title' => $this->l('Weight'),
                'type' => 'decimal',
            ),
            'product-additional_delivery_times' => array(
                'description' => $this->l('Is there additional delivery times?'),
                'type' => 'boolean',
            ),
            'product-quantity_discount' => array(
                'description' => $this->l('Is there a quantity discount?'),
                'type' => 'boolean',
            ),
            'product-active' => array(
                'description' => $this->l('Is it active? (State)'),
                'title' => $this->l('Status'),
                'type' => 'boolean',
            ),
            'product-available_for_order' => array(
                'description' => $this->l('Is it available for order?'),
                'type' => 'boolean',
            ),
            'product-available date' => array(
                'description' => $this->l('Available date'),
                'type' => 'date',
            ),
            'product-date_add' => array(
                'description' => $this->l('Date when the product was created'),
                'type' => 'date',
            ),
            'product-date_upd' => array(
                'description' => $this->l('Date when the product was updated'),
                'type' => 'date',
            ),
            'product-unity' => array(
                'description' => $this->l('Unity'),
                'title' => $this->l('Unity'),
                'type' => false,
            ),
            'product-is_virtual' => array(
                'description' => $this->l('Is it virtual?'),
                'title' => $this->l('Virtual'),
                'type' => 'boolean',
            ),

            //PRODUCT_LANG
            'product_lang-description' => array(
                'description' => $this->l('Description of the product'),
                'type' => false,
            ),
            'product_lang-description_short' => array(
                'description' => $this->l('Short description of the product'),
                'type' => false,
            ),
            'product_lang-name' => array(
                'description' => $this->l('Name of the product'),
                'title' => $this->l('Name'),
                'type' => false,
            ),

            //CATEGORY_LANG
            'category_lang-name' => array(
                'description' => $this->l('Name of the default category'),
                'title' => $this->l('Category'),
                'type' => false,
            ),
            'description-name' => array(
                'description' => $this->l('Description of the category'),
                'type' => false,
            ),

            //CATEGORY_PRODUCT
            'category_product-position' => array(
                'description' => $this->l('Position') .
                    ' (' . $this->l('only displayed when a category is selected from the tree above the list') . ')',
                'title' => $this->l('Position'),
                'type' => false,
            ),

            //IMAGE
            'image-id_image' => array(
                'description' => $this->l('Image of the product'),
                'title' => $this->l('Image'),
                'type' => false,
            ),

            //STOCK_AVAILABLE
            'stock_available-quantity' => array(
                'description' => $this->l('Quantity of the product'),
                'title' => $this->l('Quantity'),
                'type' => 'integer',
            ),
            'stock_available-physical_quantity' => array(
                'description' => $this->l('Physical quantity of the product'),
                'type' => 'integer',
            ),
            'stock_available-reserved_quantity' => array(
                'description' => $this->l('Reserved quantity (sells include)'),
                'type' => 'integer',
            ),
            'product_attribute-location' => array(
                'description' => $this->l('Stock location') . " " . $this->l('of the combinations'),
                'title' => $this->l('Location'),
                'type' => false,
            ),

            //CUSTOMER_THREAD
            'customer_thread-status' => array(
                'description' => $this->l('Status'),
                'type' => 'thread-status',
            ),
            'customer_thread-email' => array(
                'description' => $this->l('E-mail'),
                'type' => false,
            ),
            'customer_thread-date_add' => array(
                'description' => $this->l('Date when the thread was created'),
                'type' => 'date',
            ),
            'customer_thread-date_upd' => array(
                'description' => $this->l('Date when the thread was updated'),
                'type' => 'date',
            ),

            //TAG
            'tag-name' => array(
                'description' => $this->l('Tags of the product'),
                'title' => $this->l('Tags'),
                'type' => false,
            ),

            //MANUFACTURER
            'manufacturer-name' => array(
                'description' => $this->l('The name of the manufacturer') . ' (' . $this->l('Brand') . ')',
                'title' => $this->l('Brand'),
                'type' => false,
            ),

            //SUPPLIER
            'supplier-name' => array(
                'description' => $this->l('The name of the suppliers'),
                'title' => $this->l('Suppliers'),
                'type' => false,
            ),

            //ATTRIBUTE_LANG
            'attribute_lang-name' => array(
                'description' => $this->l('All combinations'),
                'title' => $this->l('Combinations'),
                'type' => false,
            ),

            //FEATURE_VALUE_LANG
            'feature_value_lang-value' => array(
                'description' => $this->l('All features'),
                'title' => $this->l('Features'),
                'type' => false,
            ),

            //TAX_RULES_GROUP
            'tax_rules_group-name' => array(
                'description' => $this->l('Tax'),
                'title' => $this->l('Tax'),
                'type' => false,
            ),

            //CARRIER
            'carrier-name' => array(
                'description' => $this->l('Carriers'),
                'title' => $this->l('Carriers'),
                'type' => false,
            ),
        );
    }

    public function ajaxProcessUpdateValue()
    {
        if (!Tools::getValue('CPL_value') && Tools::getValue('CPL_value') != '0') {
            die(json_encode(array('success' => 0, 'text' => $this->l('Please enter a value.'))));
        }

        $objects = array(
            'product' => Product::class,
            'product_lang' => Product::class,
            'product_shop' => Product::class,
            'stock_available' => StockAvailable::class
        );

        $update = $this->ajaxUpdate();

        if (array_key_exists($update['table'], $objects)) {
            $obj = new $objects[$update['table']]($update['id_primary_key']);

            if (!Validate::isLoadedObject($obj)) {
                die(json_encode(array('success' => 0, 'text' => $this->l('An error has occurred.'))));
            }

            switch ($update['table'] . '-' . $update['attribute']) {
                case 'stock_available-quantity':
                    if (Validate::isUnsignedInt(Tools::getValue('CPL_value'))) {
                        StockAvailable::setQuantity((int)$update['id_product'], 0, Tools::getValue('CPL_value'));
                    } else {
                        die(json_encode(array('success' => 0, 'text' => $this->l('An error has occurred.'))));
                    }
                    break;
                case 'product_lang-name':
                    Db::getInstance()->execute(
                        "UPDATE `" . _DB_PREFIX_ . "product_lang` 
                        SET `name` = '" . pSQL(Tools::getValue('CPL_value')) . "'
                        WHERE `id_product` = " . (int)$update['id_product'] . "
                        AND `id_lang` = " . (int)$this->context->language->id
                    );
                    break;
                default:
                    $obj->{$update['attribute']} = pSQL(Tools::getValue('CPL_value'));

                    try {
                        $obj->update();

                        switch ($update['attribute']) {
                            case 'location':
                                StockAvailable::setLocation(
                                    (int)$update['id_product'],
                                    pSQL(Tools::getValue('CPL_value'))
                                );
                                break;
                        }
                    } catch (Exception $e) {
                        die(json_encode(array('success' => 0, 'text' => $e->getMessage())));
                    }
                    break;
            }
        } else {
            $count = Db::getInstance()->getValue(
                "SELECT COUNT(*)
                FROM `" . _DB_PREFIX_ . pSQL($update['table']) . "`
                WHERE 1" . $update['where_update']
            );

            if ($count > 1 || $count <= 0) {
                die(json_encode(array('success' => 0, 'text' => $this->l('You can\'t edit this value.'))));
            } else {
                Db::getInstance()->execute(
                    "UPDATE `" . _DB_PREFIX_ . pSQL($update['table']) . "`
                    SET `" . pSQL($update['attribute']) . "` = '" . pSQL(Tools::getValue('CPL_value')) . "'
                    WHERE 1" . $update['where_update']
                );
            }
        }

        die(json_encode(array('success' => 1, 'text' => $this->l('Successful update.'))));
    }

    public function ajaxUpdate()
    {
        $table = pSQL(Tools::getValue('table'));
        $attribute = PSQL(Tools::getValue('attribute'));
        $id_product = (int)Tools::getValue('id_product');

        if (($attribute == 'final_price' || $attribute == 'price_final' || $attribute == 'price') &&
            ($table == 'product' || $table == 'product_shop')) {
            $this->ajaxUpdatePrices($id_product, $attribute);
        }

        $exists = Db::getInstance()->getValue(
            "SELECT `column_name`
            FROM `information_schema`.`COLUMNS`
            WHERE `table_schema` = '" . _DB_NAME_ . "'
            AND `table_name` = '" . _DB_PREFIX_ . pSQL($table) . "'
            AND `column_name` = '" . pSQL($attribute) . "'"
        );

        if (!$exists) {
            die(json_encode(array('success' => 0, 'text' => $this->l('This field does not exist.'))));
        }

        $primary_key = Db::getInstance()->getValue(
            "SELECT `column_name`
            FROM `information_schema`.`COLUMNS`
            WHERE `table_schema` = '" . _DB_NAME_ . "'
            AND `table_name` = '" . _DB_PREFIX_ . pSQL($table) . "'
            AND `column_key` = 'PRI'"
        );

        $where = $this->cpl->getDefaultWhere();
        $this->cpl->setDefaultJoin()->addDefaultJoin($table);

        if (!in_array($table, $this->cpl->default_tables)) {
            // JOIN
            $columns = Db::getInstance()->executeS(
                "SHOW COLUMNS FROM `" . _DB_PREFIX_ . pSQL($table) . "`"
            );
            $return = true;

            $base_on = '';
            $join_on = '';
            $join_table = '';
            $lang = false;

            foreach ($this->cpl->join_ids as $join_table_temp => $field) {
                if (is_array($field)) {
                    $join_table_temp = $field['table'];
                } else {
                    $field = array(
                        'base_on' => $field,
                        'join_on' => $field,
                    );
                }

                foreach ($columns as $column) {
                    if ($column['Field'] == $field['join_on'] && $join_on == '') {
                        $base_on = $field['base_on'];
                        $join_on = $field['join_on'];
                        $join_table = $join_table_temp;
                        $this->cpl->addDefaultJoin($join_table);
                        $return = false;
                    } elseif ($column['Field'] == 'id_lang') {
                        $lang = true;
                    }

                    if ($lang && $join_on != '') {
                        break 2;
                    }
                }
            }

            if ($return) {
                die(Tools::jsonEncode(array('success' => 0, 'text' => $this->l('An error has occurred.'))));
            }
            $on = '`' . pSQL($table) . '`.`' . pSQL($join_on) . '` =
                    `' . pSQL($join_table) . '`.`' . pSQL($base_on) . '`';
            if ($lang && preg_match('/.*_lang$/', $table)) {
                $on .= " AND `" . pSQL($table) . "`.`id_lang` = '" . (int)$this->context->language->id . "'";
            }

            $this->cpl->joinQuery(
                'left',
                pSQL($join_table),
                pSQL($table),
                pSQL($table),
                $on
            );
        }

        try {
            $row = Db::getInstance()->getRow(
                "SELECT `" . pSQL($table) . "`.*
                FROM `" . _DB_PREFIX_ . "product` `product`
                LEFT JOIN `" . _DB_PREFIX_ . "product_lang` `product_lang`
                ON `product`.`id_product` = `product_lang`.`id_product`
                AND `product_lang`.`id_lang` = '" . (int)$this->context->language->id . "' " .
                implode(' ', $this->cpl->getJoin()) . " WHERE 1 " . $where .
                (empty($id_product) ? " " : " AND `product`.`id_product` = '" . (int)$id_product . "' ") .
                "GROUP BY `product`.`id_product`
                ORDER BY `product`.`id_product` DESC"
            );
        } catch (\Exception $e) {
            die(json_encode(array('success' => 0, 'text' => $e->getMessage())));
        }
        if (empty($row)) {
            die(json_encode(array('success' => 0, 'text' => $this->l('Unable to change value.'))));
        }

        $where_update = "";
        foreach ($row as $key => $column) {
            if ($key != $attribute && !empty($column)) {
                $where_update .= " AND `" . pSQL($key) . "` = '" . pSQL($column, true) . "'";
            }
        }

        return array(
            'table' => $table,
            'attribute' => $attribute,
            'attribute_value' => $row[$attribute],
            'id_primary_key' =>
                isset($row[$primary_key]) && Validate::isInt($row[$primary_key]) ? (int)$row[$primary_key] : 0,
            'where_update' => $where_update,
            'id_product' => (isset($row['id_product']) ? (int)$row['id_product'] : 0)
        );
    }

    protected function ajaxUpdatePrices($id_product, $attribute)
    {
        $price_ttc = (float)Tools::getValue('CPL_value');
        $product  = new Product($id_product);

        if (!Validate::isLoadedObject($product)) {
            die(json_encode(array('success' => 0, 'text' => $this->l('An error has occured.'))));
        }

        $tax = (float)Db::getInstance()->getValue(
            "SELECT rate
            FROM " . _DB_PREFIX_ . "tax `a`
            LEFT JOIN " . _DB_PREFIX_ . "tax_rule `b`
                ON `a`.`id_tax` = `b`.`id_tax`
                AND `b`.`id_country` = " . (int)$this->context->country->id . "
            WHERE `b`.`id_tax_rules_group` = " . (int)$product->id_tax_rules_group
        );

        $class = '';
        $class_value = 0;
        if ($attribute == 'final_price') {
            $product->price = (float)round((float)($price_ttc * 100 / (100 + $tax)), 9);
            $class_value = $product->price;
            $class = $this->cpl->getAttributeAs('product_shop', 'price');
        } elseif ($attribute == 'price') {
            $product->price = (float)round((float)$price_ttc, 9);
            $class_value = (float)round((float)$price_ttc * (100 + $tax) / 100, 9);
            $class = $this->cpl->getAttributeAs('product', 'final_price');
        }

        try {
            $product->update();
        } catch (Exception $e) {
            die(json_encode(array(
                'success' => 0,
                'text' => $e->getMessage()
            )));
        }

        if (version_compare(_PS_VERSION_, '1.7.6.0', '>=')) {
            if (version_compare(_PS_VERSION_, '1.7.7.0', '<')) {
                $containerFinder = new PrestaShop\PrestaShop\Adapter\ContainerFinder($this->context);
                $container = $containerFinder->getContainer();
                if (null === $this->context->container) {
                    $this->context->container = $container;
                }

                $localeRepository = $container->get(self::SERVICE_LOCALE_REPOSITORY);
                $locale = $localeRepository->getLocale(
                    $this->context->language->getLocale()
                );
            } else {
                $locale = Tools::getContextLocale($this->context);
            }
            $price_currency = $locale->formatPrice($class_value, $this->context->currency->iso_code);
        } else {
            $price_currency = Tools::displayPrice($class_value);
        }

        die(json_encode(array(
            'success' => 1,
            'class' => 'cpl-' . $class,
            'class_value' => $class_value,
            'class_value_currency' => $price_currency
        )));
    }
}
