<?php
/**
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once _PS_MODULE_DIR_ . 'customproductslist/classes/CPL.php';
include_once _PS_MODULE_DIR_ . 'customproductslist/classes/CPLCustom.php';

class AdminCustomProductsListController extends ModuleAdminController
{
    protected $position_identifier = 'id_product';
    protected $cpl;
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'product';
        $this->className = 'Product';
        $this->tabClassName = 'AdminCustomProductsList';
        $this->lang = true;
        $this->deleted = false;
        $this->colorOnBackground = false;
        $this->allow_export = true;
        $this->bulk_actions = array(
        'delete' => array('text' => 'Delete selected', 'confirm' => 'Delete selected items?'),
        );
        $this->context = Context::getContext();

        parent::__construct();

        if (Tools::getIsset('id_category') && !Tools::getValue('ajax')) {
            if (Category::categoryExists((int)Tools::getValue('id_category'))) {
                $this->context->cookie->__set('cpl_id_category', (int)Tools::getValue('id_category'));
            } else {
                $this->context->cookie->__unset('cpl_id_category');
                $this->context->cookie->__unset('customproductslistproductOrderby');
            }
        }

        $this->addRowAction('edit');
        $this->addRowAction('preview');
        $this->addRowAction('duplicate');
        $this->addRowAction('delete');

        $this->cpl = CPL::getInstance('custom');
        $this->cpl->setUseParent(true);
    }

    public function displayPreviewLink($token, $id)
    {
        $tpl = $this->createTemplate('helpers/list/list_action_preview.tpl');
        if (!array_key_exists('Bad SQL query', self::$cache_lang)) {
            self::$cache_lang['Preview'] = $this->l('Preview', 'Helper');
        }

        $tpl->assign(array(
            'href' => $this->getPreviewUrl(new Product((int)$id)),
            'action' => self::$cache_lang['Preview'],
        ));

        return $tpl->fetch();
    }

    public function getPreviewUrl(Product $product)
    {
        $id_lang = Configuration::get('PS_LANG_DEFAULT', null, null, Context::getContext()->shop->id);

        if (!ShopUrl::getMainShopDomain()) {
            return false;
        }

        $is_rewrite_active = (bool)Configuration::get('PS_REWRITING_SETTINGS');
        $preview_url = $this->context->link->getProductLink(
            $product,
            $this->getFieldValue($product, 'link_rewrite', $this->context->language->id),
            Category::getLinkRewrite(
                $this->getFieldValue($product, 'id_category_default'),
                $this->context->language->id
            ),
            null,
            $id_lang,
            (int)Context::getContext()->shop->id,
            0,
            $is_rewrite_active
        );

        if (!$product->active) {
            $admin_dir = dirname($_SERVER['PHP_SELF']);
            $admin_dir = Tools::substr($admin_dir, strrpos($admin_dir, '/') + 1);
            $preview_url .= ((strpos($preview_url, '?') === false) ? '?' : '&').
                'adtoken='.$this->token.'&ad='.$admin_dir.'&id_employee='.(int)$this->context->employee->id;
        }

        return $preview_url;
    }

    public function processDuplicate()
    {
        if (Validate::isLoadedObject($product = new Product((int) Tools::getValue('id_product')))) {
            $id_product_old = $product->id;
            if (empty($product->price) && Shop::getContext() == Shop::CONTEXT_GROUP) {
                $shops = ShopGroup::getShopsFromGroup(Shop::getContextShopGroupID());
                foreach ($shops as $shop) {
                    if ($product->isAssociatedToShop($shop['id_shop'])) {
                        $product_price = new Product($id_product_old, false, null, $shop['id_shop']);
                        $product->price = $product_price->price;
                    }
                }
            }
            unset(
                $product->id,
                $product->id_product
            );

            $product->indexed = 0;
            $product->active = 0;
            if ($product->add()
                && Category::duplicateProductCategories($id_product_old, $product->id)
                && Product::duplicateSuppliers($id_product_old, $product->id)
                && ($combination_images = Product::duplicateAttributes($id_product_old, $product->id)) !== false
                && GroupReduction::duplicateReduction($id_product_old, $product->id)
                && Product::duplicateAccessories($id_product_old, $product->id)
                && Product::duplicateFeatures($id_product_old, $product->id)
                && Product::duplicateSpecificPrices($id_product_old, $product->id)
                && Pack::duplicate($id_product_old, $product->id)
                && Product::duplicateCustomizationFields($id_product_old, $product->id)
                && Product::duplicateTags($id_product_old, $product->id)
                && Product::duplicateDownload($id_product_old, $product->id)) {
                if ($product->hasAttributes()) {
                    Product::updateDefaultAttribute($product->id);
                }

                if (!Tools::getValue('noimage') &&
                    !Image::duplicateProductImages($id_product_old, $product->id, $combination_images)) {
                    $this->errors[] =
                        $this->trans('An error occurred while copying the image.', [], 'Admin.Notifications.Error');
                } else {
                    Hook::exec(
                        'actionProductAdd',
                        ['id_product_old' => $id_product_old, 'id_product' => (int) $product->id, 'product' => $product]
                    );
                    if (in_array($product->visibility, ['both', 'search']) &&
                        Configuration::get('PS_SEARCH_INDEXATION')) {
                        Search::indexation(false, $product->id);
                    }
                    $this->redirect_after = self::$currentIndex .
                        (Tools::getIsset('id_category') ? '&id_category=' .
                            (int) Tools::getValue('id_category') : '') . '&conf=19&token=' . $this->token;
                }
            } else {
                $this->errors[] =
                    $this->trans('An error occurred while creating an object.', [], 'Admin.Notifications.Error');
            }
        }
    }

    public function processDelete()
    {
        if (Validate::isLoadedObject($object = $this->loadObject()) && isset($this->fieldImageSettings)) {
            /** @var Product $object */
            // check if request at least one object with noZeroObject
            if (isset($object->noZeroObject) &&
                count(call_user_func([$this->className, $object->noZeroObject])) <= 1) {
                $this->errors[] =
                    $this->trans(
                        'You need at least one object.',
                        [],
                        'Admin.Notifications.Error'
                    ) . ' <b>' . $this->table . '</b><br />' .
                    $this->trans('You cannot delete all of the items.', [], 'Admin.Notifications.Error');
            } else {
                /*
                 * @since 1.5.0
                 * It is NOT possible to delete a product if there are currently:
                 * - physical stock for this product
                 * - supply order(s) for this product
                 */
                if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && $object->advanced_stock_management) {
                    $stock_manager = StockManagerFactory::getManager();
                    $physical_quantity = $stock_manager->getProductPhysicalQuantities($object->id, 0);
                    $real_quantity = $stock_manager->getProductRealQuantities($object->id, 0);
                    if ($physical_quantity > 0 || $real_quantity > $physical_quantity) {
                        $this->errors[] =
                            $this->trans(
                                'You cannot delete this product because there is physical stock left.',
                                [],
                                'Admin.Catalog.Notification'
                            );
                    }
                }

                if (!count($this->errors)) {
                    if ($object->delete()) {
                        $id_category = (int) Tools::getValue('id_category');
                        $category_url = empty($id_category) ? '' : '&id_category=' . (int) $id_category;
                        PrestaShopLogger::addLog(
                            sprintf('%s deletion', $this->className),
                            1,
                            null,
                            $this->className,
                            (int) $object->id,
                            true,
                            (int) $this->context->employee->id
                        );
                        $this->redirect_after = self::$currentIndex . '&conf=1&token=' . $this->token . $category_url;
                    } else {
                        $this->errors[] =
                            $this->trans('An error occurred during deletion.', [], 'Admin.Notifications.Error');
                    }
                }
            }
        } else {
            $this->errors[] = $this->trans(
                'An error occurred while deleting the object.',
                [],
                'Admin.Notifications.Error'
            ) . ' <b>' . $this->table . '</b> ' .
                $this->trans('(cannot load object)', [], 'Admin.Notifications.Error');
        }
    }

    public function init()
    {
        if (!$this->ajax) {
            // Do not move, otherwise the filters will not work
            $this->cpl->setDefaultJoin()
                ->setQuery();

            if (empty($this->_select)) {
                $this->_select = "";
            }
            if (empty($this->_join)) {
                $this->_join = "";
            }
            if (empty($this->_where)) {
                $this->_where = "";
            }

            $this->_select .= implode(', ', $this->cpl->getSelect());
            $this->_join .= implode(' ', $this->cpl->getJoin());
            $this->_where .= $this->cpl->getDefaultWhere();
            $this->_group = $this->cpl->getDefaultGroupBy();
            $this->_orderBy = $this->cpl->getDefaultOrderBy();
            $this->_orderWay = $this->cpl->getDefaultOrderWay();

            $this->fields_list = $this->cpl->getFields();
        }

        parent::init();
    }

    public function initProcess()
    {
        if (Tools::getIsset('duplicate' . $this->table)) {
            // Product duplication
            if ($this->access('add')) {
                $this->action = 'duplicate';
            } else {
                $this->errors[] = $this->trans(
                    'You do not have permission to add this.',
                    [],
                    'Admin.Notifications.Error'
                );
            }
        }

        parent::initProcess();
    }

    public function getList(
        $id_lang,
        $order_by = null,
        $order_way = null,
        $start = 0,
        $limit = null,
        $id_lang_shop = false
    ) {
        parent::getList($id_lang, $order_by, $order_way, $start, $limit, (int)$this->context->shop->id);
    }

    public function renderList()
    {
        $selected_category = isset($this->context->cookie->cpl_id_category) ?
            (int)$this->context->cookie->cpl_id_category :
            0;

        $tree = new HelperTreeCategories('categories-tree', $this->l('Filter by category'));
        $tree->setAttribute('is_category_filter', $selected_category)
            ->setAttribute(
                'base_url',
                preg_replace('#&id_category=[0-9]*#', '', self::$currentIndex).'&token='.$this->token
            )
            ->setInputName('id-category')
            ->setRootCategory(Category::getRootCategory()->id)
            ->setSelectedCategories(array((int)$selected_category));

        $link = self::$currentIndex . '&token=' . $this->token . '&id_category=0';

        $tree->setActions(array(
            new TreeToolbarLink(
                'Reset',
                $link
            )
        ));

        return $tree->render() . parent::renderList();
    }

    public function displayEnableLink(
        $token,
        $id,
        $value,
        $active,
        $id_category = null,
        $id_product = null,
        $ajax = false
    ) {
        $var = $id_product;
        $var = $id_category;
        $var = $var;
        if (preg_match('/(\w+);;;(\w+)/', $active, $result)) {
            $infos = "&table=" . (string)$result[1] .
                "&attribute=" . (string)$result[2] .
                "&id_product=" . (int)$id;
            $active = 'custom_boolean';
        } else {
            $active = 'status';
            $infos = "&id_product=" . (int)$id;
        }
        $this->context->smarty->assign(array(
            'ajax' => (bool)$ajax,
            'enabled' => (bool) $value,
            'url_enable' =>
                AdminCustomProductsListController::$currentIndex .
                "&" . $active .
                ($ajax ? "&action=" . $active . "&ajax=" . (int) $ajax : "&action=" . $active) .
                $infos .
                "&token=" . ($token != null ? $token : $this->token),
        ));

        return $this->context->smarty->fetch($this->module->getLocalPath() .
          'views/templates/admin/enableList.tpl');
    }

    public function initContent()
    {
        $this->addJS(_MODULE_DIR_ . 'customproductslist/views/js/tree.js');

        if ($this->context->cookie->__isset('cpl_id_category')) {
            $this->addJS(_MODULE_DIR_ . 'customproductslist/views/js/positions.js');
        }

        parent::initContent();
    }

    public function initToolbar()
    {
        parent::initToolbar();
        if (version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            $link = "index.php/sell/catalog/products/new";

            $id_employee = (int)$this->context->employee->id;

            $url = Context::getContext()->link->getBaseLink() . basename(_PS_ADMIN_DIR_) . '/';

            $this->toolbar_btn['new']['href'] = $url . $link . '?token=' . Tools::getAdminToken($id_employee);
        }
    }

    public function renderForm()
    {
        $link = new Link();
        if (version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            $link = $link->getAdminLink('AdminProducts', true, ['id_product' => Tools::getValue('id_product')]);
        } else {
            $link = $link->getAdminLink('AdminProducts', true) .
                "&id_product=" . Tools::getValue('id_product') . "&updateproduct";
        }

        return Tools::redirectAdmin($link);
    }

    public function ajaxProcessStatus()
    {
        $id_product = Tools::getValue('id_product');

        $active = (int)Db::getInstance()->getValue(
            "SELECT `active`
            FROM `" . _DB_PREFIX_ . "product`
            WHERE `id_product` = '" . (int)$id_product . "'"
        );

        $active = $active == 1 ? 0 : 1;

        Db::getInstance()->execute(
            "UPDATE `" . _DB_PREFIX_ . "product`
            SET `active` = '" . (int)$active . "'
            WHERE `id_product` = '" . (int)$id_product . "'"
        );

        Db::getInstance()->execute(
            "UPDATE `" . _DB_PREFIX_ . "product_shop`
            SET `active` = '" . (int)$active . "'
            WHERE `id_product` = '" . (int)$id_product . "'" .
            Shop::addSqlRestriction(false, false)
        );

        die(Tools::jsonEncode(array('success' => 1, 'text' => $this->l('Successful update.'))));
    }

    public function ajaxProcessCustomBoolean()
    {
        include_once _PS_MODULE_DIR_ . 'customproductslist/controllers/admin/AdminCustomProductsListConfig.php';
        /**
         * @var $controller AdminCustomProductsListConfigController
         */
        $controller = Controller::getController('AdminCustomProductsListConfigController');
        $update = $controller->ajaxUpdate();
//        $update = $this->ajaxUpdate();

        Db::getInstance()->execute(
            "UPDATE `" . _DB_PREFIX_ . pSQL($update['table']) . "`
            SET `" . pSQL($update['attribute']) . "` = '" . ($update['attribute_value'] ? '0' : '1') . "'
            WHERE 1" . $update['where_update']
        );

        if ($update['table'] == 'product') {
            $is_shop = (bool)Db::getInstance()->getValue(
                "SELECT `column_name`
                FROM `information_schema`.`columns`
                WHERE `table_schema` = '" . _DB_NAME_ . "' AND `table_name` = '" . _DB_PREFIX_ . "product_shop'
                AND `column_name` = '" . $update['attribute'] . "'"
            );

            if ($is_shop) {
                Db::getInstance()->execute(
                    "UPDATE " . _DB_PREFIX_ . "product_shop
                    SET `" . pSQL($update['attribute']) . "` = '" . ($update['attribute_value'] ? '0' : '1') . "'
                    WHERE `id_product` = '" . (int)$update['id_product'] . "'" .
                    Shop::addSqlRestriction(false, false)
                );
            }
        }

        die(Tools::jsonEncode(array('success' => 1, 'text' => $this->l('Successful update.'))));
    }

    private function ajaxUpdate()
    {
        $table = pSQL(Tools::getValue('table'));
        $attribute = PSQL(Tools::getValue('attribute'));
        $id_product = (int)Tools::getValue('id_product');

        $exists = Db::getInstance()->getValue(
            "SELECT `column_name`
            FROM `information_schema`.`COLUMNS`
            WHERE `table_schema` = '" . _DB_NAME_ . "'
            AND `table_name` = '" . _DB_PREFIX_ . pSQL($table) . "'
            AND `column_name` = '" . pSQL($attribute) . "'"
        );

        if (!$exists) {
            die(Tools::jsonEncode(array('success' => 0, 'text' => $this->l('An error has occurred.'))));
        }
        $this->cpl->setWithAliases(false);
        $where = $this->cpl->getDefaultWhere();
        $this->cpl->setDefaultJoin();

        if (!in_array($table, $this->cpl->default_tables)) {
            // JOIN
            $columns = Db::getInstance()->executeS(
                "SHOW COLUMNS FROM `" . _DB_PREFIX_ . pSQL($table) . "`"
            );
            $return = true;

            $base_on = '';
            $join_on = '';
            $join_table = '';
            $lang = false;

            foreach ($this->cpl->join_ids as $join_table_temp => $field) {
                if (is_array($field)) {
                    $join_table_temp = $field['table'];
                } else {
                    $field = array(
                        'base_on' => $field,
                        'join_on' => $field,
                    );
                }

                foreach ($columns as $column) {
                    if ($column['Field'] == $field['join_on'] && $join_on == '') {
                        $base_on = $field['base_on'];
                        $join_on = $field['join_on'];
                        $join_table = $join_table_temp;
                        $this->cpl->addDefaultJoin($join_table);
                        $return = false;
                    } elseif ($column['Field'] == 'id_lang') {
                        $lang = true;
                    }

                    if ($lang && $join_on != '') {
                        break 2;
                    }
                }
            }

            if ($return) {
                die(Tools::jsonEncode(array('success' => 0, 'text' => $this->l('An error has occurred.'))));
            }
            $on = '`' . pSQL($table) . '`.`' . pSQL($join_on) . '` =
                    `' . pSQL($join_table) . '`.`' . pSQL($base_on) . '`';
            if ($lang && preg_match('/.*_lang$/', $table)) {
                $on .= " AND `" . pSQL($table) . "`.`id_lang` = '" . (int)$this->context->language->id . "'";
            }

            $this->cpl->joinQuery(
                'left',
                pSQL($join_table),
                pSQL($table),
                pSQL($table),
                $on
            );
        }

        try {
            $row = Db::getInstance()->getRow(
                "SELECT `" . pSQL($table) . "`.*
                FROM `" . _DB_PREFIX_ . "product` `product`
                LEFT JOIN `" . _DB_PREFIX_ . "product_lang` `product_lang`
                ON `product`.`id_product` = `product_lang`.`id_product`
                AND `product_lang`.`id_lang` = '" . (int)$this->context->language->id . "' " .
                implode(' ', $this->cpl->getJoin()) . " WHERE 1 " . $where .
                (empty($id_product) ? " " : " AND `product`.`id_product` = '" . (int)$id_product . "' ") .
                "GROUP BY `product`.`id_product`
                ORDER BY `product`.`id_product` DESC"
            );
        } catch (\Exception $e) {
            die(Tools::jsonEncode(array('success' => 0, 'text' => $e->getMessage())));
        }
        if (empty($row)) {
            die(Tools::jsonEncode(array('success' => 0, 'text' => $this->l('Unable to change value.'))));
        }
        $where_update = "";
        foreach ($row as $key => $column) {
            if ($key != $attribute && !empty($column)) {
                $where_update .= " AND `" . pSQL($key) . "` = '" . pSQL($column, true) . "'";
            }
        }

        return array(
            'table' => $table,
            'attribute' => $attribute,
            'attribute_value' => $row[$attribute],
            'where_update' => $where_update,
            'id_product' => (isset($row['id_product']) ? (int)$row['id_product'] : 0)
        );
    }

    public function ajaxProcessUpdatePositions()
    {
        $way = (int)(Tools::getValue('way'));
        $id_product = (int)Tools::getValue('id_product');
        $id_category = (int)$this->context->cookie->__get('cpl_id_category');
        $_GET['id_category'] = (int)$id_category;
        $positions = Tools::getValue('product');
        $page = (int)Tools::getValue('page');
        $selected_pagination = (int)Tools::getValue('selected_pagination');

        if (is_array($positions)) {
            foreach ($positions as $position => $value) {
                $pos = explode('_', $value);

                if (isset($pos[2]) && ($id_category && (int)$pos[2] === $id_product)) {
                    $first_position = (int)Db::getInstance()->getValue(
                        "SELECT MIN(`position`)
                            FROM `" . _DB_PREFIX_ . "category_product`"
                    );
                    $position += $first_position;
                    if ($page > 1) {
                        $position = $position + (($page - 1) * $selected_pagination);
                    }

                    if ($product = new Product((int)$pos[2])) {
                        if (isset($position) && $product->updatePosition(
                            $way,
                            $position
                        )) {
                            $category = new Category((int)$id_category);
                            if (Validate::isLoadedObject($category)) {
                                hook::Exec('categoryUpdate', array('category' => $category));
                            }
                            echo 'ok position '.
                                $position.
                                ' for product '.(int)$pos[2]."\r\n";
                        } else {
                            echo '{"hasError" : true, "errors" : "Can not update product '.(int)$id_product.
                                ' to position '.
                                $position.' "}';
                        }
                    } else {
                        echo '{"hasError" : true, "errors" : "This product ('.(int)$id_product.') can t be loaded"}';
                    }

                    break;
                }
            }
        }
    }

    public function processPositionField()
    {
        $position = Tools::getValue('cpl_position');
        $newPosition = Tools::getValue('newPosition');

        $pos = explode('_', $position);
        $first_position = (int)Db::getInstance()->getValue(
            "SELECT MIN(`position`)
                            FROM `" . _DB_PREFIX_ . "category_product`"
        );
        $pos[3] += $first_position;

        if ($pos[3] == $newPosition) {
            $this->errors[] = $this->l('The new position needs to be different.');
            return false;
        } else {
            $way = (int)($newPosition > $pos[3]);
        }

        $id_category = (int)$this->context->cookie->__get('cpl_id_category');
        $id_product = (int)$pos[2];

        $max_position = Db::getInstance()->getValue(
            "SELECT MAX(`position`)
            FROM `" . _DB_PREFIX_ . "category_product`
            WHERE `id_category` = " . (int)$id_category
        );
        if ($newPosition <= 0 || $newPosition > $max_position) {
            $this->errors[] = sprintf($this->l('Please enter a good position (max: %d)'), $max_position);
            return false;
        } else {
            $this->updateProductPosition($id_product, $id_category, $newPosition, $way);

            $link = $this->context->link->getAdminLink('AdminCustomProductsList', true) . "&conf=4";
            Tools::redirectAdmin($link);
        }
    }

    protected function updateProductPosition($id_product, $id_category, $newPosition, $way)
    {
        $_GET['id_product'] = (int)$id_product;
        $_GET['id_category'] = (int)$id_category;

        if ($product = new Product($id_product)) {
            if (isset($newPosition) && $product->updatePosition(
                $way,
                $newPosition
            )) {
                $category = new Category((int)$id_category);
                if (Validate::isLoadedObject($category)) {
                    hook::Exec('categoryUpdate', array('category' => $category));
                }
            } else {
                return false;
            }
        }

        return true;
    }
}
