<?php
/**
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

if (version_compare(_PS_VERSION_, '1.7.6.0', '>=')) {
    include_once _PS_MODULE_DIR_ . 'customproductslist/vendor/autoload.php';
} else {
    include_once _PS_MODULE_DIR_ . 'customproductslist/classes/CPL.php';
    include_once _PS_MODULE_DIR_ . 'customproductslist/classes/CPLCustom.php';
    include_once _PS_MODULE_DIR_ . 'customproductslist/classes/CPL16.php';
}

class Customproductslist extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'customproductslist';
        $this->tab = 'administration';
        $this->version = '1.9.3';
        $this->author = 'Ciren';
        $this->need_instance = 1;
        $this->module_key = '7216304c9a1331a45114374130aa8645';

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Customize the products list');
        $this->description = $this->l('Customize the products list');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall this module?');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => '8.0.99');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        if (!parent::install()) {
            return false;
        }

        include(dirname(__FILE__).'/sql/install.php');

        Db::getInstance()->execute("TRUNCATE TABLE `" . _DB_PREFIX_ . "customproductslist`");
        $this->populateList('custom');
        if (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
            $this->populateList('16');
        } elseif (version_compare(_PS_VERSION_, '1.7.6.0', '>=')) {
            $this->populateList('override');
        }

        $return = true;

        if (version_compare(_PS_VERSION_, '1.7.0.0', '<') || version_compare(_PS_VERSION_, '1.7.6.0', '>=')) {
            $return &= $this->registerHook('actionAdminProductsListingFieldsModifier');
            $return &= $this->registerHook('actionAdminProductsListingResultsModifier');
        }
        $return &= $this->registerHook('actionAdminCustomProductsListListingResultsModifier');
        $return &= $this->addTab();

        $cpl_config = array();
        if (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
            $cpl_config['16'] = '1';
        }
        if (version_compare(_PS_VERSION_, '1.7.6.0', '>=')) {
            $cpl_config['override'] = '1';
        }
        $cpl_config['custom'] = '1';

        Configuration::updateValue('cpl_config', json_encode($cpl_config));

        if (version_compare(_PS_VERSION_, '1.7.6.0', '>=')) {
            Tools::clearSf2Cache('prod');
            Tools::clearSf2Cache('dev');
        }

        if (version_compare(_PS_VERSION_, '1.7.0.0', '>=') && version_compare(_PS_VERSION_, '1.7.6.0', '<')) {
            $this->registerHook('actionModuleUpgradeBefore');
            $this->deleteConfigDir();
        }

        return $return &&
            $this->registerHook('displayBackOfficeHeader');
    }

    public function hookActionModuleUpgradeBefore($params)
    {
        if ($params['module']->name == $this->name && version_compare(_PS_VERSION_, '1.7.6.0', '<')) {
            $this->deleteConfigDir();
        }
    }

    public function deleteConfigDir()
    {
        $files = _PS_MODULE_DIR_ . 'customproductslist/config/*.yml';

        foreach (glob($files) as $file) {
            unlink($file);
        }
    }

    public function populateList($list)
    {
        if (version_compare(_PS_VERSION_, '1.6.1.0', '<')) {
            $position = 1;
        } else {
            $position = 0;
        }
        $sql = array();
        $sql[] = array(
            'list' => $list,
            'position' => $position,
            'attribute' => 'id_image',
            'table' => 'image',
            'title' => $this->l('Image'),
            'type' => 'image',
            'align' => '1'
        );
        $sql[] = array(
            'list' => $list,
            'position' => $position + 1,
            'attribute' => 'name',
            'table' => 'product_lang',
            'title' => $this->l('Name'),
            'type' => '',
            'align' => '0'
        );
        $sql[] = array(
            'list' => $list,
            'position' => $position + 2,
            'attribute' => 'reference',
            'table' => 'product',
            'title' => $this->l('Reference'),
            'type' => '',
            'align' => '0'
        );
        $sql[] = array(
            'list' => $list,
            'position' => $position + 3,
            'attribute' => 'name',
            'table' => 'category_lang',
            'title' => $this->l('Category'),
            'type' => '',
            'align' => '0'
        );
        $sql[] = array(
            'list' => $list,
            'position' => $position + 4,
            'attribute' => 'price',
            'table' => 'product_shop',
            'title' => $this->l('Price'),
            'type' => 'price',
            'align' => '1'
        );
        $sql[] = array(
            'list' => $list,
            'position' => $position + 5,
            'attribute' => 'final_price',
            'table' => 'product',
            'title' => $this->l('Final price'),
            'type' => 'price',
            'align' => '1'
        );
        $sql[] = array(
            'list' => $list,
            'position' => $position + 6,
            'attribute' => 'quantity',
            'table' => 'stock_available',
            'title' => $this->l('Quantity'),
            'type' => 'integer',
            'align' => '1'
        );
        $sql[] = array(
            'list' => $list,
            'position' => $position + 7,
            'attribute' => 'position',
            'table' => 'category_product',
            'title' => $this->l('Position'),
            'type' => '',
            'align' => '1'
        );
        $sql[] = array(
            'list' => $list,
            'position' => $position + 8,
            'attribute' => 'active',
            'table' => 'product_shop',
            'title' => $this->l('Status'),
            'type' => 'boolean',
            'align' => '1'
        );

        foreach ($sql as $q) {
            Db::getInstance()->insert('customproductslist', $q);
        }
    }

    private function addTab()
    {
        if (Tab::getIdFromClassName('AdminLists') === false) {
            $tab = new Tab();
            $tab->class_name = 'AdminLists';
            $tab->icon = 'view_list';
            $tab->id_parent = (int)Tab::getIdFromClassName('IMPROVE');
            $tab->name[(int) Configuration::get('PS_LANG_DEFAULT')] = $this->l('Lists');
            $tab->add();
        }

        if (Tab::getIdFromClassName('AdminCustomProductsListConfig') === false) {
            $tab = new Tab();
            $tab->class_name = 'AdminCustomProductsListConfig';
            $tab->id_parent = (int)Tab::getIdFromClassName('AdminLists');
            $tab->module = $this->name;
            $tab->name[(int) Configuration::get('PS_LANG_DEFAULT')] = $this->l('Customize products columns');
            $tab->add();
        }

        if (Tab::getIdFromClassName('AdminCustomProductsList') === false) {
            $tab = new Tab();
            $tab->class_name = 'AdminCustomProductsList';
            $tab->id_parent = Tab::getIdFromClassName('AdminCatalog');
            $tab->module = $this->name;
            $tab->name[(int) Configuration::get('PS_LANG_DEFAULT')] = $this->l('Products custom');
            $tab->add();
        }

        return true;
    }

    protected function addCustomProductsTab()
    {
        $id_tab = Tab::getIdFromClassName('AdminCustomProductsList');
        $tab = new Tab($id_tab);

        if (Validate::isLoadedObject($tab)) {
            $tab->active = 1;
            $tab->update();
        }

        return true;
    }

    protected function removeCustomProductsTab()
    {
        $id_tab = Tab::getIdFromClassName('AdminCustomProductsList');
        $tab = new Tab($id_tab);

        if (Validate::isLoadedObject($tab)) {
            $tab->active = 0;
            $tab->update();
        }

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall()) {
            return false;
        }
        include(dirname(__FILE__).'/sql/uninstall.php');
        Configuration::deleteByName('cpl_config');

        if (version_compare(_PS_VERSION_, '1.7.6.0', '>=')) {
            Tools::clearSf2Cache('prod');
            Tools::clearSf2Cache('dev');
        }

        return $this->removeTabs();
    }

    private function removeTabs()
    {
        $ids_tab = array(
            Tab::getIdFromClassName('AdminCustomProductsList'),
            Tab::getIdFromClassName('AdminCustomProductsListConfig')
        );

        foreach ($ids_tab as $id_tab) {
            if ($id_tab) {
                $tab = new Tab($id_tab);
                $tab->delete();
            }
        }

        return true;
    }

    public function disable($force_all = false)
    {
        if (!parent::disable($force_all)) {
            return false;
        }

        $cpl_config_prev = Configuration::get('cpl_config');
        Configuration::updateValue('cpl_config_prev', $cpl_config_prev);

        $cpl_config = array();
        if (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
            $cpl_config['16'] = '0';
        }
        if (version_compare(_PS_VERSION_, '1.7.6.0', '>=')) {
            $cpl_config['override'] = '0';
        }
        $cpl_config['custom'] = '0';
        Configuration::updateValue('cpl_config', json_encode($cpl_config));

        $this->removeCustomProductsTab();

        return true;
    }

    public function enable($force_all = false)
    {
        if (!parent::enable($force_all)) {
            return false;
        }

        $cpl_config_prev = Configuration::get('cpl_config_prev');
        if ($cpl_config_prev != null) {
            Configuration::updateValue('cpl_config', $cpl_config_prev);

            $cpl_config_prev = json_decode($cpl_config_prev, true);
            if (array_key_exists('custom', $cpl_config_prev) && $cpl_config_prev['custom']) {
                $this->addCustomProductsTab();
            }
        }

        return true;
    }

    /**
     * Load the configuration form
     */
//    public function getContent()
//    {
//        if (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
//            $this->context->controller->informations[] =
//                $this->l('The use of a new tab allows you to use ajax boolean button.');
//
//            $link = $this->context->link->getAdminLink('AdminCustomProductsListConfig');
//            $this->context->controller->informations[] =
//                $this->l('You can change the columns of the list on this tab:') .
//                ' <a href="' . $link . '">' . $this->l('Lists') . ' > ' . $this->l('Customize products columns') .
//                '</a>';
//
//            if (Tools::isSubmit('submitCPLModule')) {
//                $this->postProcess();
//            }
//            return $this->renderForm();
//        } else {
//            $link = new Link();
//            $link = $link->getAdminLink('AdminCustomProductsListConfig', true);
//            Tools::redirectAdmin($link);
//        }
//    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        if (version_compare(_PS_VERSION_, '1.7.0.0', '<') || version_compare(_PS_VERSION_, '1.7.6.0', '>=')) {
            $this->context->cookie->__unset('cpl_list');
            if (Tools::getIsset('updatelist')) {
                // TODO enable or not the menu custom list
                $link = $this->context->link->getAdminLink('AdminCustomProductsListConfig');
                $this->context->cookie->__set('cpl_list', Tools::getValue('id_list'));
                Tools::redirectAdmin($link);
            }
            return $this->initList();
        } else {
            $this->context->cookie->__set('cpl_list', 'custom');
            $link = $this->context->link->getAdminLink('AdminCustomProductsListConfig');
            Tools::redirectAdmin($link);
        }
    }

    private function initList()
    {
        $fields_list = array(
            'list' => array(
                'title' => $this->l('List'),
                'align' => 'text-center',
                'search' => false,
                'orderby' => false
            ),
            'active' => array(
                'title' => $this->l('Active'),
                'align' => 'text-center',
                'type' => 'bool',
                'active' => 'active',
                'ajax' => true,
                'search' => false,
                'orderby' => false
            )
        );

        $cpl_config = json_decode(Configuration::get('cpl_config'), true);

        $list = array();
        if (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
            $list[] = array(
                'id_list' => '16',
                'list' => $this->l('Products'),
                'active' => isset($cpl_config['16']) ? $cpl_config['16'] : '0'
            );
        }
        if (version_compare(_PS_VERSION_, '1.7.6.0', '>=')) {
            $list[] = array(
                'id_list' => 'override',
                'list' => $this->l('Products'),
                'active' => isset($cpl_config['override']) ? $cpl_config['override'] : '0'
            );
        }
        $list[] = array(
            'id_list' => 'custom',
            'list' => $this->l('Products (custom)'),
            'active' => isset($cpl_config['custom']) ? $cpl_config['custom'] : '0'
        );

        $helper = new HelperList();

        $helper->shopLinkType = '';

        // Actions to be displayed in the "Actions" column
        $helper->actions = array('edit');

        $helper->identifier = 'id_list';
        $helper->show_toolbar = true;
        $helper->title = $this->l('Choose the list you want to personalize');
        $helper->table = 'list';

        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        return $helper->generateList($list, $fields_list);
    }

    public function ajaxProcessActivelist()
    {
        $cpl_config = json_decode(Configuration::get('cpl_config'), true);
        $id_list = Tools::getValue('id_list');
        if (null !== $cpl_config && array_key_exists($id_list, $cpl_config)) {
            $cpl_config[$id_list] = (string)(int)!$cpl_config[$id_list];
            Configuration::updateValue('cpl_config', json_encode($cpl_config));
            if ($id_list == 'custom') {
                switch ($cpl_config['custom']) {
                    case '0':
                        $this->removeCustomProductsTab();
                        break;
                    case '1':
                        $this->addCustomProductsTab();
                        break;
                }
            }
            die(json_encode(array('success' => 1, 'message' => $this->l('Successful update.'))));
        } else {
            die(json_encode(array('success' => 0, 'message' => $this->l('An error has occurred.'))));
        }
    }

    public function isActive($list)
    {
        $cpl_config = json_decode(Configuration::get('cpl_config'), true);

        return array_key_exists($list, $cpl_config) ? (bool)$cpl_config[$list] : false;
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookDisplayBackOfficeHeader()
    {
        if (!Module::isEnabled($this->name)) {
            return false;
        }

        $js_var = '<script>var admin_customproductslist_ajax_url = ' .
            (string)json_encode($this->context->link->getAdminLink('AdminCustomProductsListConfig')) . ';
            var current_id_tab = ' . (int) $this->context->controller->id . ';
            </script>';

        switch (Tools::getValue('controller')) {
            case 'AdminCustomProductsListConfig':
                return $js_var;
            case 'AdminProducts':
            case 'AdminCustomProductsList':
                if (version_compare(_PS_VERSION_, '1.6.0.12', '<')) {
                    $this->context->smarty->assign(array(
                        'id_form' => '#product'
                    ));
                } else {
                    $this->context->smarty->assign(array(
                        'id_form' => '#form-product'
                    ));
                }

                return $js_var . $this->context->smarty->fetch($this->local_path.'views/templates/admin/editable.tpl');
        }

        return false;
    }

    public function hookActionAdminProductsListingFieldsModifier($params)
    {
        if (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
            $this->adminProductsListingFieldsModifier16($params);
        } else {
            $this->adminProductsListingFieldsModifier($params);
        }
    }

    public function adminProductsListingFieldsModifier16($params)
    {
        if (!$this->isActive('16')) {
            return;
        }

        $cpl = CPL::getInstance('16')
            ->setUseParent(true)
            ->setDefaultJoin()
            ->setQuery();

        if (empty($params['where'])) {
            $params['where'] = "";
        }

        $params['select'] = implode(', ', $cpl->getSelect());
        $params['join'] = implode(' ', $cpl->getJoin());
        $params['where'] .= $cpl->getDefaultWhere();
        $params['group_by'] = $cpl->getDefaultGroupBy();
        $params['order_by'] = $cpl->getDefaultOrderBy();
        $params['order_way'] = $cpl->getDefaultOrderWay();

        $params['fields'] = $cpl->getFields();
    }

    public function adminProductsListingFieldsModifier($params)
    {
        if (!$this->isActive('override')) {
            return;
        }

        $cpl = CPL::getInstance('override')
            ->setUseParent(false)
            ->setDefaultJoin()
            ->setQuery();

        $sql_select = array(
            'id_product' => array(
                'field' => 'id_product',
                'table' => $cpl->getAlias('product'),
                'filtering' => ' %s '
            ),
            'link_rewrite' => array(
                'field' => 'link_rewrite',
                'table' => $cpl->getAlias('product_lang')
            ),
            'final_price' => '0'
        );
        foreach ($cpl->getSelect() as $select) {
            $sql_select[$select['attribute_as']] = $select;
        }

        if (!array_key_exists('price', $sql_select)) {
            $sql_select['price'] = array(
                'field' => 'price',
                'table' => $cpl->getAlias('product')
            );
        }
        if (!array_key_exists('id_image', $sql_select)) {
            $sql_select['price'] = array(
                'field' => 'id_image',
                'table' => $cpl->getAlias('image_shop')
            );
        }

        $sql_table = array(
            $cpl->getAlias('product') => 'product',
            $cpl->getAlias('product_lang') => array(
                'table' => 'product_lang',
                'join' => 'LEFT JOIN',
                'on' =>
                    $cpl->getAlias('product') . ".`id_product` = " . $cpl->getAlias('product_lang') . ".`id_product`
                        AND " . $cpl->getAlias('product_lang') . ".`id_lang` = " . (int)$this->context->language->id
            ),
        );
        foreach ($cpl->getJoin() as $join) {
            $sql_table[$join['alias']] = array(
                'table' => $join['join'],
                'join' => Tools::strtoupper($join['type']) . ' JOIN',
                'on' => $join['condition'],
                'nested' => (bool)$join['nested']
            );
        }

        $params['sql_select'] = $sql_select;
        $params['sql_table'] = $sql_table;
        $params['sql_group_by'] = array("`" . $cpl->getAlias('product') . "`.`id_product`");
    }

    public function hookActionAdminCustomProductsListListingResultsModifier($params)
    {
        $this->modifyResults($params['list'], 'custom');
    }

    public function hookActionAdminProductsListingResultsModifier($params)
    {
        if (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
            if (!$this->isActive('16')) {
                return;
            }

            $this->modifyResults($params['list'], '16');
        } elseif (version_compare(_PS_VERSION_, '1.7.6.0', '>=')) {
            if (!$this->isActive('override')) {
                return;
            }

            $this->modifyResults($params['products'], 'override');
        }
    }

    private function modifyResults(&$products, $list)
    {
        $cpl_fields = array();
        if ($list == 'override') {
            $cpl = CPL::getInstance($list);
            $cpl_fields = $cpl->getFields();

            foreach ($cpl_fields as $key => $cpl_field) {
                if (array_key_exists('type', $cpl_field)) {
                    switch ($cpl_field['type']) {
                        case 'price':
                        case 'decimal':
                        case 'percent':
                            continue 2;
                    }
                }
                unset($cpl_fields[$key]);
            }
        }

        foreach ($products as &$value) {
            $value['productfinal_price'] = Product::getPriceStatic(
                (int)$value['id_product'],
                true,
                0,
                6,
                null,
                false,
                false
            );
            $value['price_final'] = $value['productfinal_price'];

            $value['customproductslistfinal_price_discount'] = Product::getPriceStatic(
                (int)$value['id_product'],
                true,
                0,
                6,
                null,
                false,
                true
            );

            $value['customproductslistfinal_price_discount_tax_excl'] = Product::getPriceStatic(
                (int)$value['id_product'],
                false,
                0,
                6,
                null,
                false,
                true
            );

            foreach ($cpl_fields as $input_name => $cpl_field) {
                switch ($cpl_field['type']) {
                    case 'price':
                        if (is_numeric($value[$input_name])) {
                            if (version_compare(_PS_VERSION_, '1.7.7.0', '>=')) {
                                $locale = Tools::getContextLocale($this->context);
                                $price_currency = $locale->formatPrice(
                                    $value[$input_name],
                                    $this->context->currency->iso_code
                                );
                            } else {
                                $price_currency = Tools::displayPrice($value[$input_name]);
                            }
                            $value[$input_name] = $price_currency;
                        }
                        break;
                    case 'decimal':
                        if (is_numeric($value[$input_name])) {
                            $value[$input_name] = sprintf('%.2f', $value[$input_name]);
                        }
                        break;
                    case 'percent':
                        if (is_numeric($value[$input_name])) {
                            $value[$input_name] = sprintf('%.2f', $value[$input_name]) . ' %';
                        }
                        break;
                }
            }
        }
    }
}
