<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{customproductslist}prestashop>customproductslist_36ba5b30b89982263b93be1e3839117d'] = 'Personalizar la lista de productos';
$_MODULE['<{customproductslist}prestashop>customproductslist_bb8956c67b82c7444a80c6b2433dd8b4'] = '¿Está seguro de que desea desinstalar este módulo?';
$_MODULE['<{customproductslist}prestashop>customproductslist_be53a0541a6d36f6ecb879fa2c584b08'] = 'Imagen';
$_MODULE['<{customproductslist}prestashop>customproductslist_49ee3087348e8d44e1feda1917443987'] = 'Nombre';
$_MODULE['<{customproductslist}prestashop>customproductslist_63d5049791d9d79d86e9a108b0a999ca'] = 'Referencia';
$_MODULE['<{customproductslist}prestashop>customproductslist_3adbdb3ac060038aa0e6e6c138ef9873'] = 'Categoría';
$_MODULE['<{customproductslist}prestashop>customproductslist_3601146c4e948c32b6424d2c0a7f0118'] = 'Precio';
$_MODULE['<{customproductslist}prestashop>customproductslist_ed26f5ba7a0f0f6ca8b16c3886eb68ad'] = 'Precio final';
$_MODULE['<{customproductslist}prestashop>customproductslist_694e8d1f2ee056f98ee488bdc4982d73'] = 'Cantidad';
$_MODULE['<{customproductslist}prestashop>customproductslist_52f5e0bc3859bc5f5e25130b6c7e8881'] = 'Posición';
$_MODULE['<{customproductslist}prestashop>customproductslist_ec53a8c4f07baed5d8825072c89799be'] = 'Estado';
$_MODULE['<{customproductslist}prestashop>customproductslist_691d1860ec58dd973e803e209697d065'] = 'Liza';
$_MODULE['<{customproductslist}prestashop>customproductslist_120ac16e5963bff60ceb529ddbf6d482'] = 'Personalizar las columnas de la lista de productos';
$_MODULE['<{customproductslist}prestashop>customproductslist_3596bddbb39cc4f19f37a8759611f18b'] = 'Productos personalizados';
$_MODULE['<{customproductslist}prestashop>customproductslist_b2248d1316cd99ffba177ea94153926d'] = 'El uso de una nueva pestaña le permite usar el botón booleano ajax.';
$_MODULE['<{customproductslist}prestashop>customproductslist_8881cdc84534465ae189308eea13a3ff'] = 'Puede cambiar las columnas de la lista en esta pestaña:';
$_MODULE['<{customproductslist}prestashop>customproductslist_4ee29ca12c7d126654bd0e5275de6135'] = 'Lista';
$_MODULE['<{customproductslist}prestashop>customproductslist_4d3d769b812b6faa6b76e1a8abaece2d'] = 'Activa';
$_MODULE['<{customproductslist}prestashop>customproductslist_068f80c7519d0528fb08e82137a72131'] = 'Productos';
$_MODULE['<{customproductslist}prestashop>customproductslist_7af6ffa3c9a418786ee0a2ce568163da'] = 'Productos (personalizados)';
$_MODULE['<{customproductslist}prestashop>customproductslist_910f4367d496c7aec800c00866359cf0'] = 'Elija la lista que desea personalizar';
$_MODULE['<{customproductslist}prestashop>customproductslist_b4a34d3f58b039e7685c2e39b5413757'] = 'Actualización exitosa.';
$_MODULE['<{customproductslist}prestashop>customproductslist_c69732cc923305ac0684ac8fc05a4bca'] = 'Se ha producido un error.';
$_MODULE['<{customproductslist}prestashop>enablelist_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Habilitada';
$_MODULE['<{customproductslist}prestashop>enablelist_b9f5c797ebbf55adccdd8539a65a0241'] = 'Deshabilitada';
$_MODULE['<{customproductslist}prestashop>global_form_876ac16f671c12a4cf8a44e4b7034fb7'] = 'Agregar rápido';
$_MODULE['<{customproductslist}prestashop>global_form_2f75d5965fcb5ae9ebda0ccb001ebd58'] = 'Si no encuentra el atributo que desea agregar, haga clic en el botón \"Agregar una nueva columna\" en la parte superior derecha de la página';
$_MODULE['<{customproductslist}prestashop>global_form_ad3d06d03d94223fa652babc913de686'] = 'Validar';
$_MODULE['<{customproductslist}prestashop>available_attributes_b7400332dfe3450fdbffdb212f090920'] = 'Atributos disponibles';
$_MODULE['<{customproductslist}prestashop>available_attributes_51c45b795d5d18a3e4e0c37e8b20a141'] = 'Tabla';
$_MODULE['<{customproductslist}prestashop>available_attributes_b5a7adde1af5c87d7fd797b6245c2a39'] = 'Descripción';
$_MODULE['<{customproductslist}prestashop>available_attributes_ec211f7c20af43e742bf2570c3cb84f9'] = 'Agregar';
$_MODULE['<{customproductslist}prestashop>position_attributes_39204db149e8b3b2ec4f12624de909c4'] = 'Atributos de posición';
$_MODULE['<{customproductslist}prestashop>position_attributes_a1fa27779242b4902f7ae3bdd5c6d508'] = 'Tipo';
$_MODULE['<{customproductslist}prestashop>position_attributes_ad3d06d03d94223fa652babc913de686'] = 'Validar';
$_MODULE['<{customproductslist}prestashop>position_attributes_ea4788705e6873b424c65e91c2846b19'] = 'Cancelar';
$_MODULE['<{customproductslist}prestashop>configure_a1fa27779242b4902f7ae3bdd5c6d508'] = 'Tipo';
$_MODULE['<{customproductslist}prestashop>configure_8dbbe5098b6dfad060d8fc57a0ebe2c6'] = 'Agregado correctamente';
$_MODULE['<{customproductslist}prestashop>available_tables_111118f909f12d8bb8b39ea920edfeb0'] = 'Elección de la tabla';
$_MODULE['<{customproductslist}prestashop>available_tables_4f490697761d839d99bd06bb0224d627'] = 'Tablas disponibles';
$_MODULE['<{customproductslist}prestashop>available_tables_cabfbd11f7acaf2901efe20a871bdd19'] = 'Cambiar la tabla (sin guiar)';
$_MODULE['<{customproductslist}prestashop>available_tables_1eb2cf386f775968fa109ec2ae3269d2'] = 'Cambiar a tablas predeterminadas';
$_MODULE['<{customproductslist}prestashop>boolean16_field_93cba07454f06a4a960172bbd6e2a435'] = 'Sí';
$_MODULE['<{customproductslist}prestashop>boolean16_field_bafd7322c6e97d25b6299b5d6fe8920b'] = 'No';
$_MODULE['<{customproductslist}prestashop>upgrade-1.6.2_3596bddbb39cc4f19f37a8759611f18b'] = 'Productos personalizados';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_52f5e0bc3859bc5f5e25130b6c7e8881'] = 'Posición';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_287234a1ff35a314b5b6bc4e5828e745'] = 'Atributos';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_51c45b795d5d18a3e4e0c37e8b20a141'] = 'Tabla';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_b78a3223503896721cca1303f776159b'] = 'Título';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_a1fa27779242b4902f7ae3bdd5c6d508'] = 'Tipo';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_d12fbc9a2ba3b0b91a4190959b4c966b'] = 'Alinear';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_8da124923942e61b3a9add617f643d32'] = 'Boolean (Verdadero / Falso)';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_3601146c4e948c32b6424d2c0a7f0118'] = 'Precio';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_a0faef0851b4294c06f2b94bb1cb2044'] = 'Entero';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_937504f318f04eaf0f1d701df4c4d7f3'] = 'Decimal';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_37be07209f53a5d636d5c904ca9ae64c'] = 'Porcentaje';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_44749712dbec183e983dcd78a7736c41'] = 'Fecha';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_34bb8fbfb47885fe47470874152c319d'] = 'Fecha / Hora';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_2c09e798bf8c5a82d211331c82893a0f'] = 'Editable';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_0990deb4b4917b1f178525fb4dc02f4a'] = 'Filtro de lista desplegable (solo disponible para algunos campos)';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_7a1920d61156abc05a60135aefe8bc67'] = 'Defecto';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_64f39bf5288286cc946adb5bc2d50f1a'] = 'Información del producto';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_98f770b0af18ca763421bac22b4b6805'] = 'Características';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_b9208b03bcc9eb4a336258dcdcb66207'] = 'Combinaciones';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_ea9cf7e47ff33b2be14e6dd07cbcefc6'] = 'Transporte';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_dae8ace18bdcbcc6ae5aece263e14fe8'] = 'Opciones';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_0557fa923dcee4d0f86b1409f5c2167f'] = 'Volver';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_45a46eabac93dde89871f9e1d57817df'] = 'Agregar una nueva columna';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_9ba415f3e416a519c826fbd79aba30a8'] = 'Leer la documentación';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_945d5e233cf7d6240f6b783b36a374ff'] = 'Izquierda';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_4f1f6016fc9f3f2353c0cc7c67b292bd'] = 'Centrar';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_92b09c7c48c520c3c55e497875da437c'] = 'Derecha';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_90830093ead4fb5599b3478336b50ecc'] = 'Precio final con descuentos';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_2ba8c07b379d6e8e3b0beb424cd15166'] = 'Precio final';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_b30690be173bcd4a83df0cd68f89a385'] = 'Importe de descuento';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_9bc578cff9163d4dc49ec81cbf22fddf'] = 'Precio final con descuentos (IVA no incluido)';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_87c0f7408a2c076a561e347cea0488cd'] = 'Precio final (IVA no incluido)';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_af1b98adf7f686b84cd0b443e022b7a0'] = 'Categorias';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_6c957f72dc8cdacc75762f2cbdcdfaf2'] = 'Precio unitario';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_133bc9aa44eaaf809041c01ea1361a07'] = 'Muestra el mensaje \"¡En rebajas!\" bandera en la página del producto y en las listas de productos.';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_588907ab2d492aca0b07b5bf9c931eea'] = 'En venta';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_e92cfa244b5eb9025d07522080468445'] = 'Ecotasa';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_b2552975d11932879939fbc0fb4318d9'] = 'Cantidad mínima por pedido';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_24eb41ae265b268c0ce14e1fef872399'] = 'Alerta de stock baja';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_d802cdd26108941c2ceb197be9caa30b'] = 'Precio (IVA no incluido)';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_bf794ad2cabe66da3ef83fb978dd4ba1'] = 'Precio (IVA incluido)';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_13f13e9a00350642fed860ff27b9976e'] = 'Precio de coste';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_7bbdd372c9d171174e3ce1ba9d97d5f6'] = 'Costo adicional de entrega';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_63d5049791d9d79d86e9a108b0a999ca'] = 'Referencia';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_8284ae5df53e6e7ffc1f2cc67ae68765'] = 'Referencia del proveedor';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_cdba184783a0253218efd734da81a9da'] = 'Ubicación de stock';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_7feff43ab21f15d13ba477e2a14ce9f5'] = 'del producto';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_ce5bf551379459c1c61d2a204061c455'] = 'Localización';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_32954654ac8fe66a1d09be19001de2d4'] = 'Ancho';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_eec6c4bdbd339edf8cbea68becb85244'] = 'Altura';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_675056ad1441b6375b2c5abd48c27ef1'] = 'Profundidad';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_8c489d0946f66d17d73f26366a4bf620'] = 'Peso';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_22232b1149de63de3a68d832406321f8'] = '¿Hay un tiempo de entrega adicional con este producto?';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_c562c3dd26e5717d7262450f5247275b'] = '¿Hay una promoción de cantidad?';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_be8e80d576231cbe24b000cbd110e19e'] = '¿Está activo? (Estado) ';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_ec53a8c4f07baed5d8825072c89799be'] = 'Estado';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_ca9c8c69f7ef9c87b4887017b920bd30'] = '¿Está disponible para la venta?';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_de8304421c13baaf6b13b9ed84d73eb7'] = 'Fecha de disponibilidad';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_fe5038bee4d1853f943468d395727df7'] = 'Fecha de creación del producto';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_93a1aebc58a9fe94c9590133f95c393c'] = 'Fecha de actualización del producto';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_82dd98f1d663acc318e6a8ec4ab69b7a'] = 'Unidad';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_4030814f8bb016adcb20dd129987d54f'] = 'Es virtual?';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_615e6f9baca5553d44683a098d342b70'] = 'Virtual';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_09c2e1026952b086c78323051001a0e8'] = 'Descripción del producto';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_c6245a75a10852595815161cbd4dc431'] = 'Breve descripción del producto';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_68144310d2ea225be0cddaea8345d821'] = 'Nombre del producto';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_49ee3087348e8d44e1feda1917443987'] = 'Nombre';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_8353eefc561ec31e55cc61b0e7b2ff98'] = 'Nombre de la categoría predeterminada';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_3adbdb3ac060038aa0e6e6c138ef9873'] = 'Categoría';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_4ecbdf73d2263fd64c7dfcccbbb91fde'] = 'Descripción de la categoría';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_31abf3d27684c9cdcdb109d108e48350'] = 'solo se muestra cuando se selecciona una categoría del árbol encima de la lista';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_b72dc4f8c901c90347912fbee41326e8'] = 'Imagen del producto';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_be53a0541a6d36f6ecb879fa2c584b08'] = 'Imagen';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_bd958e2f216b79c9deb4e95ab17cd439'] = 'Cantidad del producto';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_694e8d1f2ee056f98ee488bdc4982d73'] = 'Cantidad';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_1c2c40f4198c0e66a64f77c607ad10f1'] = 'Cantidad física';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_e169517aef8d635b269420910921c8a8'] = 'Cantidad reservada (ventas incluidas)';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_5aadb4755f656295b1cb00457ea047eb'] = 'de las combinaciones';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_1e884e3078d9978e216a027ecd57fb34'] = 'Correo electrónico';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_be81c2645954582f02d014eba8a5205c'] = 'Fecha de creación del asunto';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_223914ac0caec3cc4cd28bcb7138f85c'] = 'Fecha de actualización del asunto';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_bc37cebb67728bbb50f9c55c1fe727cd'] = 'Palabras clave del producto';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_189f63f277cd73395561651753563065'] = 'Etiquetas';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_854c9e1028779e3e216e21813fab51cd'] = 'El nombre del fabricante';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_1be6f9eb563f3bf85c78b4219bf09de9'] = 'Marca';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_5a3a6d769b14752edc3ca38baab225df'] = 'El nombre de los proveedores';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_1814d65a76028fdfbadab64a5a8076df'] = 'Proveedores';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_4e021cd834848e751e2d1bc66cee697d'] = 'Todas las combinaciones';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_f641fe697a059e62d9353f43471bdb74'] = 'Todas las características';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_4b78ac8eb158840e9638a3aeb26c4a9d'] = 'Impuesto';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_1d6af794b2599c1407a83029a09d1ecf'] = 'Transportistas';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_022c170bea34709e834089f1a532bbfa'] = 'Porfavor introduzca un valor.';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_c69732cc923305ac0684ac8fc05a4bca'] = 'Se ha producido un error.';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_f992aa2cd482bdcdca4c13ea04d78361'] = 'No puede editar este valor.';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_b4a34d3f58b039e7685c2e39b5413757'] = 'Actualización exitosa';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_aff947fbf3ebe40ec869b58711fa1435'] = 'Este campo no existe.';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_99d708ed32fbfbdb7555fc4bfe7120b8'] = 'No se puede cambiar el valor.';
$_MODULE['<{customproductslist}prestashop>admincustomproductslistconfig_1508b06d63357212d0ce7f50343961a1'] = 'Se ha producido un error.';
$_MODULE['<{customproductslist}prestashop>admincustomproductslist_31fde7b05ac8952dacf4af8a704074ec'] = 'Avance';
$_MODULE['<{customproductslist}prestashop>admincustomproductslist_42d00bc24fd43cfe3e2e30d01f39ee95'] = 'Filtrar por categoría';
$_MODULE['<{customproductslist}prestashop>admincustomproductslist_b4a34d3f58b039e7685c2e39b5413757'] = 'Actualización exitosa.';
$_MODULE['<{customproductslist}prestashop>admincustomproductslist_c69732cc923305ac0684ac8fc05a4bca'] = 'Se ha producido un error.';
$_MODULE['<{customproductslist}prestashop>admincustomproductslist_99d708ed32fbfbdb7555fc4bfe7120b8'] = 'No se puede cambiar el valor.';
$_MODULE['<{customproductslist}prestashop>admincustomproductslist_1d133e6a456a1936a3296a67285d3141'] = 'La nueva posición debe ser diferente.';
$_MODULE['<{customproductslist}prestashop>admincustomproductslist_443ccc8d8db8b28d8532d171ab222290'] = 'Ingrese una buena posición (máx .:%d)';
